import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface Props {
	src: string;
}

export const AllCover = styled.div`
	align-content: center;
	pointer-events: inherit;
`
export const BackdropView = styled.div`
	height: 100vh;
	width: 100vw;
	position: fixed;
	top: 0;
	left: 0;
	background-color: black;
	opacity: 0.7;
	z-index: 1;
`
export const ViewContainer = styled.div`
	position: fixed;
	width: 363px;
	height: 368px;
	left: 50vw;
	top: 50vh;
	transform: translate(-50%, -50%);

	background: #ffffff;
	border-radius: 5px;
	padding: 13px 6px 13px 5px;
	z-index: 2;
`

export const ExitBtn = styled(FontAwesomeIcon)`
	position: absolute;
	top: 13px;
	right: 14.25px;
`

export const Picture = styled.div`
	width: 100%;
	height: 100%;
	border-radius: 3px;
	background-image: url(${({ src }: Props) => src});
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
`

export const CrossClose = styled(FontAwesomeIcon)`
	position: absolute;
	top: 16px;
	right: 11.7px;
	margin-bottom: 12px;
	cursor: pointer;
`
