import React, { useState } from 'react'
import {
  Box, Leftbox, Rightbox, Righttopbox, Dropdown,
  PlusMinusInCard, ClickBox, ImgBox, Img, Daybox
} from './styled'
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import add from 'components/images/addincard.svg'
import minus from 'components/images/minusincard.svg'
interface Props {
  setPerExercise: number
  increaseSet: () => void
  decreaseSet: () => void
  img: string;
  name: string;
  index: number
}

const DayWithDropdown = ({
  name,
  index,
  img,
  setPerExercise,
  increaseSet,
  decreaseSet
}: Props) => {
  const [isShowMenu, setIsShowMenu] = useState(false)
  const toggleIsShowMenu = () => { setIsShowMenu(!isShowMenu) }
  const arrowIcon = isShowMenu ? faChevronUp : faChevronDown

  return (
    <div>
      <Daybox onClick={toggleIsShowMenu} isShowMenu={isShowMenu}>
        Day {index + 1}
        <Dropdown >
          <FontAwesomeIcon icon={arrowIcon} />
        </Dropdown>
      </Daybox>
      {isShowMenu ? (
        <Box>
          <Leftbox>
            <ImgBox><Img style={{ backgroundImage: `url(${img})` }}></Img></ImgBox>
          </Leftbox>
          <Rightbox>
            <Righttopbox>{name}</Righttopbox>
            <PlusMinusInCard>
              <ClickBox src={minus} alt="cls" onClick={decreaseSet} />
              {setPerExercise}
              <ClickBox src={add} alt="cls" onClick={increaseSet} />
            </PlusMinusInCard>
          </Rightbox>
        </Box>
      ) : null}
    </div>
  )
}
export default DayWithDropdown
