import React from 'react'

interface Props {
    isOpen: boolean
    delayTime: number
}

export function delayUnmounting(Component: any) {
    return class extends React.Component<Props, {}> {
        state = {
            shouldRender: this.props.isOpen
        };

        componentDidUpdate(prevProps: any) {
            if (prevProps.isOpen && !this.props.isOpen) {
                setTimeout(
                    () => this.setState({ shouldRender: false }),
                    this.props.delayTime
                )
            } else if (!prevProps.isOpen && this.props.isOpen) {
                this.setState({ shouldRender: true })
            }
        }

        render() {
            return this.state.shouldRender ? <Component {...this.props} /> : null
        }
    }
}
