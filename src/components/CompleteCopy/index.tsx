import React from 'react'
import { OverallContainer, OrangeText, Triangle, WhiteText } from './styled'

const CompleteCopy = () => {
  return (
    <OverallContainer>
      <WhiteText>Complete copy this Program to</WhiteText>
      <OrangeText>MyProgram</OrangeText>
      <Triangle></Triangle>
    </OverallContainer>
  )
}

export default CompleteCopy
