import React from 'react'
import { Container, CheckIcon, CardWrapper } from './styled'
import ExcercisePostCard from 'components/ExcercisePostCard'
import { DayExerciseType, ExerciseType } from 'modules/stores/types'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faCircle } from '@fortawesome/free-regular-svg-icons'
import { observer, inject } from 'mobx-react'
import withAuth from 'utils/withAuth'
import { get } from 'lodash'
import { compose } from 'recompose'
import ExerciseStore from 'modules/stores/ExerciseStore'

interface StoreProps {
  exerciseWithId: (id: string) => ExerciseType
}

interface Props {
  index: number
  isEdit?: boolean
  isCheckedExercises?: boolean[]
  dayExercise: DayExerciseType
  clickElementOnDelete?: (index: number, id: string) => void
}

const DayExercise = ({ isCheckedExercises, dayExercise,
  index, isEdit, clickElementOnDelete, exerciseWithId }: Props & StoreProps) => {
  const checkIcon = get(isCheckedExercises, index) ? faCheckCircle : faCircle
  const onClickToDelete = () => {
    const id = get(dayExercise, 'id')
    if (clickElementOnDelete) clickElementOnDelete(index, id)
  }
  const { setsPerExercise } = dayExercise
  const exercise = exerciseWithId(dayExercise.exercise)
  const onCheck = isEdit ? onClickToDelete : undefined
  return (
    <Container onClick={onCheck}>
      {isEdit &&
        <CheckIcon icon={checkIcon} size="2x" color="#f7522a" />
      }
      <CardWrapper isEdit={isEdit || false}>
        <ExcercisePostCard exercise={exercise} setsPerExercise={setsPerExercise}
	hasSetPerExerciseIcon hasAddBtn={false} />
      </CardWrapper>
    </Container >
  )
}

interface Stores {
  exerciseStore: ExerciseStore
}

export default compose<StoreProps & Props, Props>(
  withAuth,
  inject(({ exerciseStore }: Stores) => ({
    exerciseWithId: exerciseStore.exerciseId,
  })),
  observer
)(DayExercise)
