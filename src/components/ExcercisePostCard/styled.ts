import styled from 'styled-components'

export const Box = styled.div`
	height: 100%;
	width: 100%;
	background-color: white;
	box-shadow: 0px 2px 4px 2px #c4c4c4;
	border-radius: 4px;
	cursor: pointer;
	padding-bottom: 5px;
`
export const Header = styled.div`
	text-align: left;
	padding-top: 15px;
	padding-left: 20px;
	font-size: 14px;
	color: #f7522a;
	font-weight: bold;
`
export const Detail = styled.div`
	text-align: left;
	padding-left: 20px;
	font-size: 12px;
	color: rgba(76, 76, 76, 0.5);
	display: flex;
`
export const Clock = styled.div`
	padding-top: 2.5px;
	padding-right: 3px;
`
export const ImgBox = styled.div`
	height: 135px;
	width: 100%;
	background-repeat: no-repeat;
	background-position: center center;
	background-size: auto 90%;
`

export const Add = styled.div`
	cursor: pointer;
	position: absolute;
	bottom: 0;
	right: 0;
	padding-right: 10px;
`

export const BoxNumEx = styled.div`
	position: absolute;
	height: 40px;
	width: 40px;
	bottom: 10px;
	right: 10px;
	background-color: #f7522a;
	border-radius: 50%;
	line-height: 40px;
	white-space: nowrap;
	color: #ffffff;
	font-size: 18px;
`
