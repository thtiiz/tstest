import React from 'react'
import { Box, Header, Detail, Clock, ImgBox, Add, BoxNumEx } from './styled'
import clock from 'components/images/clock.svg'
import history from 'core/history'
import { ExerciseType } from 'modules/stores/types'
import { mapCategory, mapDifficulty } from 'utils/mapper'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import ExerciseStore from 'modules/stores/ExerciseStore'
import Addtoprobut from 'components/images/Addtoprobut.svg'
import { pick } from 'lodash'

interface Props {
  exercise: ExerciseType
  className?: string
  hasAddProgramIcon?: boolean
  setsPerExercise?: number
  hasSetPerExerciseIcon?: boolean
  hasAddBtn?: boolean
}

interface StoreProps {
  clickExercise: (exercise: ExerciseType) => void
}

const ExcercisePostCard = ({
  exercise,
  className,
  clickExercise,
  hasAddProgramIcon = false,
  hasSetPerExerciseIcon = false,
  setsPerExercise,
  hasAddBtn = true
}: Props & StoreProps) => {
  const { id, category, difficulty, images, name, set } = pick(exercise, ['id', 'category',
    'difficulty', 'images', 'name', 'set'])
  const mappedCategory = mapCategory.get(category)
  const mappedDifficulty = mapDifficulty.get(difficulty)
  const onClickCard = () => {
    history.push({
      pathname: `/exercise/${id}`,
      state: { hasAddBtn }
    })
    clickExercise(exercise)
  }
  const onClickAdd = () => {
    history.push({
      pathname: `/myprograms`,
      state: {
        isAddProgram: true,
        exerciseId: exercise.id,
      }
    })
  }
  return (
    <div style={{ position: 'relative' }} >
      <Box className={className} onClick={onClickCard}>
        <ImgBox style={{ backgroundImage: `url(${images[0]})` }} />
        <Header>{name}</Header>
        <Detail>Area : {mappedCategory}</Detail>
        <Detail>Difficulty :
          <Detail style={{ color: '#f7522a', paddingLeft: '3px' }}>{mappedDifficulty}</Detail>
        </Detail>
        <Detail>
          <Clock><img src={clock} alt="clk" /></Clock>
          : set of {set}
        </Detail>
      </Box>
      {hasAddProgramIcon && <Add className={className}
	onClick={onClickAdd}><img src={Addtoprobut} alt="atp" /></Add>}
      {hasSetPerExerciseIcon && <BoxNumEx>X{setsPerExercise}</BoxNumEx>}
    </div>
  )
}

interface Stores {
  exerciseStore: ExerciseStore
}

export default compose<StoreProps & Props, Props>(
  inject(({ exerciseStore }: Stores) => ({
    clickExercise: exerciseStore.clickExercise,
  })),
  observer,
)(ExcercisePostCard)
