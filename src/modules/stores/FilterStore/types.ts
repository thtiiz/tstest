export interface ExerciseName {
  label: string
  value: string
}
