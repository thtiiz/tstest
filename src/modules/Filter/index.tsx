import React, { Component } from 'react'
import {
  FilterContainer,
  Header,
  TextFilter,
  Catagory,
  TextCatagory,
  Choicecontainer,
  ChoiceHeader,
  Close,
  HeaderCatagory,
  ResetCategory,
  BtnApply,
  Backdrop,
  ChoiceSelecter,
  OverFlowChoice,
  Coverall
} from './styled'
import Checkbox from 'components/Checkbox'
import FilterTag from 'components/FilterTag'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import FilterStore from 'modules/stores/FilterStore'
import { observer, inject } from 'mobx-react'
import { compose } from 'recompose'
import FilterTags from 'components/FilterTags'

const muscleItems = ['Abs', 'Arms', 'Back', 'Calves', 'Chest', 'Legs', 'Shoulders']

const equipmentItems = ['bodyweight', 'Brbell', 'SZ-Bar', 'Dumbbell',
  'Gym mat', 'Swiss Ball', 'Pull-up bar', 'Bench', 'Incline bench', 'Kettlebell']

interface Stores {
  filterStore: FilterStore
}

interface Props {
  handleToggle: () => void;
  isOpenFilter: boolean
}
interface State {
  isChoiceMuscle: boolean;
}

interface Storeprops {
  filterTagItems: string[];
  filterClick: (filterItem: string) => void;
  filterTagClose: (FilterTagItem: string) => void;
  resetFilterTag: () => void;
  applyFilters: () => void
}

class Filter extends Component<Props & Storeprops, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      isChoiceMuscle: true
    }
  }

  rederFilterTag = () => {
    const { filterTagItems, filterTagClose } = this.props
    return filterTagItems.map((filterTagItem, i) => (
      <FilterTag
	postName={filterTagItem}
	onClose={() => filterTagClose(filterTagItem)}
	key={`checkbox-${filterTagItem}`}
      />
    ))
  }

  handleClose = () => {
    const { handleToggle, applyFilters } = this.props
    applyFilters()
    handleToggle()
  }

  handleMuscle = () => {
    this.setState({ isChoiceMuscle: true })
  }

  handleEqiup = () => {
    this.setState({ isChoiceMuscle: false })
  }

  renderFilter = (filterItems: string[]) => {
    const { filterClick, filterTagItems } = this.props
    return filterItems.map((filterItem, i) => {
      const isCheck = filterTagItems.findIndex(filtertag => filtertag === filterItem) !== -1
      return (
        <Checkbox
	onClick={() => filterClick(filterItem)}
	title={filterItem}
	isCheck={isCheck}
	key={`checkbox-${filterItem}`}
        />
      )
    })
  }

  render() {
    const { resetFilterTag, filterTagItems, filterTagClose, isOpenFilter, handleToggle } = this.props
    const { isChoiceMuscle } = this.state
    return (
      <Coverall>
        <FilterContainer>
          <div onClick={handleToggle}>
            <Close icon={faTimes} ></Close>
          </div>
          <Header>
            <TextFilter>Filter</TextFilter>
          </Header>
          <Catagory>
            <HeaderCatagory>
              <TextCatagory>Catagory</TextCatagory>
              <ResetCategory onClick={resetFilterTag}>Reset</ResetCategory>
            </HeaderCatagory>
            <FilterTags filterTagItems={filterTagItems} filterTagClose={filterTagClose} />
          </Catagory>
          <Choicecontainer>
            <ChoiceSelecter>
              <ChoiceHeader isClickChoice={isChoiceMuscle} onClick={this.handleMuscle}>Muscle
                </ChoiceHeader>
              <ChoiceHeader isClickChoice={!isChoiceMuscle} onClick={this.handleEqiup}>Equipment
                </ChoiceHeader>
            </ChoiceSelecter>
            {isChoiceMuscle ? <OverFlowChoice>{this.renderFilter(muscleItems)}</OverFlowChoice>
              : <OverFlowChoice>{this.renderFilter(equipmentItems)}</OverFlowChoice>}
          </Choicecontainer>
          <BtnApply onClick={this.handleClose}>APPLY</BtnApply>
        </FilterContainer>
        <Backdrop isOpenFilter={isOpenFilter} onClick={handleToggle} />
      </Coverall>
    )
  }
}

export default compose<any, any>(
  inject(({ filterStore }: Stores) => ({
    filterTagItems: filterStore.filterTagItems,
    filterClick: filterStore.filterClick,
    filterTagClose: filterStore.filterTagClose,
    resetFilterTag: filterStore.resetFilterTag,
    applyFilters: filterStore.applyFilters
  })),
  observer
)(Filter)
