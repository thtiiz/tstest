import React, { Component } from 'react'
import { Container, LogoName, Hamburger, ContainerLogo, Redline } from './styled'
import hamburger from 'components/images/hamburgur.svg'
import MenuBar from 'modules/MenuBar/lazy'
import { Linked } from 'components/Linked'

interface State {
	isOpen: boolean;
}
class Navbar extends Component<{}, State> {
	constructor(props: any) {
		super(props)
		this.state = {
			isOpen: false,
		}
	}

	handleOpen = () => {
		this.setState({ isOpen: true })
	}

	closeMenuBar = () => {
		this.setState({ isOpen: false })
	}

	render() {
		const { isOpen } = this.state
		return (
			<Container>
				<Hamburger onClick={this.handleOpen}>
					<img src={hamburger} alt="hamburger menu" />
				</Hamburger>
				<ContainerLogo>
					<Linked to="/home">
						<LogoName>EXERSITE</LogoName>
					</Linked>
					<Redline></Redline>
				</ContainerLogo>
				<MenuBar delayTime={100} isOpen={isOpen} closeMenuBar={this.closeMenuBar}></MenuBar>
			</Container >)
	}
}

export default Navbar
