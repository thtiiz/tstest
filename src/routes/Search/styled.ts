import styled from 'styled-components'
import FilterTags from 'components/FilterTags'

export const SkeletonWrapper = styled.div`
	&:not(:last-child) {
		margin-bottom: 16px;
	}
`

export const SearchContainer = styled.div`
	width: 100%;
	padding: 24px 0 16px 0;
	display: flex;
	flex-direction: column;
	align-items: center;
	background-color: white;
`

export const PostcardBox = styled.div`
	padding: 16px 10px 16px 10px;
	> :not(:first-child) {
		margin-top: 16px;
	}
`
export const NoResult = styled.div`
	height: 50vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`

export const FilterTagsStyled = styled(FilterTags)`
	align-self: flex-start;
	margin: 24px 0 0 16px;
`

export const Container = styled.div`
	display: grid;
	grid-gap: 10px;
	grid-template-columns: repeat(auto-fill, minmax(303px, 1fr));
`
