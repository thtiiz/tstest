import React from 'react'
import { Program, Container, CheckIcon } from './styled'
import { faCircle } from '@fortawesome/free-regular-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { MyProgramType } from 'modules/stores/types'
import { get } from 'lodash'

interface Props {
  isEdit: boolean
  index: number
  myProgram: MyProgramType
  editedPrograms: string[]
  isCheckedPrograms: boolean[]
  onClickProgram: (id: string) => void
  setEditedPrograms: (editedPrograms: string[]) => void
  setIsCheckedPrograms: (isCheckedPrograms: boolean[]) => void
}

const MyProgramButton = ({ isEdit, myProgram, onClickProgram, setIsCheckedPrograms, isCheckedPrograms,
  editedPrograms, setEditedPrograms, index }: Props) => {
  const checkIcon = isCheckedPrograms[index] ? faCheckCircle : faCircle
  const toggleCheck = () => {
    const newChecked = isCheckedPrograms
    newChecked[index] = !newChecked[index]
    setIsCheckedPrograms(newChecked)
  }
  const clickProgramOnEdit = () => {
    toggleCheck()
    if (editedPrograms.indexOf(myProgram.id) !== -1) {
      setEditedPrograms(editedPrograms.filter(
        editedProgram => editedProgram !== myProgram.id))
    } else {
      setEditedPrograms(
        editedPrograms.concat(myProgram.id))
    }
  }
  const onClick = () => {
    onClickProgram(myProgram.id)
  }
  const name = get(myProgram, ['program', 'name'], '')
  return (
    <Container onClick={isEdit
      ? clickProgramOnEdit : onClick}>
      {isEdit && <CheckIcon icon={checkIcon} size="2x" color="#f7522a" />}
      <Program >{name}</Program>
    </Container >
  )
}

export default MyProgramButton
