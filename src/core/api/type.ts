export interface Fetch {
	headers?: object;
	method: string;
	body?: object;
	params?: object;
	path: string;
}
