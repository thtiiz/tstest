import React, { Fragment } from 'react'
import { HeaderText, Username, Edit } from './styled'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { STATES } from 'core/api/FetchData'
import CoverImg from 'components/CoverImg'

interface Props {
  isAddProgram?: boolean
  isEdit: boolean
  toggleIsEdit?: () => void
  fetchState?: string
  title?: string
  numHeader: number
  username?: string
  image?: string
}
export const HEADER_STATES = {
  MYPROGRAMS: 0,
  MYPROGRAM: 1,
  POSTINDAY: 2,
  PROGRAMSUGGEST: 3
}
const MyProgramsHeader = ({ numHeader, title, username, image,
  isAddProgram, isEdit, toggleIsEdit, fetchState }: Props) => {
  const renderEditIcon = () => fetchState === STATES.LOADING
    ? null : <FontAwesomeIcon icon={faEdit} />

  const renderHeader = (numHeader: number) => {
    if (numHeader === 0) {
      return <HeaderText>
        Hello, <Username>{title}</Username>
        {!isAddProgram && !isEdit && <Edit onClick={toggleIsEdit}>{renderEditIcon()}</Edit>}
      </HeaderText>
    } else if (numHeader === 1) {
      return (
        <Fragment>
          <CoverImg image={image || ''} />
          <HeaderText>
            {title} {!isAddProgram && !isEdit && <Edit onClick={toggleIsEdit}>{renderEditIcon()}</Edit>}
          </HeaderText>
        </Fragment>
      )
    } else if (numHeader === 2) {
      return <HeaderText>{`Day ${title}`}
        {!isEdit && <Edit onClick={toggleIsEdit}>{renderEditIcon()}</Edit>}
      </HeaderText>
    } else {
      return <HeaderText>
        {title}
      </HeaderText>
    }
  }
  return (
    <div>{renderHeader(numHeader)}</div>
  )
}

export default MyProgramsHeader
