import { fullHeight } from 'utils'
import styled from 'styled-components'

export const Container = styled.div`
	background-color: white;
	${fullHeight};
`

export const Detail = styled.div`
	font-size: 20px;
	text-align: left;
	margin: 15px 0px 0px 20px;
	width: 89.33%;
	height: fit-content;
	overflow-wrap: break-word;
`
export const Line = styled.div`
	height: 2px;
	background-color: #f7522a;
`
export const SecondText = styled.div`
	font-size: 20px;
	font-weight: bold;
	text-align: left;
	margin: 0px 0px 15px 20px;
`
