import { Btn } from 'components/Button/styled'
import styled from 'styled-components'

export const EditContentBox = styled.div`
	margin-bottom: 26.8px;
	margin-top: 2px;
`

export const HeaderContent = styled.div`
	font-weight: bold;
	font-size: 18px;
	align-items: center;
	color: #20212a;
	margin-bottom: 12px;
	display: flex;
	align-items: center;
`

export const ContentInput = styled.input`
	width: 300px;
	min-height: 50.15px;
	background: #ffffff;
	border: 2px solid #e6e6e6;
	box-sizing: border-box;
	border-radius: 4px;
	color: #bbbdbf;
	font-size: 18px;
	font-weight: bold;
	padding: 23px;
`

export const HeaderBox = styled.div`
	justify-content: space-between;
	display: flex;
`
export const EditBtn = styled(Btn)`
	width: 47px;
	height: 27px;
	background: #f7522a;
	border-radius: 4px;
`
