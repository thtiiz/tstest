import React, { useState, useEffect } from 'react'
import {
  FormContainer, LoginButton, Container,
  DetailText, GenderRadioButton, TextGender, GenderBox, InputTextStyle
} from './styled'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import { RegisterForm } from 'modules/stores/types'
import UserStore from 'modules/stores/UserStore'
import history from 'core/history'
import { get } from 'lodash'
import ErrorMessage from 'components/ErrorMessage'

interface StoreProps {
  signup: (formValues: RegisterForm) => void
  isLogin: boolean
  state: string
  isInitial: boolean
  errorTxt: string
}
const SignupPage = ({ errorTxt, signup, isLogin, state, isInitial }: StoreProps) => {
  const [formValues, setFormValues] =
    useState({ firstName: '', lastName: '', email: '', password: '', gender: '1' })
  const [confirmPassword, setConfirmPassword] = useState('')
  const [disabled, setDisabled] = useState(false)

  const handleChange = (e: any) => {
    const { name, value } = e.target
    const newFormValues = { ...formValues, [name]: value }
    setFormValues(newFormValues)
  }
  const handleConfirmPassword = (e: any) => {
    setConfirmPassword(e.target.value)
  }

  const onRegister = (e: any) => {
    e.preventDefault()
    if (confirmPassword === formValues.password) signup(formValues)
    setDisabled(true)
  }

  useEffect(() => {
    if (!isInitial) setDisabled(false)
    else if (state === 'loading') setDisabled(true)
    else setDisabled(false)
    const historyState = get(history, ['location', 'state'], {})
    const { redirect, ...safeState } = historyState
    if (isLogin) history.push({ pathname: redirect || '/home', state: safeState })
  }, [disabled, isInitial, isLogin, state])

  return (
    <Container>
      {errorTxt ? <ErrorMessage message={errorTxt} /> : null}
      <FormContainer onSubmit={onRegister} id="form1">
        <span style={{
          marginTop: '20px',
          marginBottom: '40px',
          fontSize: '20px'
        }} > Create your account</span>

        <InputTextStyle required type="text" name="firstName"
	onChange={handleChange} value={formValues.firstName} title="First Name" />

        <InputTextStyle title="Last Name" required type="text" name="lastName"
	onChange={handleChange} value={formValues.lastName} />
        <InputTextStyle title="Email" required type="text" name="email"
	onChange={handleChange} value={formValues.email} />
        <DetailText>Use 6 or more characters</DetailText>
        <InputTextStyle title="Password" required type="password" name="password"
	onChange={handleChange} value={formValues.password} />
        <DetailText>Use 8 or more characters with a mix of letters, numbers and symbols</DetailText>
        <InputTextStyle title="Confirm Password" required type="password" name="confirm"
	onChange={handleConfirmPassword}
	value={confirmPassword} />

        <p style={{ marginTop: '-10px' }}> Select your Gender </p>
        <GenderBox>
          <GenderRadioButton type="radio" id="choice1"
	value={1} name="gender" onChange={handleChange}
	checked={formValues.gender === '1'} /><TextGender>Male</TextGender>
          <GenderRadioButton type="radio" id="choice2"
	value={2} name="gender" onChange={handleChange}
	checked={formValues.gender === '2'} /><TextGender>Female</TextGender>
          <GenderRadioButton type="radio" id="choice3"
	value={3} name="gender" onChange={handleChange}
	checked={formValues.gender === '3'} /><TextGender>Other</TextGender>
        </GenderBox>
        <LoginButton disabled={disabled} type="submit" form="form1" >Sign Up</LoginButton>
      </FormContainer>
    </Container >

  )
}
interface Stores {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  inject(({ userStore }: Stores) => ({
    signup: userStore.signup,
    isLogin: userStore.isLogin,
    state: userStore.state,
    isInitial: userStore.isInitial,
    errorTxt: userStore.error
  })),
  observer
)(SignupPage)
