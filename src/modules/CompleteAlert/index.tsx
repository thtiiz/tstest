import React from 'react'
import { Header, More, SelectBtn, Yes, No } from 'modules/CompleteAlert/styled'
import ReactModal from 'react-modal'

interface Props {
  onComplete: () => void;
  toggleAlert: () => void;
  isOpen: boolean;
}

const customStyles = {
  content: {
    top: '40%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '280px',
    padding: '0px',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)'
  },
}

const CompleteAlert = (props: any) => {
  const { onComplete, isOpen, toggleAlert } = props
  const handleYes = () => {
    onComplete()
    toggleAlert()
  }
  return (
    <ReactModal isOpen={isOpen} style={customStyles} ariaHideApp={false}
	contentLabel="Complete Modal" shouldCloseOnOverlayClick onRequestClose={toggleAlert}>
      <Header>Complete day?</Header>
      <More>{"if you complete this,you couldn't change it."}</More>
      <SelectBtn>
        <No onClick={toggleAlert}>No</No>
        <Yes onClick={handleYes}>Yes</Yes>
      </SelectBtn>
    </ReactModal>
  )
}
export default CompleteAlert
