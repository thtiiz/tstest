import React from 'react'
import { DayExerciseType } from 'modules/stores/types'
import DayExercise from 'modules/DayExercise'
import { ContentContainer } from './styled'

interface Props {
  dayExercises: DayExerciseType[]
  isCheckedElements?: boolean[]
  clickElementOnDelete?: (index: number, id: string) => void
  isEdit?: boolean
}

const RenderExercises = ({ isEdit, dayExercises, clickElementOnDelete, isCheckedElements }: Props) => {
  return (
    <ContentContainer>{dayExercises.map((dayExercise: DayExerciseType, index: number) => (
      <DayExercise key={`exercise-${index}`} isEdit={isEdit}
	isCheckedExercises={isCheckedElements}
	clickElementOnDelete={clickElementOnDelete}
	index={index} dayExercise={dayExercise} />
    ))}</ContentContainer>
  )
}

export default RenderExercises
