import React from 'react'
import { Route, Router } from 'react-router'
import Loadable from 'react-loadable'
import history from 'core/history'
import Navbar from 'components/Navbar'

const LandingPage = Loadable({
  loader: () => import(/* webpackChunkName: "Landing.page" */ 'routes/LandingPage'),
  loading: () => null,
})

const Home = Loadable({
	loader: () => import(/* webpackChunkName: "Home.page" */ 'routes/Home'),
	loading: () => null,
})
const AboutUs = Loadable({
	loader: () => import(/* webpackChunkName: "AboutUs.page" */ 'routes/AboutUs'),
	loading: () => null,
})
const SearchResult = Loadable({
	loader: () => import(/* webpackChunkName: "SearchResult.page" */ 'routes/Search'),
	loading: () => null,
})

const ExercisePost = Loadable({
	loader: () => import(/* webpackChunkName: "ExercisePost.page" */ 'routes/ExercisePost'),
	loading: () => null,
})

const Login = Loadable({
	loader: () => import(/* webpackChunkName: "Login.page" */ 'routes/Login'),
	loading: () => null,
})

const Signup = Loadable({
	loader: () => import(/* webpackChunkName: "Signup.page" */ 'routes/Signup'),
	loading: () => null,
})

const MyPrograms = Loadable({
	loader: () => import(/* webpackChunkName: "MyPrograms.page" */ 'routes/MyPrograms'),
	loading: () => null,
})
const MyProfile = Loadable({
	loader: () => import(/* webpackChunkName: "Profile.page" */ 'routes/Profile'),
	loading: () => null,
})
const MyEditProfile = Loadable({
	loader: () => import(/* webpackChunkName: "EditProfile.page" */ 'modules/EditProfile'),
	loading: () => null,
})

const PostInDay = Loadable({
	loader: () => import(/* webpackChunkName: "PostInDay.page" */ 'modules/PostInDay'),
	loading: () => null,
})

const MyProgram = Loadable({
	loader: () => import(/* webpackChunkName: "MyProgram.page" */ 'routes/MyProgram'),
	loading: () => null,
})

const ProgramSuggest = Loadable({
  loader: () => import(/* webpackChunkName: "ProgramSuggest.page" */ 'routes/ProgramSuggest'),
  loading: () => null,
})

const PostInDaySuggest = Loadable({
  loader: () => import(/* webpackChunkName: "PostInDaySuggest.page" */ 'routes/PostInDaySuggest'),
  loading: () => null,
})

const Routes = () => {
  return (
    <Router history={history}>
      <Navbar />
      <Route exact path="/" component={LandingPage} />
      <Route exact path="/Home" component={Home} />
      <Route path="/myprograms" component={MyPrograms} />
      <Route exact path="/programsuggest/:id" component={ProgramSuggest} />
      <Route exact path="/programsuggest/day/:numDay" component={PostInDaySuggest} />
      <Route exact path="/myprogram/:id" component={MyProgram} />
      <Route exact path="/myprogram/day/:numDay" component={PostInDay} />
      <Route path="/search" component={SearchResult} />
      <Route path="/exercise/:id" component={ExercisePost} />
      <Route path="/aboutus" component={AboutUs} />
      <Route path="/signup" component={Signup} />
      <Route path="/login" component={Login} />
      <Route path="/profile" component={MyProfile} />
      <Route path="/editprofile" component={MyEditProfile} />
    </Router>
  )
}

export default Routes
