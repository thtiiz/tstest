import styled from 'styled-components'

export const HeaderTitle = styled.div`
	font-size: 24px;
	line-height: 28px;
	color: #000000;
	margin-bottom: 11px;
`
export const StopLine = styled.div`
	height: 0px;
	border: 1px solid #f7522a;
	transform: rotate(0.18deg);
	margin-top: 38px;
	margin-bottom: 38px;
	margin-left: 18px;
	margin-right: 18px;
`
