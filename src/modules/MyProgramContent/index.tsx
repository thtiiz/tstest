import React, { Component } from 'react'
import { HeaderTitle, StopLine } from './styled'
import MyProgramCard from 'components/MyProgramCardProfile'
import { MyProgramType } from 'modules/stores/types'

interface Props {
  myPrograms: MyProgramType[];
}

class MyProgramContent extends Component<Props, {}> {
  rederMyProgramList = (status: number) => {
    const { myPrograms } = this.props
    return myPrograms.map((myProgram, i) => {
      if (myProgram.status === status) {
        return (
          <MyProgramCard myProgram={myProgram} key={`MyProgramCard-${i}`} />
        )
      }
    })
  }

  render() {
    return (
      <div>
        <HeaderTitle>In Process</HeaderTitle>
        {this.rederMyProgramList(2)}
        <StopLine></StopLine>
        <HeaderTitle>InComplete</HeaderTitle>
        {this.rederMyProgramList(1)}
        <StopLine></StopLine>
        <HeaderTitle>Complete</HeaderTitle>
        {this.rederMyProgramList(3)}
        <StopLine></StopLine>
      </div>
    )
  }
}

export default MyProgramContent
