import React, { useEffect } from 'react'
import { PostcardBox, NoResult, FilterTagsStyled, SearchContainer, Container } from './styled'
import SearchBar from 'modules/SearchBar'
import history from 'core/history'
import qs from 'querystring'
import { inject, observer } from 'mobx-react'
import SearchStore from 'modules/stores/SearchStore'
import { compose } from 'recompose'
import { ExerciseType } from 'modules/stores/types'
import FilterStore from 'modules/stores/FilterStore'
import { STATES } from 'core/api/FetchData'
import searchIcon from 'components/images/SearchBar-searchicon.svg'
import SearchResultCards from 'modules/SearchResultCards'
import SkeletonCard from 'components/SkeletonCard'
import { ContainerCard } from 'modules/SearchResultCards/styled'

interface StoreProps {
  searchResults: ExerciseType[]
  fetchSearch: (params: any) => void
  filterTagClose: (filter: string) => void
  allFilters: string[]
  applyFilters: () => void
  searchState: string
}

const renderSkeletonCard = () => (new Array(6).fill(0).map((value, i) =>
  <ContainerCard key={`skeleton-${i}`}><SkeletonCard /></ContainerCard>
))

const noResult = () => <NoResult>
  <img src={searchIcon} alt="not-found-img" style={{ width: '100px' }} />
  <h4 style={{ color: '#BFBFBF' }}>
    No matching items found.
  </h4>
</NoResult>

const Search = ({
  searchResults, fetchSearch,
  filterTagClose, allFilters, applyFilters, searchState }: StoreProps) => {
  const { search } = history.location

  useEffect(() => {
    fetchSearch(qs.parse(search.slice(1)))
  }, [fetchSearch, search])

  const onTagClose = (filter: string) => {
    filterTagClose(filter)
    applyFilters()
  }

  const isLoadedwithNoContent = (searchState === STATES.LOADED && (searchResults.length > 0)) ||
    searchState === STATES.LOADING

  const resultsWithSkeletion = () => {
    return searchState !== STATES.LOADED
      ? <Container>{renderSkeletonCard()}</Container>
      : <SearchResultCards searchResults={searchResults} />
  }

  return (
    <div>
      <SearchContainer>
        <SearchBar />
        <FilterTagsStyled filterTagItems={allFilters} filterTagClose={onTagClose} />
      </SearchContainer>
      <PostcardBox >
        {isLoadedwithNoContent ? resultsWithSkeletion()
          : noResult()}
      </PostcardBox>
    </div>
  )
}

interface Stores {
  searchStore: SearchStore
  filterStore: FilterStore
}

export default compose<StoreProps, {}>(
  inject(({ searchStore, filterStore }: Stores) => ({
    filterTagClose: filterStore.filterTagClose,
    allFilters: filterStore.allFilters,
    applyFilters: filterStore.applyFilters,
    searchResults: searchStore.searchResults,
    fetchSearch: searchStore.fetchSearch,
    searchState: searchStore.state,
  })),
  observer
)(Search)
