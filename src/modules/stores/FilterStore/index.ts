import history from 'core/history'
import qs from 'querystring'
import { get, isArray } from 'lodash'
import FetchData from 'core/api/FetchData'
import { serializeParams, paramToString } from 'utils'
import { observable, action, computed } from 'mobx'
import { CATEGORY, EQUIPMENT } from './../types'
import { ExerciseName } from './types'
import { mapCategory, mapEquipment } from 'utils/mapper'

class FilterStore extends FetchData {
	@observable
	filterTagItems: string[] = []

	@observable
	searchValue: string = ''

	@observable
	exerciseNames: ExerciseName[]

	constructor() {
		super()
		this.exerciseNames = []
	}

	@action
	handleSearchChange = (newVal: string) => {
		this.searchValue = newVal
	}

	@action
	fetchExerciseNames = async () => {
		const data = await this.fetchData({ path: '/exercises/names', method: 'get' })
		const message = get(data, 'message')
		if (message) {
			this.exerciseNames = message
		}
		return message || []
	}

	@action
	filterClick = (filterItem: string) => {
		const found = this.filterTagItems.findIndex(element => element === filterItem) === -1
		if (found) {
			this.filterTagItems = this.filterTagItems.concat(filterItem)
		} else {
			this.filterTagClose(filterItem)
		}
	}

	@action
	syncHistory = () => {
		const syncFilterTagItems: string[] = []
		let syncSearchValue = this.searchValue
		const params = qs.parse(history.location.search.slice(1))
		for (const key in params) {
			if (key === 'category') {
				paramToString(params[key], syncFilterTagItems, mapCategory)
			} else if (key === 'equipment') {
				paramToString(params[key], syncFilterTagItems, mapEquipment)
			} else if (key === 'q') {
				if (isArray(params[key])) syncSearchValue = params[key][0]
				else syncSearchValue = String(params[key])
			}
		}
		this.filterTagItems = syncFilterTagItems
		this.searchValue = syncSearchValue
	}

	@action
	filterTagClose = (filterTagItem: string) => {
		this.filterTagItems = this.filterTagItems.filter(item => item !== filterTagItem)
		if (filterTagItem === this.searchValue) this.searchValue = ''
	}

	@action
	resetFilterTag = () => {
		this.filterTagItems = []
	}

	@computed
	get filterToParams() {
		const category: string[] = []
		const equipment: string[] = []
		const q = this.searchValue
		this.allFilters.forEach(filterTagItem => {
			const categoryNumber = get(CATEGORY, filterTagItem)
			const equipmentNumber = get(EQUIPMENT, filterTagItem)
			if (categoryNumber) category.push(categoryNumber)
			if (equipmentNumber) equipment.push(equipmentNumber)
		})
		return { category, equipment, q }
	}

	@computed
	get allFilters() {
		return this.searchValue ? this.filterTagItems.concat(this.searchValue) : this.filterTagItems
	}

	@action
	applyFilters = () => {
		history.push({
			pathname: '/search',
			search: serializeParams(this.filterToParams),
		})
	}
}

export default FilterStore
