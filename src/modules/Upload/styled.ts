import styled from 'styled-components'

export const UploadPic = styled.div`
	transform: translate(0, -50%);
	position: absolute;
	padding: 20px;
	cursor: pointer;
`
