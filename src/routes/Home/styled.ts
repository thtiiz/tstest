import styled from 'styled-components'

export const SearchContainer = styled.div`
	width: 100%;
	height: 118px;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	background-color: #ffffff;
`
