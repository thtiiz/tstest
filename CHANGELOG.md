# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.1.0](https://gitlab.com/thtiiz/tstest/compare/v3.0.0...v3.1.0) (2019-11-12)

## [3.0.0](https://gitlab.com/thtiiz/tstest/compare/v2.2.1...v3.0.0) (2019-11-05)


### Bug Fixes

* Programsuggest page (withAuth) ([a0e668e](https://gitlab.com/thtiiz/tstest/commit/a0e668e))

### [2.2.1](https://gitlab.com/thtiiz/tstest/compare/v2.2.0...v2.2.1) (2019-11-05)

## [2.2.0](https://gitlab.com/thtiiz/tstest/compare/v2.1.1...v2.2.0) (2019-11-05)


### Bug Fixes

* joi ([3d2f5d8](https://gitlab.com/thtiiz/tstest/commit/3d2f5d8))

### [2.1.1](https://gitlab.com/thtiiz/tstest/compare/v2.1.0...v2.1.1) (2019-11-02)

## [2.1.0](https://gitlab.com/thtiiz/tstest/compare/v2.0.0...v2.1.0) (2019-11-02)

## [2.0.0](https://gitlab.com/thtiiz/tstest/compare/v1.0.1...v2.0.0) (2019-11-02)


### Bug Fixes

* all Program edit icon ([f7445e6](https://gitlab.com/thtiiz/tstest/commit/f7445e6))
* fix filter height ([9f1fc7d](https://gitlab.com/thtiiz/tstest/commit/9f1fc7d))
* history on myPrograms ([290c5f4](https://gitlab.com/thtiiz/tstest/commit/290c5f4))
* isAddProgram state ([4513426](https://gitlab.com/thtiiz/tstest/commit/4513426))
* login redirect ([de3aa3a](https://gitlab.com/thtiiz/tstest/commit/de3aa3a))
* login redirect ([6f9d9b1](https://gitlab.com/thtiiz/tstest/commit/6f9d9b1))
* onclick add to program ([a3f124e](https://gitlab.com/thtiiz/tstest/commit/a3f124e))
* store ([89a3e24](https://gitlab.com/thtiiz/tstest/commit/89a3e24))


### Features

* Exercisepost in Day ([f656135](https://gitlab.com/thtiiz/tstest/commit/f656135))
* My program ([263a84e](https://gitlab.com/thtiiz/tstest/commit/263a84e))
* Upload profile image ([5a25262](https://gitlab.com/thtiiz/tstest/commit/5a25262))

### [1.0.1](https://gitlab.com/thtiiz/tstest/compare/v1.0.0...v1.0.1) (2019-10-09)

## [1.0.0](https://gitlab.com/thtiiz/tstest/compare/v0.1.0...v1.0.0) (2019-10-09)

### Features

* Feature/ExercisePost - add Exercise card ([3fdffc73](https://gitlab.com/thtiiz/tstest/merge_requests/17))
* Feature/ExercisePost - add Detail page ([4150f9b2](https://gitlab.com/thtiiz/tstest/merge_requests/30))
* Feature/Filter&Search - add Searchbar with filter ([4730b12e](https://gitlab.com/thtiiz/tstest/merge_requests/27))
* Feature/Homepage - add Homepage with carousel ([9727fd78](https://gitlab.com/thtiiz/tstest/merge_requests/22))
* Feature/AboutUs - add About us page ([5cb92bde](https://gitlab.com/thtiiz/tstest/merge_requests/25))
* Feature/Navbar - add Navbar ([11e41c2a](https://gitlab.com/thtiiz/tstest/merge_requests/15))
* Feature/Footer - add Footer ([15058675](https://gitlab.com/thtiiz/tstest/merge_requests/16))

## 0.1.0 (2019-09-19)
