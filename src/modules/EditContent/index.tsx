import React, { ChangeEvent } from 'react'
import { HeaderContent, EditContentBox, ContentInput, HeaderBox } from 'modules/EditContent/styled'

interface Props {
	title: string;
	value: string;
	isPassword?: boolean;
	name: string;
	onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const EditContent = (props: Props) => {
	const { title, value, onChange, isPassword, name } = props
	const type = isPassword ? 'password' : 'text'
	return (
		<EditContentBox>
			<HeaderBox>
				<HeaderContent>{title}</HeaderContent>
			</HeaderBox>
			<ContentInput type={type} value={value} onChange={onChange} name={name} />
		</EditContentBox>
	)
}
export default EditContent
