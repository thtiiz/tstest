import styled from 'styled-components'

export const HeaderText = styled.div`
	font-size: 24px;
	font-weight: bold;
	text-align: left;
	padding: 20px 0px 0px 20px;
	display: flex;
	padding-right: 20px;
`
export const Username = styled.div`
	color: #f7522a;
	margin-left: 8px;
`
export const Edit = styled.div`
	margin-left: auto;
	cursor: pointer;
`
