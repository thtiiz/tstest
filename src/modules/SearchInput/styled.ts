import styled from 'styled-components'

export const searchStyled = {
	container: (provided: any) => ({
		...provided,
		width: '200px',
		display: 'flex',
		justifyContent: 'center',
	}),
	control: (provided: any) => ({
		...provided,
		border: 'none',
		width: '100%',
		marginLeft: '4px',
		backgroundColor: '#FFFFFF',
		boxShadow: 'none',
	}),
	menu: (provided: any) => ({
		...provided,
		width: '340px',
	}),
	option: (provided: any, state: any) => ({
		...provided,
		backgroundColor: state.isSelected ? '#f7522a' : null,
		'&:hover': {
			backgroundColor: state.isFocused ? '#7b7b7b' : null,
			color: state.isFocused ? 'white' : null,
		},
	}),
}

export const CreateText = styled.div`
	color: #7b7b7b;
	&:hover {
		color: white;
	}
`

export const Highlight = styled.span`
	font-weight: bold;
`
