export const initialMyProgram = {
	id: '',
	program: {
		id: '',
		name: '',
		description: '',
		difficulty: 0,
		days: [],
		image: '',
	},
	currentDay: 0,
	totalDays: 0,
	totalDayComplete: 0,
	completedExercises: 0,
	percent: 0,
	status: 1,
}

export const initialExercise = {
	id: '',
	description: '',
	difficulty: 0,
	comments: '',
	name: '',
	category: 0,
	muscles: [],
	musclesSecondary: [],
	equipment: [],
	images: [],
	imageGif: '',
	set: 0,
}

export const initialDay = {
	id: '',
	exercises: [],
	isComplete: false,
}
