import React, { Fragment } from 'react'
import { Container } from './styled'
import { DayType } from 'modules/stores/types'
import DayButton from 'components/DayButton'
import MyProgramStore from 'modules/stores/MyProgramStore'

interface Props {
  isEdit: boolean
  days: DayType[]
  myProgramStore: MyProgramStore
}

const Daylist = ({ isEdit, myProgramStore, days }: Props) => {
  return (
    <Fragment>
      <Container >
        {days.map((day: DayType, index: number) => (
          <DayButton key={`Day-${day.id}`} index={index} day={day}
	myProgramStore={myProgramStore} isEdit={isEdit} />
        ))}
      </Container>
    </Fragment>
  )
}

export default Daylist
