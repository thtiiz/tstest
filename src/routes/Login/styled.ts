import styled from 'styled-components'
import Button from 'components/Button'
import { fullHeight } from 'utils'

export const ErrorText = styled.p`
	font-size: 16px;
	color: red;
	margin-bottom: 10px;
	margin-top: 10px;
	padding: 20px;
`
export const FormContainer = styled.form`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 322px;
	margin-top: 60px;
	text-align: left;
	place-items: center;
`
export const Container = styled.div`
	width: 100%;
	${fullHeight}
	background-color: #ffffff;
	justify-items: center;
	display: flex;
	flex-direction: column;
`

export const LoginButton = styled(Button)`
	box-shadow: 0px 4px 9px rgba(0, 0, 0, 0.25);
	margin-bottom: 40px;
	width: 300px;
	font-size: 18px;
	padding: 14px 0px 14px 0px;
`

export const ORSection = styled.div`
	display: flex;
	flex-direction: row;
	width: 193px;
	height: 21px;
	align-items: center;
	margin-bottom: 15px;
`

export const ORText = styled.h1`
	font-size: 18px;
	color: #c4c4c4;
	margin-left: 15px;
	margin-right: 15px;
`
export const SignUpText = styled.span`
	font-size: 14px;
	color: #4c4c4c;
`

export const Line = styled.div`
	width: 70px;
	height: 2px;
	border: 1px solid #c4c4c4;
	transform: matrix(1, 0, 0, 1, 0, 0);
`

export const SocialSection = styled.div`
	display: flex;
	justify-content: center;
	width: 150px;
	height: 30px;
	margin-bottom: 20px;
`
export const ErrorAlert = styled.span`
	margin-top: 80px;
	font-size: 20px;
`
export const ErrorSection = styled.div`
	height: 70px;
	width: 100%;
`

export const Img = styled.img`
	width: 30px;
	height: 30px;
	cursor: pointer;
	&:not(:last-child) {
		margin-right: 30px;
	}
`

export const Signup = styled.span`
	color: #7d7abc;
	cursor: pointer;
	font-weight: bold;
	text-decoration: underline;
`
