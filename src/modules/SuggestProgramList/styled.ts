import styled from 'styled-components'

export const Container = styled.div`
	padding: 20px;
	display: grid;
	grid-gap: 20px;
	justify-items: center;
	align-items: center;
	grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
`
