import React, { useState, Fragment } from 'react'
import MyProgramButton from 'components/MyProgramButton'
import { Container, Edit, CancelButton, RemoveButton } from './styled'
import { MyProgramType } from 'modules/stores/types'
import history from 'core/history'

interface Props {
  isEdit: boolean
  myPrograms: MyProgramType[]
  toggleIsEdit: () => void
  deletePrograms: (editedPrograms: string[], isCheckedPrograms: boolean[]) => void
}

const MyProgramsList = ({ myPrograms, isEdit, toggleIsEdit, deletePrograms }: Props) => {
  const [editedPrograms, setEditedPrograms] = useState<string[]>([])
  const [isCheckedPrograms, setIsCheckedPrograms] = useState<boolean[]>(
    new Array(myPrograms.length).fill(false))
  const onCancel = () => {
    toggleIsEdit()
    setEditedPrograms([])
    setIsCheckedPrograms(new Array(myPrograms.length).fill(false))
  }
  const onRemove = () => {
    toggleIsEdit()
    deletePrograms(editedPrograms, isCheckedPrograms)
    setEditedPrograms([])
    setIsCheckedPrograms(new Array(myPrograms.length).fill(false))
  }
  const onClickProgram = (id: string) => {
    const state = history.location.state
    history.push({
      pathname: `/myprogram/${id}`,
      state
    })
  }
  return (
    <Fragment>
      <Container isEdit={isEdit}>
        {myPrograms.map((myProgram, index) => (
          <MyProgramButton key={`MyProgram-${index}`} onClickProgram={onClickProgram} myProgram={myProgram}
	isEdit={isEdit} setEditedPrograms={setEditedPrograms} editedPrograms={editedPrograms}
	isCheckedPrograms={isCheckedPrograms} index={index} setIsCheckedPrograms={setIsCheckedPrograms} />
        ))}
      </Container>
      {isEdit && <Edit>
        <CancelButton onClick={onCancel}>Cancel</CancelButton>
        <RemoveButton onClick={onRemove}>Remove</RemoveButton>
      </Edit>}
    </Fragment>
  )
}

export default MyProgramsList
