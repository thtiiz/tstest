import React, { Fragment } from 'react'
import { SearchContainer } from './styled'
import { ExerciseType, ProgramType } from 'modules/stores/types'
import { inject, observer } from 'mobx-react'
import { compose } from 'recompose'
import ExerciseStore from 'modules/stores/ExerciseStore'
import SearchBar from 'modules/SearchBar'
import CarouselExercise from 'components/CarouselExercise'
import CarouselProgram from 'components/CarouselProgram'
import SuggestProgramStore from 'modules/stores/SuggestProgramStore'
import Footer from 'components/Footer'
interface StoreProps {
  exercises: ExerciseType[]
  programs: ProgramType[]
  chestExercise: ExerciseType[]
  beginnerExercise: ExerciseType[]
}

export const Home = (props: StoreProps) => {
  const { chestExercise, beginnerExercise,
    exercises, programs } = props
  return (
    <Fragment>
      <SearchContainer>
        <SearchBar />
      </SearchContainer>
      <CarouselProgram title="Suggest Program" programs={programs} />
      <CarouselExercise title='Popular Search' exercises={exercises} path='' />
      <CarouselExercise title='For Chest' exercises={chestExercise} path='?category=11' />
      <CarouselExercise title='For Beginner' exercises={beginnerExercise} path='?difficulty=1' />
      <Footer />
    </Fragment>
  )
}
interface Stores {
  exerciseStore: ExerciseStore
  suggestProgramStore: SuggestProgramStore
}

export default compose<StoreProps, {}>(
  inject(({ exerciseStore, suggestProgramStore }: Stores) => ({
    exercises: exerciseStore.exercises,
    fetchExercises: exerciseStore.fetchExercises,
    fetchChestCategory: exerciseStore.fetchChestCategory,
    fetchBeginnerExercise: exerciseStore.fetchBeginnerExercise,
    programs: suggestProgramStore.programs,
    chestExercise: exerciseStore.chestExercise,
    beginnerExercise: exerciseStore.beginnerExercise,
  })),
  observer
)(Home)
