import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const Program = styled.div`
	background-color: #f7522a;
	width: 100%;
	height: 43px;
	border-radius: 10px;
	text-align: left;
	padding: 10px 20px 10px 20px;
	color: #ffffff;
	font-size: 18px;
	text-overflow: ellipsis;
	overflow: hidden;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`

export const Container = styled.div`
	display: flex;
	cursor: pointer;
	align-items: center;
	margin-bottom: 20px;
`

export const CheckIcon = styled(FontAwesomeIcon)`
	margin: 0 10px 0 0;
`
