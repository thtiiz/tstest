import { Fetch } from './type'
import Client from 'core/api/Client'
import { observable, action } from 'mobx'

export const STATES = {
	INITIAL: 'initial',
	LOADING: 'loading',
	ERROR: 'error',
	LOADED: 'loaded',
}

// loading object
class FetchData {
	client: Client = new Client()

	@observable
	state: string = ''

	constructor() {
		this.state = STATES.INITIAL
	}

	@action
	setState = (newState: string) => {
		this.state = newState
	}

	async fetchData(request: Fetch) {
		const { LOADING, ERROR, LOADED } = STATES
		let data
		try {
			this.state = LOADING
			data = await this.client.fetch(request)
			this.state = LOADED
		} catch (e) {
			this.state = ERROR
		} finally {
			// eslint-disable-next-line
			return data
		}
	}
}

export default FetchData
