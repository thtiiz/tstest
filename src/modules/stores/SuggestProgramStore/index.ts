import { computedFn } from 'mobx-utils'
import FetchData from 'core/api/FetchData'
import { get } from 'lodash'
import { ProgramType, FetchPrograms, MyProgramType } from 'modules/stores/types'
import { observable, action } from 'mobx'

class SuggestProgramStore extends FetchData {
	@observable
	programs: ProgramType[] = []

	constructor() {
		super()
		this.fetchSuggestProgram()
	}

	programId = computedFn((id: string) => {
		const index = this.programs.findIndex(program => program.id === id)
		return this.programs[index]
	})

	dayIndex = computedFn((programId: string, index: number) => {
		const program: ProgramType = this.programId(programId)
		return program.days[index]
	})

	@action
	copyProgram = async (programId: string) => {
		const fetch = await this.fetchData({
			path: '/user/copyProgram',
			method: 'post',
			body: { programId },
		})
		const data: MyProgramType = get(fetch, 'data')
		return data
	}

	@action
	fetchSuggestProgram = async () => {
		const fetch: FetchPrograms = await this.fetchData({
			path: '/programs',
			method: 'get',
		})
		this.programs = get(fetch, 'data', [])
	}
}

export default SuggestProgramStore
