import React from 'react'
import { SecondText } from 'routes/MyPrograms/styled'
import { Highlight } from './styled'

interface Props {
  isEdit: boolean
}

const DayText = ({ isEdit }: Props) => {
  return (
    <SecondText>{isEdit ? <Highlight>Deletes</Highlight> : 'Days'}</SecondText>
  )
}

export default DayText
