import FetchData from 'core/api/FetchData'
import { get } from 'lodash'
import { ExerciseType } from '../types'
import { FetchExercises } from 'modules/stores/types'
import { observable, action, computed } from 'mobx'

class SearchStore extends FetchData {
	@observable
	searchResults: ExerciseType[] = []

	@observable
	searchHistory: string[] = []

	constructor() {
		super()
		this.syncSearchHistory()
	}

	pushSearchHistory = (value: string) => {
		const searchHistory = localStorage.getItem('searchHistory')
		if (searchHistory) {
			const arr: string[] = JSON.parse(searchHistory).filter((element: string) => element !== value)
			if (arr.length < 5) {
				arr.unshift(value)
			} else {
				arr.unshift(value)
				arr.pop()
			}
			localStorage.setItem('searchHistory', JSON.stringify(arr))
		} else {
			localStorage.setItem('searchHistory', JSON.stringify([value]))
		}
		this.syncSearchHistory()
	}

	@action
	syncSearchHistory = () => {
		const searchHistory = localStorage.getItem('searchHistory')
		if (searchHistory) this.searchHistory = JSON.parse(searchHistory)
	}

	@computed
	get historyOptions() {
		const options: any[] = []
		this.searchHistory.forEach((value: string) => options.push({ value, label: value }))
		return [{ label: 'History', options }]
	}

	@action
	fetchSearch = async (params: any) => {
		const fetch: FetchExercises = await this.fetchData({
			path: '/exercises',
			method: 'get',
			params,
		})
		this.searchResults = get(fetch, 'data', [])
	}
}

export default SearchStore
