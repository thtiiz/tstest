import styled from 'styled-components'
export const BoxText = styled.div`
	padding: 1px 5px 1px 5px;
	height: fit-content;
	width: fit-content;
	background-color: #000000;
	display: flex;
	border-radius: 4px;
	margin: 2px 2px 2px 3px;
`
export const CloseInTag = styled.img`
	margin-left: 5px;
	cursor: pointer;
`
export const Text = styled.div`
	font-size: 14px;
	color: #ffffff;
`
