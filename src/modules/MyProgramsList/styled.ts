import styled from 'styled-components'

interface Props {
	isEdit: boolean;
}

export const Container = styled.div`
	display: flex;
	width: 335px;
	margin: 0 auto;
	flex-direction: column;
	padding-bottom: ${({ isEdit }: Props) => (isEdit ? '50px' : '0px')};
`

const Button = styled.div`
	cursor: pointer;
	width: 100%;
	height: 50px;
	display: flex;
	align-items: center;
	justify-content: center;
`

export const RemoveButton = styled(Button)`
	background-color: #f7522a;
	color: #ffffff;
`
export const CancelButton = styled(Button)`
	background-color: #ffffff;
	color: #f7522a;
`

export const Edit = styled.div`
	display: flex;
	position: fixed;
	bottom: 0;
	width: 100vw;
	font-size: 18px;
	border-top: solid 2px #f7522a;
`
