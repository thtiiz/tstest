import { Component } from 'react'

interface State {
  value: number;
}

interface Props {
  percent: number;
  children: any;
}

class ChangingProgressProvider extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      value: 0,
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        value: this.props.percent,
      })
    }, 0)
  }

  componentDidUpdate(prevProp: Props) {
    if (this.props.percent !== prevProp.percent) {
      this.setState({ value: 0 })
      setTimeout(() => {
        this.setState({
          value: this.props.percent,
        })
      }, 0)
    }
  }

  render() {
    const { children } = this.props
    const { value } = this.state
    return children(value)
  }
}

export default ChangingProgressProvider
