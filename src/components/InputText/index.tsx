
import React from 'react'
import { Text, InputWrap, UnderLine } from './styled'

interface Props {
  required: boolean
  type: string
  name?: string
  onChange: any
  value: any
  title: string
  className?: string
}

const InputText = ({ className, title, ...rest }: Props) => {
  return (
    <InputWrap className={className}>
      <Text {...rest} />
      <label >{title}</label>
      <UnderLine />
    </InputWrap >
  )
}

export default InputText
