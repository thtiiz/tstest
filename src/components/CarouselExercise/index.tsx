
import React, { Component } from 'react'
import { CarouselCard } from 'modules/CarouselCard'
import { TitleSection, Title, Container, Card, ViewMore } from './styled'
import { ExerciseType } from 'modules/stores/types'
import ExcercisePostCard from 'components/ExcercisePostCard'
import SkeletonCard from 'components/SkeletonCard'
import { Redirect } from 'react-router'

interface Props {
  exercises: ExerciseType[]
  title: string
  path: string
}
interface States {
  isRedirect: boolean;
}

const renderSkeletonCard = () => (new Array(6).fill(0).map((value, i) =>
  <Card key={`skeleton-${i}`}><SkeletonCard /></Card>
))

class CarouselExercise extends Component<Props, States> {
  constructor(props: Props) {
    super(props)
    this.state = {
      isRedirect: false
    }
  }

  carouselCard = () => {
    const { exercises } = this.props
    return (<CarouselCard>
      {exercises.length !== 0 ? this.renderCard() : renderSkeletonCard()}
    </CarouselCard>)
  }

  handleViewMore = () => {
    this.setState({ isRedirect: true })
  }

  renderCard = () => {
    const { exercises } = this.props
    return exercises.map(exercise =>
      <Card key={exercise.name}>
        <ExcercisePostCard hasAddProgramIcon exercise={exercise} key={exercise.name} />
      </Card>
    )
  }

  render() {
    const { path } = this.props
    return (
      <div>
        <TitleSection >
          <Title>{this.props.title}</Title>
          <ViewMore onClick={this.handleViewMore}>view more</ViewMore>
          {this.state.isRedirect && < Redirect push to={'/search' + path} />}
        </TitleSection >
        <Container>
          {this.carouselCard()}
        </Container>
      </div>
    )
  }
}

export default CarouselExercise
