import styled from 'styled-components'

export const OverallContainer = styled.div`
	width: 267px;
	height: 32px;
	background: #2f2f2f;
	border-radius: 3px;
	padding: 8px 6px 8px 6px;
	display: flex;
	position: relative;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`

export const WhiteText = styled.div`
	font-weight: 500;
	font-size: 12px;
	line-height: 16px;
	letter-spacing: 0.06em;
	color: #ffffff;
`

export const OrangeText = styled.div`
	font-weight: 500;
	font-size: 12px;
	line-height: 16px;
	letter-spacing: 0.06em;
	color: #f7522a;
	margin-left: 3px;
`

export const Triangle = styled.div`
	width: 0;
	height: 0;
	border-left: 12.5px solid transparent;
	border-right: 12.5px solid transparent;
	border-bottom: 25px solid #2f2f2f;
	position: absolute;
	transform: translate(0, -100%);
	top: 5px;
	right: 14px;
`
