import React from 'react'
import ChangingProgressProvider from 'components/ChangingProgressProvider'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import { Container } from './styled'
import 'react-circular-progressbar/dist/styles.css'

interface Props {
  percentage: number;
}

const PercentCircle = ({ percentage }: Props) => {
  return (
    <Container>
      <ChangingProgressProvider percent={percentage}>
        {(percentage: number) => (
          <CircularProgressbar
	value={percentage}
	text={`${percentage}%`}
	styles={buildStyles({
              strokeLinecap: 'butt',
              textSize: '18px',
              pathTransitionDuration: 0.5,
              pathColor: `rgba(247,	82,	42)`,
              textColor: '#000000',
              trailColor: '#000000',
              backgroundColor: '#ffffff',
            })}
          />
        )}
      </ChangingProgressProvider>
    </Container>
  )
}

export default PercentCircle
