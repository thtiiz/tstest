import history from 'core/history'
import FetchData from 'core/api/FetchData'
import { get, pick } from 'lodash'
import { observable, action, computed } from 'mobx'
import {
	LoginForm,
	RegisterForm,
	UserType,
	FetchUser,
	MyProgramType,
	EditProfileForm,
} from 'modules/stores/types'
import MyProgramStore from '../MyProgramStore'
import { computedFn } from 'mobx-utils'

class UserStore extends FetchData {
	@observable
	user: UserType

	@observable
	myPrograms: MyProgramStore[]

	@computed
	get name() {
		const { firstName, lastName } = this.user
		return firstName + ' ' + lastName || ''
	}

	@observable
	isLogin: boolean

	@observable
	isInitial: boolean

	@observable
	error: any

	constructor() {
		super()
		this.isInitial = false
		this.myPrograms = []
		this.user = {
			firstName: '',
			lastName: '',
			email: '',
			gender: '',
			image: '',
			myPrograms: [],
		}
		this.isLogin = false
		this.verify()
	}

	myProgramsStatus = computedFn((status: number) => {
		const myPrograms: MyProgramType[] = []
		this.myPrograms.map((myProgram: MyProgramStore) => {
			if (myProgram.status === status) myPrograms.push(myProgram.myProgram)
		})
		return myPrograms
	})

	myProgramId = computedFn((id: string) => {
		const index = this.myPrograms.findIndex(myProgram => myProgram.id === id)
		return this.myPrograms[index]
	})

	@computed
	get getMyPrograms() {
		const myPrograms: MyProgramType[] = []
		this.myPrograms.map(myProgramStore => {
			myPrograms.push(myProgramStore.myProgram)
		})
		return myPrograms
	}

	@action
	loginNative = async (formValues: LoginForm) => {
		const res = await this.fetchData({ path: '/auth/login', method: 'post', body: formValues })
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			localStorage.setItem('token', message)
			this.isLogin = true
			this.fetchUser()
		} else {
			this.isLogin = false
		}
	}

	@action
	loginFacebook = async (response: any) => {
		const body = pick(response, ['email', 'expiresIn', 'first_name', 'last_name', 'id', 'picture'])
		const res = await this.fetchData({ path: '/auth/login/facebook', method: 'post', body })
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			console.log(message)
			localStorage.setItem('token', message)
			this.isLogin = true
			this.fetchUser()
		} else {
			this.isLogin = false
		}
	}

	@action
	loginGoogle = async (response: any) => {
		const { email, name, googleId, imageUrl } = get(response, 'profileObj', {})
		const expiresIn = get(response, ['Zi', 'expires_in'], 3600)
		const res = await this.fetchData({
			path: '/auth/login/google',
			method: 'post',
			body: { email, name, id: googleId, picture: imageUrl, expiresIn },
		})
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			localStorage.setItem('token', message)
			this.isLogin = true
			this.fetchUser()
		} else {
			this.isLogin = false
		}
	}

	@action
	login = async (formValues: LoginForm) => {
		const res = await this.fetchData({ path: '/auth/login', method: 'post', body: formValues })
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			localStorage.setItem('token', message)
			this.isLogin = true
			this.fetchUser()
		} else {
			this.isLogin = false
		}
	}

	@action
	verify = async () => {
		const res = await this.fetchData({ path: '/auth/verifytoken', method: 'get' })
		const error = get(res, 'error')

		if (error) {
			localStorage.removeItem('token')
			this.isLogin = false
		} else {
			this.isLogin = true
			await this.fetchUser()
		}
		this.isInitial = true
	}

	@action
	logout = async () => {
		localStorage.removeItem('token')
		this.isLogin = false
	}

	@action
	signup = async (formValues: RegisterForm) => {
		const res = await this.fetchData({ path: '/auth/register', method: 'post', body: formValues })
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			this.isLogin = true
			localStorage.setItem('token', message)
			this.fetchUser()
		}
	}

	@action
	fetchUser = async () => {
		const fetch: FetchUser = await this.fetchData({
			path: '/user/me',
			method: 'get',
		})
		const data: UserType = get(fetch, 'data')
		if (data) {
			const myPrograms: MyProgramStore[] = []
			this.user = data
			data.myPrograms.map(myProgram => {
				myPrograms.push(new MyProgramStore(myProgram))
			})
			this.myPrograms = myPrograms
		}
	}

	@action
	editProfile = async (formValues: EditProfileForm) => {
		const res = await this.fetchData({
			path: '/user/editProfile',
			method: 'post',
			body: { newProfile: formValues },
		})
		const message = get(res, 'message')
		this.error = get(res, 'error')
		if (message) {
			localStorage.setItem('token', message)
			this.fetchUser()
			history.goBack()
		}
	}

	@action
	addMyProgram = async (data: MyProgramType) => {
		this.myPrograms = this.myPrograms.concat(new MyProgramStore(data))
	}

	@action
	newProgram = async () => {
		const fetch = await this.fetchData({
			path: '/programs/newProgram',
			method: 'get',
		})
		const data: MyProgramType = get(fetch, 'data')
		if (data) {
			this.myPrograms = this.myPrograms.concat(new MyProgramStore(data))
		}
	}

	@action
	deletePrograms = async (editedPrograms: string[], isCheckedPrograms: boolean[]) => {
		const fetch = await this.fetchData({
			path: '/programs/deletes',
			method: 'post',
			body: { myProgramsId: editedPrograms },
		})
		const data: string[] = get(fetch, 'data')
		if (data) {
			const filteredMyProgramStores = this.myPrograms.filter(myprogram => !data.includes(myprogram.id))
			this.myPrograms = filteredMyProgramStores
		}
	}
}

export default UserStore
