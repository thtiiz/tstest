import { fullHeight } from 'utils'
import styled from 'styled-components'
interface Props {
	isComplete: boolean;
}

const BtnColor = (isComplete: boolean) => (isComplete ? '#B6B6B6' : '#f7522a')

export const OverallContainer = styled.div`
	background-color: white;
	${fullHeight};
`

export const CompleteBtn = styled.div`
	width: 100%;
	height: 54px;
	background: ${({ isComplete }: Props) => BtnColor(isComplete)};
	font-size: 20px;
	letter-spacing: 0.05em;
	color: #ffffff;
	padding: 14px;
	cursor: pointer;
	position: absolute;
	bottom: 0;
	cursor: pointer;
`

const Button = styled.div`
	cursor: pointer;
	width: 100%;
	height: 50px;
	display: flex;
	align-items: center;
	justify-content: center;
`
export const Line = styled.div`
	height: 2px;
	background-color: #f7522a;
	margin-top: 23.5px;
	margin-bottom: 23.5px;
`
export const RemoveButton = styled(Button)`
	background-color: #f7522a;
	color: #ffffff;
`
export const CancelButton = styled(Button)`
	background-color: #ffffff;
	color: #f7522a;
`

export const Edit = styled.div`
	width: 100%;
	height: 50px;
	display: flex;
	bottom: 0;
	position: fixed;
	font-size: 18px;
	letter-spacing: 0.05em;
	justify-content: flex-end;
	border-top: solid 2px #f7522a;
`
