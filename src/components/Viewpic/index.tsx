import React from 'react'
import { BackdropView, ViewContainer, Picture, AllCover, CrossClose } from './styled'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

interface Props {
	img: string;
	onClick: () => void;
}

export const ViewPic = (props: Props) => {
	const { img, onClick } = props
	return (
		<AllCover>
			<BackdropView onClick={onClick}></BackdropView>
			<CrossClose icon={faTimes}></CrossClose>
			<ViewContainer>
				<Picture src={img}></Picture>
			</ViewContainer>
		</AllCover>
	)
}
