import styled from 'styled-components'

export const Box = styled.div`
	position: relative;
	display: flex;
	margin-top: 8px;
	margin-bottom: 8px;
`
export const Detail = styled.span`
	margin-left: 16px;
	margin-top: 4px;
`
