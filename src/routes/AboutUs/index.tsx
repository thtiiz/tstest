import React from 'react'
import Footer from 'components/Footer'
import { Background, Redline, About, Qoute, DQoute, Qoutebox, Conbox, WePic, Name, Gitbox } from './styled'
import BackAboutUs from 'components/images/BackAboutUs.jpg'
import Conleft from 'components/images/Conleft.svg'
import Conright from 'components/images/Conright.svg'
import Conleftbot from 'components/images/Conleftbot.svg'
import Conrightbot from 'components/images/Conrightbot.svg'
import Ford from 'components/images/Ford.jpg'
import Opor from 'components/images/Opor.jpg'
import Frong from 'components/images/Frong.jpg'
import Proud from 'components/images/Proud.jpg'
import Aum from 'components/images/Aum.jpg'
import withScrollTop from 'utils/withScrollTop'
import github from 'components/images/github.svg'
import gitlab from 'components/images/gitlab.svg'

const AboutUs = () => {
	return (
		<div style={{ backgroundColor: 'white' }}>
			<Background src={BackAboutUs} />
			<About>ABOUT US</About>
			<Redline />
			<Qoutebox>
				<DQoute>“</DQoute>
			</Qoutebox>
			<Qoutebox>
				<Qoute>Reading is to the mind what</Qoute>
			</Qoutebox>
			<Qoutebox>
				<Qoute style={{ marginRight: '40px' }}>
					<span style={{ color: '#e0261b' }}>exercise</span> is to the body.
				</Qoute>
			</Qoutebox>
			<Conbox>
				<img src={Conleft} alt={`Conleft-${Conbox}`} />
				<img src={Conright} alt={`Conright-${Conbox}`} style={{ marginLeft: '265px' }} />
			</Conbox>
			<div style={{ marginTop: '15px', fontSize: '24px', fontWeight: 'bold' }}>Constructor</div>
			<WePic src={Opor} />
			<Name>Thitiwat Naprom</Name>
			<Gitbox>
				<a href="https://gitlab.com/thtiiz">
					<img src={gitlab} alt="gitlab" />
				</a>
				<a href="https://github.com/thtiiz">
					<img src={github} alt="github" />
				</a>
			</Gitbox>
			<WePic src={Ford} />
			<Name>Theerathat Nakrong</Name>
			<Gitbox>
				<a href="https://gitlab.com/fordtheerathat">
					<img src={gitlab} alt="gitlab" />
				</a>
				<a href="https://github.com/fordtheerathat">
					<img src={github} alt="github" />
				</a>
			</Gitbox>
			<WePic src={Frong} />
			<Name>Sivakorn Thaitae</Name>
			<Gitbox>
				<a href="https://gitlab.com/frongso">
					<img src={gitlab} alt="gitlab" />
				</a>
				<a href="https://github.com/frongso">
					<img src={github} alt="github" />
				</a>
			</Gitbox>
			<WePic src={Aum} />
			<Name>Noraset Potong</Name>
			<Gitbox>
				<a href="https://gitlab.com/aum0056">
					<img src={gitlab} alt="gitlab" />
				</a>
				<a href="https://github.com/aum0056">
					<img src={github} alt="github" />
				</a>
			</Gitbox>
			<WePic src={Proud} />
			<Name>Pandha Pramotekul</Name>
			<Gitbox>
				<a href="https://gitlab.com/proudpdd">
					<img src={gitlab} alt="gitlab" />
				</a>
				<a href="https://github.com/proudpdd">
					<img src={github} alt="github" />
				</a>
			</Gitbox>
			<Conbox>
				<img src={Conleftbot} alt={`Conleft-${Conbox}`} />
				<img src={Conrightbot} alt={`Conright-${Conbox}`} style={{ marginLeft: '265px' }} />
			</Conbox>
			<div style={{ color: '#979797' }}>
				<div style={{ marginTop: '107px', fontSize: '18px' }}>
					@Kasetsart University <div>Project of Software Engineering</div> subject
				</div>
				<div style={{ marginTop: '40px', fontSize: '24px' }}>Special Thanks !!!</div>
				<div style={{ marginTop: '18px' }}>license</div>
				<div style={{ marginTop: '18px' }}>CC-BY-SA 3</div>
				<div>CC-BY-SA 4</div>
				<div>CC0</div>
			</div>
			<p style={{ paddingBottom: '30px' }}>
				<b>
					<a href="wger.de/api/v2">wger.de/api/v2</a>
				</b>
			</p>
			<Footer />
		</div>
	)
}

export default withScrollTop(AboutUs)
