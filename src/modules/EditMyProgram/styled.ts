import InputText from 'components/InputText'
import styled, { css } from 'styled-components'

const styleInput = css`
	input {
		width: 100%;
		height: 30px;
		line-height: 30px;
		font-size: 20px;
		border: 0;
		background: none;
		border-bottom: 1px solid #ccc;
		outline: none;
		border-radius: 0;
		&:focus,
		&:valid {
			& ~ label {
				color: black;
				transform: translateY(-20px);
				font-size: 10px;
				cursor: default;
			}
		}
	}
`

export const EditProgramName = styled(InputText)`
	min-width: 50%;
	max-width: 80%;
	${styleInput};
`

export const EditDescription = styled.textarea`
	width: 89.33%;
	height: 80px;
	resize: none;
	::-webkit-scrollbar {
		display: none;
	}
	border: none;
	padding-bottom: 4px;
	border-bottom: 2px solid #ccc;
	display: block;
`

export const DescriptionTitle = styled.label`
	display: block;
	font-size: 10px;
	margin-bottom: 8px;
`

export const HeaderText = styled.div`
	font-size: 20px;
	font-weight: bold;
	text-align: left;
	padding: 20px 0px 0px 20px;
	> div:not(:last-child) {
		margin-bottom: 20px;
	}
	padding-right: 20px;
`

const Button = styled.div`
	cursor: pointer;
	width: 100%;
	height: 50px;
	display: flex;
	align-items: center;
	justify-content: center;
`

export const ApplyButton = styled(Button)`
	background-color: #f7522a;
	color: #ffffff;
`
export const CancelButton = styled(Button)`
	background-color: #ffffff;
	color: #f7522a;
`

export const Edit = styled.div`
	display: flex;
	position: fixed;
	bottom: 0;
	width: 100vw;
	font-size: 18px;
	border-top: solid 2px #f7522a;
`
