import React, { useEffect } from 'react'
import {
  ContentBlackBox,
  MainText,
  MainImgBox,
  BlackBox,
  WhiteBox,
  OrangeBox,
  PinkBox,
  Button,
  ContentText,
  ContentWhiteBox,
  Picture,
  PicBehind,
  PicInWhiteBox,
  ContentPinkBox,
  SlideBlackBox,
  BacktoTopBox,
  UpArrow,
  FooterBox,
  WhiteContentText,
} from 'routes/LandingPage/styled'
import history from 'core/history'
import AOS from 'aos'
import 'aos/dist/aos.css'
import chevronup from 'components/images/chevron-left.svg'
import Carousellanding from 'components/Carousellanding'
import Footer from 'components/Footer'
import { CDN } from 'core/config'
import { firstpic, backpic, picinwhitebox, picwithcom } from './config'

const onClickSignUp = () => {
  history.push({ pathname: `/signup` })
}

const onClickStartNow = () => {
  history.push({ pathname: `/home` })
}

const onClickTryItNow = () => {
  history.push({ pathname: `/myprograms` })
}

const scrollToTop = () => {
  window.scrollTo({
    top: 0,
    behavior: 'smooth',
  })
}

const LandingPage = () => {
  useEffect(() => {
    AOS.init()
  })
  return (
    <div style={{ backgroundColor: '#ffffff' }}>
      <MainImgBox
	data-aos="fade-in"
	data-aos-duration="2000"
	style={{ backgroundImage: `url(${CDN + firstpic})` }}
      ></MainImgBox>
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <MainText
	data-aos="fade-right"
	data-aos-anchor="#example-anchor"
	data-aos-offset="500"
	data-aos-duration="2000"
        >
          “BUILD THE BEST BODY WITH US”
				</MainText>
      </div>
      <BlackBox>
        <ContentBlackBox>
          <div style={{ display: 'flex' }}>
            <PicBehind src={CDN + backpic} alt="cls"></PicBehind>
            <Picture />
          </div>
          <div style={{ justifyItems: 'center' }}>
            <ContentText style={{ color: '#ffffff' }}>Be Our Family</ContentText>
            <Button onClick={onClickSignUp}>Sign up</Button>
          </div>
        </ContentBlackBox>
        <OrangeBox />
        <WhiteBox />
        <ContentWhiteBox>
          <div>
            <WhiteContentText>It</WhiteContentText>
            <ContentText style={{ color: '#f7522a' }}>
              <span>Chance</span>
            </ContentText>
            <WhiteContentText>
              To <span style={{ color: '#f7522a' }}>Change</span>
            </WhiteContentText>
            <Button onClick={onClickStartNow}>Start now</Button>
          </div>
          <PicInWhiteBox style={{ backgroundImage: `url(${CDN + picinwhitebox})` }} />
        </ContentWhiteBox>
      </BlackBox>
      <PinkBox />
      <ContentPinkBox>
        <PicInWhiteBox
	data-aos="fade-in"
	data-aos-duration="1500"
	style={{ backgroundImage: `url(${CDN + picwithcom})` }}
        />
        <div style={{ overflowX: 'hidden' }}>
          <ContentText data-aos="fade-right" data-aos-offset="500" data-aos-duration="1500">
            Manage
					</ContentText>
          <ContentText data-aos="fade-left" data-aos-offset="500" data-aos-duration="1500">
            Your own
					</ContentText>
          <ContentText data-aos="fade-right" data-aos-offset="500" data-aos-duration="1500">
            programs
					</ContentText>
          <Button onClick={onClickTryItNow}>Try it now !!</Button>
        </div>
      </ContentPinkBox>
      <SlideBlackBox>
        <Carousellanding />
      </SlideBlackBox>
      <BacktoTopBox onClick={scrollToTop}>
        <UpArrow style={{ backgroundImage: `url(${chevronup})` }} />
        <div style={{ cursor: 'pointer' }}>Back to top</div>
      </BacktoTopBox>
      <FooterBox>
        <Footer />
      </FooterBox>
    </div>
  )
}
export default LandingPage
