import { Btn } from 'components/Button/styled'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled, { keyframes } from 'styled-components'

interface IsOpenType {
	isOpenFilter?: boolean;
	isClickChoice?: boolean;
}

const filterOpen = keyframes`
	from{
		opacity: 0;
	}
	to{
		opacity: 1;
	}
`

const backdropOpen = keyframes`
	from{
		opacity: 0;
	}
	to{
		opacity: 0.2;
	}
`

export const Backdrop = styled.div`
	height: 100vh;
	width: 100vw;
	position: fixed;
	top: 0;
	left: 0;
	z-index: 3;
	opacity: 0.2;
	pointer-events: ${({ isOpenFilter }: IsOpenType) => (isOpenFilter ? 'auto' : 'none')};
	background-color: black;
	animation: ${backdropOpen} 0.2s ease-out;
`

export const Coverall = styled.div`
	z-index: 2;
`
export const FilterContainer = styled.div`
	animation: ${filterOpen} 0.1s ease-out;
	min-height: 561px;
	min-width: 325px;
	background-color: #fafafa;
	position: fixed;
	margin-top: 30vh;
	left: 50%;
	transform: translate(-50%, -50%);
	border-radius: 4px;
	padding: 18px;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	border-radius: 4px;
	z-index: 4;
`

export const Header = styled.div`
	text-align: left;
	width: 285px;
	height: 36px;
	top: 14px;
	left: 21px;
	border-bottom: 1px solid #b8b8b8;
	margin-top: 18px;
	margin-bottom: 20px;
`

export const TextFilter = styled.h3`
	font-style: normal;
	font-weight: normal;
	font-size: 18px;
	line-height: 23px;
	color: rgba(0, 0, 0, 0.85);
`

export const Catagory = styled.div`
	text-align: left;

	width: 285px;
	min-height: 73px;
	top: 70px;
	left: 21px;
	align-items: center;
	justify-items: center;
	border-bottom: 1px solid #b8b8b8;
	margin-bottom: 32px;
`

export const HeaderCatagory = styled.div`
	justify-content: space-between;
	display: flex;
`

export const TextCatagory = styled.h3`
	font-style: normal;
	font-weight: normal;
	font-size: 16px;
	line-height: 18px;
	margin-right: 5px;
	text-align: left;
	color: rgba(0, 0, 0, 0.85);
	border-radius: 4px;
	margin-bottom: 9px;
`

export const ResetCategory = styled.h3`
	margin-right: 5px;
	text-align: left;
	font-size: 14px;
	line-height: 18px;
	color: #f7522a;
	border-radius: 4px;
	margin-bottom: 9px;
	cursor: pointer;
	text-decoration: underline;
	font-style: normal;
	font-weight: 300;
`

export const Choicecontainer = styled.div`
	text-align: left;
	height: 261px;
	background: #fafafa;
`

export const ChoiceHeader = styled(Btn)`
	font-size: 16px;
	color: rgba(0, 0, 0, 0.85);
	margin-bottom: 12px;
	width: 143px;
	height: 32px;
	background: #fafafa;
	border-radius: 0px;
	border-bottom: solid 4px;
	border-color: ${({ isClickChoice }: IsOpenType) => (isClickChoice ? '#f7522a' : '#bfbfbf')};
	outline: none;
	:active {
		color: white;
	}
`

export const Close = styled(FontAwesomeIcon)`
	position: absolute;
	top: 17px;
	right: 23px;
	margin-bottom: 12px;
	cursor: pointer;
`

export const BtnApply = styled(Btn)`
	margin-top: 25px;
	padding: 15px 20px 15px 20px;
	cursor: pointer;
	transition: 0.5;
`
export const ChoiceSelecter = styled.div`
	display: flex;
`
export const OverFlowChoice = styled.div`
	overflow-y: scroll;
	width: 100%;
	height: 225px;
`
