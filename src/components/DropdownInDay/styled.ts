import styled from 'styled-components'

export const Box = styled.div`
	height: 80px;
	width: 335px;
	background-color: #f7522a;
	color: #ffffff;
	display: flex;
	border-bottom-left-radius: 4px;
	border-bottom-right-radius: 4px;
`
export const Rightbox = styled.div`
	width: 50%;
	height: 80px;
`
export const Leftbox = styled(Rightbox)`
	width: 50%;
	height: 80px;
	display: flex;
	align-items: center;
	justify-content: center;
`
export const Righttopbox = styled.div`
	height: 50%;
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 14px;
	border-bottom-left-radius: 4px;
	border-bottom-right-radius: 4px;
`
export const Rightbotbox = styled(Righttopbox)``
export const ImgBox = styled.div`
	height: 90%;
	width: 90%;
	background-repeat: no-repeat;
	background-position: center center;
	background-size: auto 90%;
`
