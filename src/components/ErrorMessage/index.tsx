import React from 'react'
import { Container, Text } from './styled'

interface Props {
    message: string
}

const ErrorMessage = ({ message }: Props) => {
    return (
        <Container>
            <Text>{message}</Text>
        </Container>
    )
}

export default ErrorMessage
