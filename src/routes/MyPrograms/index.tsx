import React, { useState } from 'react'
import {
  Detail, Line, SecondText, Container
} from './styled'
import add from 'components/images/add.svg'
import UserStore from 'modules/stores/UserStore'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import ProgramTab from 'components/ProgramTab'
import MyProgramsList from 'modules/MyProgramsList'
import { MyProgramType } from 'modules/stores/types'
import withAuth from 'utils/withAuth'
import { STATES } from 'core/api/FetchData'
import { Spinner } from 'components/Spinner'
import { get } from 'lodash'
import history from 'core/history'
import MyProgramsHeader, { HEADER_STATES } from 'components/ProgramsHeader'
import NewButton from 'components/NewButton'
import SuggestProgramList from 'modules/SuggestProgramList'
import MediaQuery from 'react-responsive'

interface StoreProps {
  name: string
  fetchState: string
  newProgram: () => void
  myProgramsStatus: (status: number) => MyProgramType[]
  deletePrograms: (editedPrograms: string[]) => void
}

const renderAddIcon = (isAddProgram: boolean, newProgram: () => void, fetchState: string) => {
  return isAddProgram ? (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Line style={{ width: '81.33%' }} />
      {fetchState === STATES.LOADED
        ? <NewButton onClick={newProgram} src={add} alt="add" /> : <Spinner />}
      <Line style={{ width: 'calc(18.67% - 50px)' }} />
    </div>
  ) : (
      <Line style={{ width: '100px', margin: '15px 0px 15px 20px' }} />
    )
}

const MyPrograms = (props: StoreProps) => {
  const { newProgram, name, myProgramsStatus, deletePrograms, fetchState } = props
  const [tabState, setTabState] = useState(1)
  const [isEdit, setIsEdit] = useState(false)
  const isAddProgram = get(history, ['location', 'state', 'isAddProgram'], false)
  const isSuggestProgram = get(history, ['location', 'state', 'isSuggestProgram'], false)
  const toggleIsEdit = () => { setIsEdit(!isEdit) }
  const renderProgramTabs = () => {
    return isAddProgram ? (
      <SecondText>My programs</SecondText>
    ) : (
      <MediaQuery minDeviceWidth={0}>
        <MediaQuery maxDeviceWidth={767}>
        <div style={{ display: 'flex' }}>
          <ProgramTab isClick={tabState === 1} onClick={setTabState} tabId={1} name="Inactive" />
          <ProgramTab isClick={tabState === 2} onClick={setTabState} tabId={2} name="Active" />
          <ProgramTab isClick={tabState === 3} onClick={setTabState} tabId={3} name="Completed" />
        </div>
        </MediaQuery>
        <MediaQuery minDeviceWidth={768}>
        <div style={{ display: 'flex' }}>
          <ProgramTab isClick={tabState === 1} onClick={setTabState} tabId={1} name="Inactive Programs" />
          <ProgramTab isClick={tabState === 2} onClick={setTabState} tabId={2} name="Active Programs" />
          <ProgramTab isClick={tabState === 3} onClick={setTabState} tabId={3} name="Completed Programs" />
        </div>
        </MediaQuery>
        </MediaQuery>
      )
  }
  return (
    <Container>
      <MyProgramsHeader numHeader={HEADER_STATES.MYPROGRAMS} title={name} fetchState={fetchState}
	isAddProgram={isAddProgram || isSuggestProgram} isEdit={isEdit} toggleIsEdit={toggleIsEdit} />
      <Detail>
        {isSuggestProgram ? 'This’s our suggest planning'
          : 'Let’s complete your planning'
        }
      </Detail>
      {renderAddIcon(isAddProgram, newProgram, fetchState)}
      {!isSuggestProgram && renderProgramTabs()}
      {isSuggestProgram ? <SuggestProgramList />
        : <MyProgramsList myPrograms={myProgramsStatus(tabState)}
	isEdit={isEdit} toggleIsEdit={toggleIsEdit} deletePrograms={deletePrograms} />}
    </Container>
  )
}
interface Store {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  inject(({ userStore }: Store) => ({
    name: userStore.name,
    myProgramsStatus: userStore.myProgramsStatus,
    newProgram: userStore.newProgram,
    deletePrograms: userStore.deletePrograms,
    fetchState: userStore.state
  })),
  observer,
)(MyPrograms)
