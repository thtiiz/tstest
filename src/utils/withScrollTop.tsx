import React, { useEffect } from 'react'

const withScrollTop = (WrappedComponent: any) => (safeProps: any) => {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  })
  const Wrapped = (
    <WrappedComponent {...safeProps} />
  )
  return Wrapped
}

export default withScrollTop
