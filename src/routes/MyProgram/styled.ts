import { Button } from 'components/NewButton'
import { fullHeight } from 'utils'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const Container = styled.div`
	background-color: white;
	${fullHeight};
`

export const Detail = styled.div`
	font-size: 14px;
	text-align: left;
	margin: 15px 0px 0px 20px;
	width: 89.33%;
	max-height: 160px;
	::-webkit-scrollbar {
		display: none;
	}
	overflow-y: scroll;
	overflow-wrap: break-word;
`
export const Line = styled.div`
	height: 2px;
	width: 100vw;
	background-color: #f7522a;
`

export const DoneButton = styled.div`
	width: 100%;
	color: #ffffff;
	font-size: 20px;
	position: fixed;
	z-index: 3;
	bottom: 0px;
	background-color: #00ad89;
	padding: 14px 160.5px 14px 160.5px;
`

export const ActionSection = styled.div`
	display: flex;
	align-items: center;
`

export const ProgramAction = styled(FontAwesomeIcon)`
	margin: 15px 10px 10px 10px;
`

export const StartButton = styled.div`
	${Button};
`
