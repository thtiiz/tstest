import React from 'react'
import { searchStyled, CreateText, Highlight } from './styled'
import Creatable from 'react-select/creatable'
import { ValueType } from 'react-select/src/types'
import { Option } from './types'

interface Props {
  options: any
  value: string
  isLoading: boolean
  onFocus: () => void
  handleSearch: (value: ValueType<Option>) => void
  onInputChange: (value: string) => void
}

const createText = (value: string) => <CreateText>
  {`Search for `}<Highlight>{value}</Highlight>
</CreateText>

const SearchInput = ({ value, options, handleSearch, onFocus, onInputChange, isLoading }: Props) => {
  return (
    <Creatable
	isClearable
	isLoading={isLoading}
	onFocus={onFocus}
	placeholder='Search...'
	options={options}
	inputValue={value}
	onChange={handleSearch}
	onInputChange={onInputChange}
	formatCreateLabel={createText}
	components={
        {
          IndicatorsContainer: () => null,

        }
      }
	styles={searchStyled}
    />
  )
}

export default SearchInput
