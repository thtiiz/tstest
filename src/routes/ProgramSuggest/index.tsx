import React, { useState } from 'react'
import { compose } from 'recompose'
import { ProgramType, DayType, MyProgramType } from 'modules/stores/types'
import { inject, observer } from 'mobx-react'
import SuggestProgramStore from 'modules/stores/SuggestProgramStore'
import { useParams } from 'react-router'
import { get } from 'lodash'
import {
  Day, Container, DayWrapper, Username, ProgramLine,
  Line, CopyIcon, DayText, StyleSpinner, CopySection, CompleteWrapper, UserBox
} from './styled'
import MyProgramsHeader, { HEADER_STATES } from 'components/ProgramsHeader'
import history from 'core/history'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy, faCheck } from '@fortawesome/free-solid-svg-icons'
import { STATES } from 'core/api/FetchData'
import UserStore from 'modules/stores/UserStore'
import CompleteCopy from 'components/CompleteCopy'
import withAuth from 'utils/withAuth'
import CoverImg from 'components/CoverImg'

interface StoreProps {
  programId: (id: string) => ProgramType
  state: string
  copyProgram: (programId: string) => MyProgramType
  addMyProgram: (myProgram: MyProgramType) => void
}

const ProgramSuggest = ({ programId, state, copyProgram, addMyProgram }: StoreProps) => {
  const id = get(useParams(), 'id', '')
  const program = programId(id)
  const days: DayType[] = get(program, 'days', [])
  const image: string = get(program, 'image')
  const programName = get(program, 'name', '')
  const userName = get(program, 'ownerName', '')
  const [isSuccess, setIsSuccess] = useState(false)
  const onClickDay = (index: number) => {
    history.push({
      pathname: `/programsuggest/day/${index + 1}`,
      state: { programId: id }
    })
  }

  const onSuccess = () => {
    setIsSuccess(true)
    setTimeout(() => {
      setIsSuccess(false)
    }, 3000)
  }

  const onCopy = async () => {
    if (!isSuccess) {
      const data = await copyProgram(program.id)
      if (data) {
        onSuccess()
        addMyProgram(data)
      }
    }
  }
  const actionIcon = isSuccess ? faCheck : faCopy
  return (
    <Container>
      <CoverImg image={image} />
      <MyProgramsHeader numHeader={HEADER_STATES.PROGRAMSUGGEST} title={programName} isEdit />
      <UserBox>
        <ProgramLine />
        <div>
          <Username>{userName}</Username>
        </div>
      </UserBox>
      <CopySection>
        <Line style={{ width: '81.33%' }} />
        {state === STATES.LOADING ? <StyleSpinner />
          : <CopyIcon onClick={onCopy}>
            <FontAwesomeIcon icon={actionIcon} color="white" size="lg" />
          </CopyIcon>}
        <Line style={{ width: 'calc(18.67% - 50px)' }} />
        {isSuccess && <CompleteWrapper>
          <CompleteCopy />
        </CompleteWrapper>}
      </CopySection>
      <div style={{ marginTop: '32px' }}>
        <DayText>
          Days
        </DayText>
        {days.map((day, i) => {
          const onClick = () => { onClickDay(i) }
          return <DayWrapper key={`program-day-${i}`} >
            <Day onClick={onClick}>Day {i + 1}</Day>
          </DayWrapper>
        })
        }
      </div>
    </Container>
  )
}

interface Store {
  suggestProgramStore: SuggestProgramStore
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  inject(({ suggestProgramStore, userStore }: Store) => ({
    programId: suggestProgramStore.programId,
    state: suggestProgramStore.state,
    copyProgram: suggestProgramStore.copyProgram,
    addMyProgram: userStore.addMyProgram
  })),
  observer,
)(ProgramSuggest)
