import styled, { keyframes } from 'styled-components'
import { fullHeight } from 'utils'
import { Spinner } from 'components/Spinner'

const fadeIn = keyframes`
	0%{
		opacity: 0;
	}
	7%{
		opacity: 1;
	}
  93%{
    opacity: 1;
  }
  100%{
    opacity: 0;
  }
`

export const Container = styled.div`
	background-color: white;
	${fullHeight};
	flex-direction: column;
	> div:not(:last-child, :first-child) {
		margin-bottom: 20px;
	}
	> div:first-child {
		margin-bottom: 8px;
	}
`

export const CompleteWrapper = styled.div`
	top: 70px;
	right: calc(18.67% - 50px);
	position: absolute;
	animation: ${fadeIn} 3s ease-in-out;
`

export const StyleSpinner = styled(Spinner)`
	width: 50px;
	height: 50px;
	padding: 10px;
	border-radius: 50%;
	transform: translate(0, -50%);
	background-color: white;
`

export const Username = styled.div`
	font-size: 14px;
	text-align: left;
	padding-left: 12px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`
export const CopySection = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	position: relative;
`

export const CopyIcon = styled.div`
	font-size: 20px;
	right: 100px;
	width: 50px;
	height: 50px;
	padding: 10px;
	border-radius: 50%;
	background-color: #f7522a;
	cursor: pointer;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`

export const DayText = styled.div`
	font-size: 20px;
	font-weight: bold;
	text-align: left;
	margin: 0 0 20px 20px;
`

export const Line = styled.div`
	height: 2px;
	width: 100%;
	background-color: #f7522a;
`

export const ProgramLine = styled.div`
	border-left: 2px solid #f7522a;
	height: 34px;
	margin-left: 20px;
`

export const DayWrapper = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;
	margin-bottom: 20px;
	align-items: center;
`

export const Day = styled.div`
	background-color: #f7522a;
	width: 335px;
	height: 43px;
	border-radius: 10px;
	text-align: left;
	padding: 10px 20px 10px 20px;
	color: #ffffff;
	font-size: 18px;
	text-overflow: ellipsis;
	overflow: hidden;
	cursor: pointer;
`
export const UserBox = styled.div`
	display: flex;
`
