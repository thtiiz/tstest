import React, { useEffect } from 'react'
import '@brainhubeu/react-carousel/lib/style.css'
import {
  WhiteBox,
  OrangeBox,
  Box,
  Pic,
  LearnBox,
  ArrowBox,
  ContentBox,
  DesktopBox,
  Button,
  Header,
  ContextMobileBox,
} from './styled'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'
import compress from 'components/images/compress.svg'
import dumbell from 'components/images/dumbell.svg'
import wave from 'components/images/wave.svg'
import muscle from 'components/images/muscle.svg'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'
import MediaQuery from 'react-responsive'
import history from 'core/history'
import AOS from 'aos'
import 'aos/dist/aos.css'
import { CDN } from 'core/config'
import { man1, girl1, man2, girl2 } from './config'

const responsive = {
  mobile: {
    breakpoint: { max: 3000, min: 0 },
    items: 1,
    partialVisibilityGutter: 153, // this is needed to tell the amount of px that should be visible.
  },
}

const Carousellanding = () => {
  useEffect(() => {
    AOS.init()
  })
  const onClickAboutUs = () => {
    history.push({ pathname: `/aboutus` })
  }

  return (
    <MediaQuery minDeviceWidth={0}>
      <MediaQuery maxDeviceWidth={1366}>
        <Carousel
	additionalTransfrom={0}
	arrows
	centerMode={false}
	containerClass="container"
	dotListClass=""
	draggable
	focusOnSelect={false}
	infinite={false}
	itemClass="image-item"
	keyBoardControl
	minimumTouchDrag={80}
	renderDotsOutside={false}
	responsive={responsive}
	showDots={false}
	sliderClass=""
	slidesToSlide={1}
	swipeable
	partialVisbile={true}
        >
          <Box>
            <ContentBox>
              <WhiteBox>
                <OrangeBox src={compress} />
                <ContextMobileBox>
                  <div>
                    <Header>Create your personalized exercise</Header>
                    <MediaQuery minDeviceWidth={587}>
                      You’ll be able to manage your exercise, add new programs, and more.
										</MediaQuery>
                  </div>
                </ContextMobileBox>
              </WhiteBox>
              <Pic src={CDN + man1} alt="man1" />
            </ContentBox>
          </Box>
          <Box>
            <ContentBox>
              <Pic src={CDN + girl1} alt="girl1" />
              <WhiteBox>
                <OrangeBox src={dumbell} />
                <ContextMobileBox>
                  <div>
                    <Header>Exercise postures for every level</Header>
                    <MediaQuery minDeviceWidth={587}>
                      We want every person has a good health. That’s why we offer the exercise for every level
											person.
										</MediaQuery>
                  </div>
                </ContextMobileBox>
              </WhiteBox>
            </ContentBox>
          </Box>
          <Box>
            <ContentBox>
              <WhiteBox>
                <OrangeBox src={wave} />
                <ContextMobileBox>
                  <div>
                    <Header>Find programs from suggestion</Header>
                    <MediaQuery minDeviceWidth={587}>
                      You can manage your programs from other user’s programs which published.
										</MediaQuery>
                  </div>
                </ContextMobileBox>
              </WhiteBox>
              <Pic src={CDN + man2} alt="man2" />
            </ContentBox>
          </Box>
          <Box>
            <ContentBox>
              <Pic src={CDN + girl2} alt="girl2" />
              <WhiteBox>
                <OrangeBox src={muscle} />
                <ContextMobileBox>
                  <div>
                    <Header>Achieve your perfect body</Header>
                    <MediaQuery minDeviceWidth={587}>
                      We have techniques, tips and more for help you to achieve your goal.
										</MediaQuery>
                  </div>
                </ContextMobileBox>
              </WhiteBox>
            </ContentBox>
          </Box>
          <Box>
            <LearnBox onClick={onClickAboutUs}>
              <div>
                <div>Learn More About Us</div>
                <ArrowBox icon={faArrowRight} />
              </div>
            </LearnBox>
          </Box>
        </Carousel>
      </MediaQuery>
      <MediaQuery minDeviceWidth={1367}>
        <div style={{ backgroundColor: '#20212a', paddingBottom: '84px' }}>
          <DesktopBox>
            <Box>
              <ContentBox data-aos="fade-right" data-aos-offset="500" data-aos-duration="1500">
                <WhiteBox>
                  <OrangeBox src={compress} />
                  <div>
                    <Header>Create your personalized exercise</Header>
                    You’ll be able to manage your exercise, add new programs, and more.
									</div>
                </WhiteBox>
                <Pic src={man1} alt="man1" />
              </ContentBox>
            </Box>
            <Box>
              <ContentBox data-aos="fade-right" data-aos-offset="500" data-aos-duration="2000">
                <Pic style={{ backgroundImage: `url(${CDN + girl1})` }} />
                <WhiteBox>
                  <OrangeBox src={dumbell} />
                  <div>
                    <Header>Exercise postures for every level</Header>
                    We want every person has a good health. That’s why we offer the exercise for every level
										person.
									</div>
                </WhiteBox>
              </ContentBox>
            </Box>
            <Box>
              <ContentBox data-aos="fade-right" data-aos-offset="500" data-aos-duration="2500">
                <WhiteBox>
                  <OrangeBox src={wave} />
                  <div>
                    <Header>Find programs from suggestion</Header>
                    You can manage your programs from other user’s programs which published.
									</div>
                </WhiteBox>
                <Pic style={{ backgroundImage: `url(${man2})` }} />
              </ContentBox>
            </Box>
            <Box>
              <ContentBox data-aos="fade-right" data-aos-offset="500" data-aos-duration="3000">
                <Pic style={{ backgroundImage: `url(${girl2})` }} />
                <WhiteBox>
                  <OrangeBox src={muscle} />
                  <div>
                    <Header>Achieve your perfect body</Header>
                    We have techniques, tips and more for help you to achieve your goal.
									</div>
                </WhiteBox>
              </ContentBox>
            </Box>
          </DesktopBox>
          <Button onClick={onClickAboutUs}>About Us</Button>
        </div>
      </MediaQuery>
    </MediaQuery>
  )
}

export default Carousellanding
