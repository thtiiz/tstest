import React, { useState, useEffect } from 'react'
import {
  OverallContainer,
  CompleteBtn,
  Edit,
  CancelButton,
  RemoveButton,
  Line
} from './styled'
import CompleteAlert from 'modules/CompleteAlert/index'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import { DayExerciseType } from 'modules/stores/types'
import withAuth from 'utils/withAuth'
import { get } from 'lodash'
import { useParams } from 'react-router'
import history from 'core/history'
import MyProgramStore from 'modules/stores/MyProgramStore'
import UserStore from 'modules/stores/UserStore'
import MyProgramsHeader, { HEADER_STATES } from 'components/ProgramsHeader'
import RenderExercises from 'components/RenderExercises'
import withScrollTop from 'utils/withScrollTop'

interface StoreProps {
  myProgramStoreId: (id: string) => MyProgramStore
}

const PostInDay = (props: StoreProps) => {
  const { myProgramStoreId } = props
  const { myProgramId, dayId } = get(history, ['location', 'state'], {})
  const myProgramStore = myProgramStoreId(myProgramId)
  const { deleteExercises, initialEditState, completeDay,
    isCheckedElements, clickElementOnDelete, status, state } = myProgramStore
  const day = myProgramStore.dayId(dayId)
  const dayExercises: DayExerciseType[] = get(day, 'exercises', [])
  const numDay = get(useParams(), 'numDay')
  const [isEdit, setIsEdit] = useState(false)
  const [isOpenCompleteAlert, setIsOpenCompleteAlert] = useState(false)
  const renderHeader = () => {
    const isRenderEdit = status === 1 ? isEdit : true
    return < MyProgramsHeader numHeader={HEADER_STATES.POSTINDAY}
	title={numDay} fetchState={state}
	isEdit={isRenderEdit}
	toggleIsEdit={toggleIsEdit} />
  }
  const toggleIsEdit = () => {
    setIsEdit(!isEdit)
  }

  const onCancel = () => {
    toggleIsEdit()
    initialEditState(dayExercises.length)
  }

  const onRemove = () => {
    toggleIsEdit()
    deleteExercises(dayId)
    initialEditState(dayExercises.length)
  }

  const toggleAlert = () => setIsOpenCompleteAlert(!isOpenCompleteAlert)

  const isComplete = get(day, 'isComplete')

  const onComplete = () => { completeDay(day.id) }

  useEffect(() => {
    const dayExercises: DayExerciseType[] = get(day, 'exercises', [])
    const { length } = dayExercises
    initialEditState(length)
  }, [day, initialEditState])

  return (
    <OverallContainer>
      {renderHeader()}
      <Line />
      <RenderExercises isEdit={isEdit} isCheckedElements={isCheckedElements}
	clickElementOnDelete={clickElementOnDelete} dayExercises={dayExercises} />
      {!isComplete && isEdit && (
        <Edit>
          <CancelButton onClick={onCancel}>Cancel</CancelButton>
          <RemoveButton onClick={onRemove}>Remove</RemoveButton>
        </Edit>
      )}
      {status === 2 &&
        <CompleteBtn onClick={toggleAlert} isComplete={isComplete}>COMPLETE</CompleteBtn>}
      {isOpenCompleteAlert &&
        <CompleteAlert toggleAlert={toggleAlert} onComplete={onComplete} isOpen={isOpenCompleteAlert} />}
    </OverallContainer>
  )
}

interface Stores {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  withScrollTop,
  inject(({ userStore }: Stores) => ({
    myProgramStoreId: userStore.myProgramId,
  })),
  observer
)(PostInDay)
