import { Fetch } from './type'
import axios from 'axios'
import qs from 'querystring'

const option = {
	baseURL: 'https://swbackend.herokuapp.com/api/',
	// baseURL: 'https://exersite-server.tk/api/',
	timeout: 20000,
	header: {
		'Content-Type': 'application/json',
	},
}

class Client {
	client: any

	constructor() {
		this.client = axios.create(option)
		this.client.interceptors.request.use(
			async (config: any) => {
				const jwtToken = await localStorage.getItem('token')
				if (jwtToken != null) {
					config.headers = { 'Content-Type': 'application/json', Authorization: 'Bearer ' + jwtToken }
				}
				return config
			},
			(error: any) => {
				console.log(error)
				return Promise.reject(error)
			},
		)
	}

	serializeParams(params: any) {
		return qs.stringify(params)
	}

	fetch({ headers = {}, method, body = {}, params = {}, path }: Fetch) {
		const request = {
			url: path,
			data: body,
			headers,
			method,
			params,
			paramsSerializer: this.serializeParams,
			validateStatus: false,
		}
		return this.client(request).then((res: any) => res.data)
	}
}

export default Client
