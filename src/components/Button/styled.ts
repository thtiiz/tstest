import styled from 'styled-components'
interface BtnTypes {
	color?: string;
	secondary?: boolean;
}

const btnColor = ({ color, secondary }: BtnTypes) => {
	return color || (secondary ? 'black' : '#F7522A')
}

export const Btn = styled.button`
	border-radius: 4px;
	min-width: 40px;
	background-color: ${btnColor};
	color: white;
	border: none;
	padding: 4px 8px 4px 8px;
	&:active {
		transition: 0.1s;
		background-color: ${({ secondary }: BtnTypes) => (secondary ? '#3b3b3b' : '#e0261b')};
	}
`
