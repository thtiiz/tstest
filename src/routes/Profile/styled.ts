import styled from 'styled-components'

interface Props {
	src?: string;
}

export const OverallContainer = styled.div`
	background-color: #c4c4c4;
	padding-bottom: 92px;
`

export const BGHeader = styled.div`
	height: 265px;
	width: 100%;
	background-image: url(${({ src }: Props) => src});
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
`
export const SettingBtn = styled.img`
	top: 90px;
	right: 29.5px;
	position: absolute;
	cursor: pointer;
`
export const ProfileContainer = styled.div`
	min-height: 837px;
	position: relative;
	margin-left: 10px;
	margin-right: 10px;
	background-color: #ffffff;
	display: flex;
	flex-direction: column;
	align-items: center;
	box-shadow: 0px 2px 24px rgba(0, 0, 0, 0.14), 0px 8px 10px rgba(0, 0, 0, 0.2);
	border-radius: 6px;
`
export const ProfilePicture = styled.div`
	background-image: url(${({ src }: Props) => src});
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	transform: translate(0, -50%);
	border-radius: 50%;
	width: 160px;
	height: 160px;
	box-shadow: 0px 16px 30px rgba(0, 0, 0, 0.3), 0px 4px 10px rgba(0, 0, 0, 0.3);
`
export const Name = styled.div`
	font-size: 25.55px;
	text-align: center;
	color: #3c4858;
	font-weight: bold;
	margin-top: -70px;
`

export const UserName = styled.div`
	font-weight: 300;
	font-size: 14px;
	line-height: 16px;
	color: #f7522a;
	margin-top: 19px;
`

export const Status = styled.div`
	font-weight: 300;
	font-size: 14px;
	line-height: 16px;
	text-align: center;
	color: #999999;
	margin-top: 28px;
`
export const SelectIcon = styled.div`
	margin-top: 21px;
	width: 253px;
	height: 84px;
	background: #f7522a;
	box-shadow: 0px 4px 20px #dadada;
	border-radius: 4px;
	text-align: center;
	justify-content: center;
	cursor: pointer;
`

export const ProgramImg = styled.img`
	margin-top: 9px;
`

export const MyProgramText = styled.div`
	font-weight: 500;
	font-size: 18px;
	line-height: 14px;
	margin-top: 12px;
	color: #ffffff;
`

export const MyProgramContainer = styled.div`
	margin-left: 20px;
	margin-right: 20px;
	min-height: 447;
	margin-top: 43px;
`
