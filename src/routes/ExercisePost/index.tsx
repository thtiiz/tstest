import React, { useEffect, Fragment } from 'react'
import {
  ExerciseDetailCard,
  ImgBox, HeaderName, DetailBoxblack, DetailBoxwhite, AddBtn, CoverText
} from './styled'
import ExerciseListContent from 'components/ExerciseLIstContent'
import { ExerciseType } from 'modules/stores/types'
import { useParams } from 'react-router'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import ExerciseStore from 'modules/stores/ExerciseStore'
import withScrollTop from 'utils/withScrollTop'
import { mapCategory, mapEquipment, mapDifficulty, mapMuscles } from 'utils/mapper'
import { joinArray } from 'utils'
import { isEmpty, get } from 'lodash'
import history from 'core/history'
interface StoreProps {
  exercise: ExerciseType
  fetchExerciseById: (id: string) => void
}

const ExercisePost = ({ exercise, fetchExerciseById }: StoreProps) => {
  const { id } = useParams()
  const hasExercise = !isEmpty(exercise)
  useEffect(() => {
    const fetchExercise = async () => {
      if (!hasExercise && id) fetchExerciseById(id)
    }
    fetchExercise()
  }, [exercise, fetchExerciseById, hasExercise, id])
  const { category, muscles, musclesSecondary, description,
    imageGif, name, equipment, difficulty, set, comments } = exercise

  const mapperMuscle = () => {
    const mapped = muscles.map((muscle, i) => (
      mapMuscles.get(muscle)
    ))
    return joinArray(mapped)
  }

  const mapperMuscleSec = () => {
    const mapped = musclesSecondary.map((muscleSecondary, i) => (
      mapMuscles.get(muscleSecondary)
    ))

    return joinArray(mapped)
  }

  const mapperEquipment = () => {
    const mapped = equipment.map((eachequipment, i) => (
      mapEquipment.get(eachequipment)
    ))
    return joinArray(mapped)
  }

  const handleAddProgram = () => {
    history.push({
      pathname: `/myprograms`,
      state: {
        isAddProgram: true,
        exerciseId: exercise.id
      }
    })
  }

  const hasAddBtn = get(history, ['location', 'state', 'hasAddBtn'], true)

  return (
    <Fragment>
      {hasExercise
        ? <ExerciseDetailCard>
          <ImgBox style={{ backgroundImage: `url(${imageGif})` }} />
          <DetailBoxblack>
            <CoverText>
              <HeaderName>{name}</HeaderName>
              <ExerciseListContent theme="black" title="Area" content={mapCategory.get(category)} />
              <ExerciseListContent theme="black" title="Muscles" content={mapperMuscle()} />
              <ExerciseListContent theme="black" title="Muscles secondary" content={mapperMuscleSec()} />
              <ExerciseListContent theme="black" title="Equipment" content={mapperEquipment()} />
              <ExerciseListContent theme="black" title="Difficulty" content={mapDifficulty.get(difficulty)} />
              <ExerciseListContent theme="black" title="Set recommend" content={set} />
            </CoverText>
          </DetailBoxblack>
          <DetailBoxwhite>
            <CoverText>
              <ExerciseListContent theme="white" title="Description" content={description || 'none'} />
              <ExerciseListContent theme="white" title="Comments" content={joinArray(comments)} />
            </CoverText>
          </DetailBoxwhite>
          {hasAddBtn && <AddBtn onClick={handleAddProgram}>ADD TO MY PROGRAM</AddBtn>}
        </ExerciseDetailCard>
        : null
      }
    </Fragment>
  )
}

interface Stores {
  exerciseStore: ExerciseStore
}

export default compose<StoreProps, {}>(
  withScrollTop,
  inject(({ exerciseStore }: Stores) => ({
    exercise: exerciseStore.exerciseClicked,
    fetchExerciseById: exerciseStore.fetchExerciseById,
  })),
  observer
)(ExercisePost)
