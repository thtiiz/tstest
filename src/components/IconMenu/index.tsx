import React from 'react'
import { MenuName, ImgLogo, SectionMenu, Select } from './styled'

interface Props {
    icon: string
    text: string
}

const IconMenu = ({ icon, text }: Props) => {
    return (
        <SectionMenu>
            <Select />
            <ImgLogo src={icon} alt="icon" />
            <MenuName>{text}</MenuName>
        </SectionMenu >
    )
}

export default IconMenu
