import styled from 'styled-components'

export const ImageWrapper = styled.div`
	margin-bottom: 10px;
`
export const TextWrapper = styled.div`
	padding-left: 15px;
	margin-top: -4px;
`
export const PicBox = styled.div`
	background-color: white;
	text-align: left;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`
