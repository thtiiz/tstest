import { fullHeight } from 'utils'
import InputText from 'components/InputText'

import styled from 'styled-components'
import Button from 'components/Button'

export const DetailText = styled.span`
    font-size: 10px;
    margin-bottom:30px;
    margin-top:-40px;
    width:300px;
    
`
export const FormContainer = styled.form`
    display:flex;
    flex-direction:column;
    align-items:center;
    text-align:left;  
`
export const Container = styled.div`
    width: 100vw;
    ${fullHeight}
    background-color: #ffffff;
    display:flex;
    flex-direction:column;

    
`
export const ErrorSection = styled.div`
    
    height: 70px;
    width: 100%;

`

export const LoginButton = styled(Button)`
    box-shadow: 0px 4px 9px rgba(0, 0, 0, 0.25);
    margin-top: 25px;
    width: 300px;
    font-size: 18px;
    padding: 14px 0px 14px 0px;
    
`
export const GenderContainer = styled.div`
    display:flex;
    flex-direction:column;
    justify-items: left;
    text-align: left;
`

export const TextGender = styled.span`
	font-size:16px;
    margin-right:16px;
    
`
export const GenderRadioButton = styled.input`
 margin: 0px 4px 0px 4px;
`
export const GenderBox = styled.div`
	margin-top: 16px;
	display:flex;
	align-items:center;
    margin-bottom: 20px;
`
export const InputTextStyle = styled(InputText)`
    margin-bottom:40px;
   
`
