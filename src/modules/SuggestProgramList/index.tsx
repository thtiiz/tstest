import React from 'react'
import { ProgramType } from 'modules/stores/types'
import { compose } from 'recompose'
import SuggestProgramStore from 'modules/stores/SuggestProgramStore'
import { inject, observer } from 'mobx-react'
import ProgramCard from 'components/ProgramCard'
import { Container } from './styled'

interface StoreProps {
  programs: ProgramType[]
}

const SuggestProgramList = ({ programs }: StoreProps) => {
  return (
    <Container>
      {programs.map((program, index) => (
        <ProgramCard key={`Program-${index}-${program.name}`} program={program} />
      ))}
    </Container>
  )
}

interface Store {
  suggestProgramStore: SuggestProgramStore
}

export default compose<StoreProps, {}>(
  inject(({ suggestProgramStore }: Store) => ({
    programs: suggestProgramStore.programs
  })),
  observer,
)(SuggestProgramList)
