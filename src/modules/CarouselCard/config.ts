export const responsive = {
	superLargeDesktop: {
		// the naming can be any, depends on you.
		breakpoint: { max: 3000, min: 2560 },
		items: 4,
		partialVisibilityGutter: 100,
	},
	fullscreen: {
		breakpoint: { max: 2559, min: 1920 },
		items: 4,
		partialVisibilityGutter: 50,
	},
	desktop1: {
		breakpoint: { max: 1919, min: 1440 },
		items: 3.5,
		partialVisibilityGutter: 50,
	},
	desktop: {
		breakpoint: { max: 1439, min: 1024 },
		items: 3,
		partialVisibilityGutter: 50,
	},
	tablet: {
		breakpoint: { max: 1023, min: 701 },
		items: 2,
		partialVisibilityGutter: 50,
	},
	mobile1: {
		breakpoint: { max: 700, min: 501 },
		items: 1.5,
		partialVisibilityGutter: 10,
	},
	mobile: {
		breakpoint: { max: 500, min: 0 },
		items: 1,
		partialVisibilityGutter: 50,
	},
}
