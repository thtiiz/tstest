import React, { Component } from 'react'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import UserStore from 'modules/stores/UserStore'
import history from 'core/history'
import { get } from 'lodash'

interface StoreProps {
  isLogin: boolean
}
interface Stores {
  userStore: UserStore
}

const withAuth = (WrappedComponent: any): any => {
  class WithAuth extends Component<any, {}> {
    render() {
      const { isLogin, isInitial, ...safeProps } = this.props
      const historyState = get(history, ['location', 'state'])
      const isSuggestProgram = get(historyState, 'isSuggestProgram', false)
      if ((isLogin || isSuggestProgram) && isInitial) {
        return <WrappedComponent {...safeProps} />
      } else if (!isLogin && isInitial) {
        history.replace({
          pathname: '/login',
          state: { ...historyState, redirect: history.location.pathname }
        })
        return null
      } else {
        return null
      }
    }
  }
  return compose<any, any>(
    inject(({ userStore }: Stores) => ({
      isLogin: userStore.isLogin,
      isInitial: userStore.isInitial
    })),
    observer)(WithAuth)
}

export default withAuth
