import styled from 'styled-components'

export const Background = styled.img`
	height: 100%;
	width: 100%;
`
export const Redline = styled.div`
	height: 5px;
	width: 100%;
	background-color: #e0261b;
`
export const About = styled.div`
	height: 63px;
	width: 100%;
	font-size: 18px;
	text-align: left;
	padding: 20px 55px;
	font-weight: bold;
`
export const DQoute = styled.div`
	width: fit-content;
	font-size: 64px;
	padding: 25px 230px 0px 0px;
	text-align: left;
`

export const Qoute = styled.div`
	width: fit-content;
	font-size: 18px;
	text-align: left;
	padding: 0px 63px 0px 78px;
`
export const Qoutebox = styled.div`
	justify-content: center;
	display: flex;
`
export const Conbox = styled.div`
	display: flex;
	padding: 90px 0px 0px 0px;
	justify-content: center;
`
export const WePic = styled.img`
	margin-top: 36px;
	height: 217px;
	width: 218px;
	border-radius: 50%;
	box-shadow: 0px 16px 30px rgba(0, 0, 0, 0.3), 0px 4px 10px rgba(0, 0, 0, 0.3);
`
export const Name = styled.div`
	margin-top: 22px;
	font-size: 24px;
`
export const Gitbox = styled.div`
	padding-top: 10px;
	display: flex;
	margin: auto;
	justify-content: space-between;
	width: 100px;
`
