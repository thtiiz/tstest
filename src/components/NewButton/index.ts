import styled, { css } from 'styled-components'

export const Button = css`
	:hover {
		background-color: #e0261b;
	}
	:active {
		background-color: #e0261b;
		box-shadow: 0 2px #e6e6e6;
		transform: translateY(2px);
	}
	cursor: pointer;
	width: 49px;
	height: 49px;
	background-color: #f7522a;
	border-radius: 50%;
	box-shadow: 3px 3px #e6e6e6;
`

const NewButton = styled.img`
	${Button};
	padding: 10px;
`

export default NewButton
