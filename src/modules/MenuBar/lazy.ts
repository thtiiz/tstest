import Loadable from 'react-loadable'

const MenuBar = Loadable({
    loader: () => import(/* webpackChunkName: "modules.MenuBar" */ '.'),
    loading: () => null,
})

export default MenuBar
