export const mapDifficulty = new Map([[1, 'Beginner'], [2, 'Intermediate'], [3, 'Expert']])

export const mapCategory = new Map([
	[8, 'Arms'],
	[9, 'Legs'],
	[10, 'Abs'],
	[11, 'Chest'],
	[12, 'Back'],
	[13, 'Shoulders'],
	[14, 'Calves'],
])

export const mapEquipment = new Map([
	[1, 'Barbell'],
	[2, 'SZ-Bar'],
	[3, 'Dumbbell'],
	[4, 'Gym mat'],
	[5, 'Swiss Ball'],
	[6, 'Pull-up bar'],
	[7, 'bodyweight'],
	[8, 'Bench'],
	[9, 'Incline bench'],
	[10, 'Kettlebell'],
])

export const mapMuscles = new Map([
	[1, 'Biceps brachii'],
	[2, 'Anterior deltoid'],
	[3, 'Serratus anterior'],
	[4, 'Pectoralis major'],
	[5, 'Triceps brachii'],
	[6, 'Rectus abdominis'],
	[7, 'Gastrocnemius'],
	[8, 'Gluteus maximus'],
	[9, 'Trapezius'],
	[10, 'Quadriceps femoris'],
	[11, 'Biceps femoris'],
	[12, 'Latissimus dorsi'],
	[13, 'Brachialis'],
	[14, 'Obliquus externus abdominis'],
	[15, 'Soleus'],
])
