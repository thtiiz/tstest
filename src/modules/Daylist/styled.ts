import styled from 'styled-components'

export const Container = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	padding-bottom: 50px;
	> div { 
		margin-bottom: 20px;
	}
`
