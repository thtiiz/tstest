import styled from 'styled-components'

export const Container = styled.div`
	background: #ffffff;
	width: 100%;
	height: 56px;
	position: sticky;
	top: 0;
	display: flex;
	align-items: center;
	justify-content: center;
	z-index: 2;
`
export const ContainerLogo = styled.div`
	display: flex;
	justify-content: center;
	align-content: center;
	flex-direction: column;
	width: 93px;
	height: 25px;
`

export const Redline = styled.div`
	width: 93px;
	border: 2px solid #f7522a;
`

export const LogoName = styled.div`
	font-size: 18px;
	color: #20212a;
`
export const Hamburger = styled.div`
	top: 14px;
	left: 24px;
	position: absolute;
	cursor: pointer;
`
