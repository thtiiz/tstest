import styled from 'styled-components'

export const MenuName = styled.span`
	font-size: 16px;
	color: #ffffff;
`

export const SectionMenu = styled.div`
	padding: 16px 0px 16px 14px;
	width: 203px;
	height: 64px;
	display: flex;
	align-items: center;
`
export const ImgLogo = styled.img`
	width: 20px;
	height: 20px;
	margin-right: 14px;
`
export const Select = styled.div`
	width: 10px;
	height: 100%;
	background-color: #f7522a;
	&:hover {
		background-color: #f7522a;
		opacity: 80%;
	}
`
