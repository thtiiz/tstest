# Software Engineer Wiki :tada: :tada:
Demo at [http://exersite.netlify.com/](http://exersite.netlify.com/)

## Project Architecture (Frontend)

- React

- Jest (Testing)

- Mobx (Store)

- styled-components (Instead of css)

- react-router-dom (Router)

- Axios (Fetch api)

- gitlab-ci (CI/CD)

- Netlify (Hosting)

## Project Structure

- [client](#client)
- [components](#components)
- [core](#core)
- [modules](#modules)
- [routes](#routes)

## client

> เก็บไฟล์ App ของโปรเจคเราโดยจะถูกรันที่ฝั่ง client

## components

> เก็บ component ที่แทบจะไม่มี logic ex. button, card, images

## core

> เป็นกระดูกสันหลังของ App ex. Fetch api, Routing

## modules

> component ที่พัฒนาโดยมี logic ที่เจาะจงโดยเฉาะ

## routes

> เก็บ route, path ต่างๆของเว็บไว้ render ด้วย Router
