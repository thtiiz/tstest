import Loadable from 'react-loadable'

const Filter = Loadable({
	loader: () => import(/* webpackChunkName: "module.Filter" */ '.'),
	loading: () => null,
})

export default Filter
