import styled, { css } from 'styled-components'
import Carousel from 'react-multi-carousel'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const CarouselSection = styled(Carousel)`
    padding:15px 12px 15px 12px;
    background-color: #ffffff;
`

const ArrowStyle = css`
    position:absolute;
   	display:flex;
	justify-content:center;
	align-items:center;
	outline:0;
	transition:all .5s;
	border-radius:35px;
	z-index:1;
	border:0;
	background:rgba(0,0,0,0.5);
	min-width:43px;
	min-height:43px;
	opacity:1;
    cursor:pointer;
	
`
export const Arrow = styled(FontAwesomeIcon)`
	color:white;
	font-size:20px;
`
export const RightArrowStyle = styled.div`
	${ArrowStyle};
	right:10px;
`

export const LeftArrowStyle = styled.div`
	${ArrowStyle};
	left:10px;
`
