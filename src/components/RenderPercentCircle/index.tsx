import React from 'react'
import PercentCircle from 'components/PercentCircle'
import { Detail } from 'components/Checkbox/styled'
import { MyProgramType } from 'modules/stores/types'

interface Props {
  isAddProgram: boolean
  myProgram: MyProgramType
}

const RenderPercentCircle = ({ isAddProgram, myProgram }: Props) => {
  const { percent, status } = myProgram
  return (!isAddProgram && status !== 1) ? (
    <Detail style={{ textAlign: 'center' }}>
      <div>You completed {percent}%</div>
      <div style={{ marginBottom: '20px' }}>of your program.</div>
      <PercentCircle percentage={percent} />
    </Detail>
  ) : null
}

export default RenderPercentCircle
