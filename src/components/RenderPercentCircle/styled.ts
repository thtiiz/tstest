import styled from 'styled-components'

export const Detail = styled.div`
	font-size: 14px;
	text-align: left;
	margin: 15px 0px 0px 20px;
	width: 89.33%;
	height: fit-content;
	overflow-wrap: break-word;
`
