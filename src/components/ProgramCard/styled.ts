import styled, { css } from 'styled-components'

interface Props {
	src: string;
}

const overflowText = css`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`

export const Box = styled.div`
	display: flex;
	width: 100%;
	height: 200px;
	color: #f7522a;
	background: #ffffff;
	border-radius: 10px;
	cursor: pointer;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`
export const LeftSection = styled.div`
	position: relative;
	width: 50%;
	height: 100%;
	display: flex;
	flex-direction: column;
	text-align: left;
`
export const ProgramSection = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 75%;
	padding: 10px 8px 8px 20px;
	position: relative;
`
export const RightSection = styled.div`
	width: 50%;
	height: 100%;
	border-radius: 0 10px 10px 0;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	background-image: url(${({ src }: Props) => src});
`

export const ProgramName = styled.div`
	font-size: 18px;
	${overflowText};
`
export const Detail = styled.div`
	font-size: 10px;
	color: #4c4c4c;
	word-wrap: break-word;
	overflow: hidden;
	color: black;
	width: 140px;
	height: 110px;
	padding-top: 5px;
`

export const RatingText = styled.div`
	bottom: 8px;
	font-size: 10px;
	color: black;
	position: absolute;
`

export const OwnerWrap = styled.div`
	width: 100%;
	height: 25%;
	display: flex;
	align-items: center;
	padding-left: 20px;
	padding-right: 8px;
	border-top: 1px solid #c4c4c4;
`
export const OwnerName = styled.span`
	${overflowText};
	font-size: 8px;
	margin-left: 5px;
`

export const ImgOwner = styled.img`
	width: 22px;
	height: 22px;
	border-radius: 50%;
`
