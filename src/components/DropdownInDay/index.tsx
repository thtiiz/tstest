import React from 'react'
import { Box, Leftbox, Rightbox, Righttopbox, Rightbotbox, ImgBox } from 'components/DropdownInDay/styled'
import PlusMinusInCard from 'components/PlusMinusInCard'

interface Props {
  img: string
  name: string
}

const DropdownInDay = ({ img, name }:Props) => {
	return (
		<Box>
			<Leftbox>
				<ImgBox style={{ backgroundImage: `url(${img})` }}></ImgBox>
			</Leftbox>
			<Rightbox>
				<Righttopbox>{name}</Righttopbox>
				<Rightbotbox>
					<PlusMinusInCard />
				</Rightbotbox>
			</Rightbox>
		</Box>
	)
}

export default DropdownInDay
