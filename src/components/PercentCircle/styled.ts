import styled from 'styled-components'

export const Container = styled.div`
	width: 150px;
	height: 150px;
	display: flex;
	margin: 0 auto;
`
