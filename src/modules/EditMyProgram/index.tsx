import React, { useState, useCallback } from 'react'
import MyProgramStore from 'modules/stores/MyProgramStore'
import {
  HeaderText, Edit, CancelButton, ApplyButton,
  EditProgramName, EditDescription, DescriptionTitle
} from './styled'
import { get } from 'lodash'
import CoverImg from 'components/CoverImg'

interface Props {
  myProgramStore: MyProgramStore
  toggleIsEdit: () => void
}

const EditMyProgram = ({ myProgramStore, toggleIsEdit }: Props) => {
  const { myProgram, initialEditDayState, editMyProgram } = myProgramStore
  const name = get(myProgram, ['program', 'name'])
  const description = get(myProgram, ['program', 'description'])
  const totalDay = get(myProgram, 'totalDays')
  const image = get(myProgram, ['program', 'image'])
  const [programName, setProgramName] = useState(name)
  const [coverImg, setCoverImg] = useState(image)
  const [newDescription, setNewDescription] = useState(description)

  const onNameChange = useCallback((e: any) => {
    setProgramName(e.target.value)
  }, [])

  const onDescriptionChange = useCallback((e: any) => {
    setNewDescription(e.target.value)
  }, [])

  const onCancel = () => {
    toggleIsEdit()
    initialEditDayState(totalDay)
  }
  const onApply = () => {
    toggleIsEdit()
    editMyProgram(programName, newDescription, coverImg)
    initialEditDayState(totalDay)
  }

  const handleUpload = (imageUrl: string) => {
    setCoverImg(imageUrl)
  }

  return (
    <div>
      <CoverImg image={coverImg} canEdit handleUpload={handleUpload} />
      <HeaderText>
        <EditProgramName required title="Program name"
	type="text" value={programName} onChange={onNameChange} />
        <DescriptionTitle htmlFor="description">Description </DescriptionTitle>
        <EditDescription id="description" value={newDescription} onChange={onDescriptionChange} />
      </HeaderText>
      <Edit>
        <CancelButton onClick={onCancel}>Cancel</CancelButton>
        <ApplyButton onClick={onApply}>Apply</ApplyButton>
      </Edit>
    </div>
  )
}

export default EditMyProgram
