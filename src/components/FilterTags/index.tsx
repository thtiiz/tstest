import React from 'react'
import FilterTag from 'components/FilterTag'
import { FilterTagStyled } from './styled'

interface Props {
  filterTagItems: string[]
  filterTagClose: (filter: string) => void
  className?: string
}

const FilterTags = ({ filterTagItems, filterTagClose, className }: Props) => {
  return (
    <FilterTagStyled className={className}>
      {filterTagItems.map(filterTagItem => (
        <FilterTag
	postName={filterTagItem}
	onClose={() => filterTagClose(filterTagItem)}
	key={`checkbox-${filterTagItem}`}
        />
      ))}
    </FilterTagStyled>
  )
}

export default FilterTags
