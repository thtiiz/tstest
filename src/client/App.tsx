import React from 'react'
import { Provider } from 'mobx-react'
import RootStore from 'modules/stores/RootStore'
import Routes from 'routes'
import 'client/App.css'

const App: React.FC = () => {
	return (
		<Provider {...RootStore}>
			<div className="App">
				<Routes />
			</div>
		</Provider>
	)
}

export default App
