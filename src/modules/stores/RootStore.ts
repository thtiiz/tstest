import SearchStore from 'modules/stores/SearchStore'
import FilterStore from './FilterStore'
import ExerciseStore from './ExerciseStore'
import UserStore from './UserStore'
import SuggestProgramStore from './SuggestProgramStore'

class RootStore {
	filterStore = new FilterStore()

	exerciseStore = new ExerciseStore()

	userStore = new UserStore()

	searchStore = new SearchStore()

	suggestProgramStore = new SuggestProgramStore()
}

export default new RootStore()
