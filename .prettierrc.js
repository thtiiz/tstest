module.exports = {
  "bracketSpacing": true,
  "jsxBracketSameLine": false,
  "parser": "babel",
  "printWidth": 110,
  "semi": false,
  "singleQuote": true,
  "tabWidth": 2,
  "trailingComma": "all",
  "useTabs": true
}
