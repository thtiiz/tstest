import React, { Component } from 'react'
import { Box, ClickBox } from './styled'
import add from 'components/images/addincard.svg'
import minus from 'components/images/minusincard.svg'

interface State {
	clicks: number;
}

class PlusMinusInCard extends Component<{}, State> {
	constructor(props: number) {
		super(props)
		this.state = {
			clicks: 0,
		}
	}

	IncrementItem = () => {
		this.setState({ clicks: this.state.clicks + 1 })
	}

	DecreaseItem = () => {
		if (this.state.clicks > 0) {
			this.setState({ clicks: this.state.clicks - 1 })
		}
	}

	render() {
		return (
			<Box>
				<ClickBox src={minus} alt="cls" onClick={this.DecreaseItem} />
				{this.state.clicks}
				<ClickBox src={add} alt="cls" onClick={this.IncrementItem} />
			</Box>
		)
	}
}

export default PlusMinusInCard
