import React from 'react'
import 'react-multi-carousel/lib/styles.css'
import { CarouselSection, RightArrowStyle, LeftArrowStyle, Arrow } from './styled'
import { responsive } from './config'
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

interface Props {
	children: any;
	autoPlay?: boolean;
}

const CustomRightArrow = ({ onClick }: any) => {
	return <RightArrowStyle onClick={() => onClick()}>
		<Arrow icon={faChevronRight} />
	</RightArrowStyle>
}
const CustomLeftArrow = ({ onClick }: any) => {
	return <LeftArrowStyle onClick={() => onClick()} >
		<Arrow icon={faChevronLeft} />
	</LeftArrowStyle >
}

export const CarouselCard = ({ autoPlay = false, children }: Props) => {
	return (
		<CarouselSection
			customRightArrow={<CustomRightArrow />}
			customLeftArrow={<CustomLeftArrow />}
			additionalTransfrom={0}
			centerMode={false}
			containerClass="container"
			dotListClass=""
			draggable
			focusOnSelect={false}
			infinite={false}
			itemClass="image-item"
			keyBoardControl
			minimumTouchDrag={80}
			renderDotsOutside={false}
			responsive={responsive}
			showDots={false}
			sliderClass=""
			slidesToSlide={1}
			swipeable
			partialVisbile={true}
			autoPlay={autoPlay}
		>
			{children}
		</CarouselSection>
	)
}
