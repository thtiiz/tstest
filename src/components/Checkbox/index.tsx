import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Detail, Box } from './styled'
import { faSquare } from '@fortawesome/free-regular-svg-icons'
import { faCheckSquare } from '@fortawesome/free-solid-svg-icons'

interface Props {
  title: string;
  onClick: any;
  isCheck: boolean;
}

const Checkbox = ({ title, onClick, isCheck }: Props) => {
  const checkIcon = isCheck ? faCheckSquare : faSquare
  const checkColor = isCheck ? '#f7522a' : '#BFBFBF'
  return (
    <Box onClick={onClick}>
      <div>
        <FontAwesomeIcon icon={checkIcon} color={checkColor} size={'2x'} />
      </div>
      <Detail>{title}</Detail>
    </Box>
  )
}
export default Checkbox
