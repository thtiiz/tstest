import styled from 'styled-components'

export const Container = styled.div`
	display: grid;
	grid-gap: 10px;
	grid-template-columns: repeat(auto-fill, minmax(303px, 1fr));
`
export const ContainerCard = styled.div`
	width: 303px;
	height: 225px;
	margin: auto;
`
