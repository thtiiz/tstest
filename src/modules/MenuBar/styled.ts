import styled, { css } from 'styled-components'

interface MenuBarTypes {
	isOpen: boolean
}

const moveIn = css`
	@keyframes moveIn{
	0%{left:-700px;}
	100%{left:0px;}
}
`
const moveOut = css`
	@keyframes moveOut{
	0%{left:0px;}
	100%{left:-700px;}
}
`
const MoveIn = css`
${moveIn}
animation: moveIn 0.2s ease-in-out;
`
const MoveOut = css`
${moveOut}
animation: moveOut 0.2s ease-in-out;
`

const fadeIn = css`
@keyframes fadeIn {
	0% { opacity: 0; }
	100% { opacity: 0.4; }
}
`
const FadeIn = css`
${fadeIn}
animation: fadeIn 0.1s ease-in-out;
`

const fadeOut = css`
@keyframes fadeOut {
	0% { opacity: 0.4; }
	100% { opacity: 0; }
}
`
const FadeOut = styled.div`
${fadeOut}
animation: fadeOut 0.1 ease-in-out;
`

export const Container = styled.div`
width: 203px;
height: 100vh;
background-color: black;
position: fixed;
display: flex;
flex-direction: column;
background-color: #4D4D4D;
top: 0px;
left: 0px;
z-index: 2;
${({ isOpen }: MenuBarTypes) => (isOpen ? MoveIn : MoveOut)};

`

export const Backdrop = styled.div`
height: 100vh;
width: 100%;
position: fixed;
top: 0px;
left: 0px;
z-index: 1;
opacity: 0.4;
${({ isOpen }: MenuBarTypes) => (isOpen ? FadeIn : FadeOut)};
pointer-events: ${({ isOpen }: MenuBarTypes) => (isOpen ? 'auto' : 'none')};
background-color: black;
`

export const SignInText = styled.span`
margin-top: 5px;
font-style: italic;
font-size: 14px;
color: #fff;
`
export const AvatarSection = styled.div`
width: 203px;
height: 160px;
display: flex;
flex-direction: column;
align-content: center;
justify-content:center;
margin-bottom: 30px;
`
export const ImageProfile = styled.img`
width: 80px;
height: 80px;
border-radius: 50%;
`

export const MenuName = styled.span`
font-size: 16px;
color: #ffffff;
`

export const SectionMenu = styled.div`
padding: 16px 0px 16px 14px;
width: 203px;
height: 64px;
display: flex;
align-items: center;
	&:hover{
	border-width: 0px 0px 0px 8px;
	border-style: solid;
	border-left-color: #f7522a;
	background-color: #333333;
}
`

export const ImgLogo = styled.img`
width: 20px;
height: 20px;
margin-right: 14px;
`
export const Select = styled.div`
width: 5px;
height: 100%;

`
