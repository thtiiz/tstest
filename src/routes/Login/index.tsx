import React, { useState, useEffect } from 'react'
import {
  FormContainer, Container, LoginButton,
  Line, ORSection, ORText, SocialSection, Img,
  SignUpText, ErrorText, ErrorSection, Signup
}
  from './styled'
import gmail from 'components/images/login-gmail.svg'

import { GoogleLogin } from 'react-google-login'
import facebook from 'components/images/login-facebook.svg'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import UserStore from 'modules/stores/UserStore'
import { inject, observer } from 'mobx-react'
import { compose } from 'recompose'
import { LoginForm } from 'modules/stores/types'
import history from 'core/history'
import { emailValidation, passwordValidation } from 'utils/validator'
import { get } from 'lodash'
import ErrorMessage from 'components/ErrorMessage'
import InputText from 'components/InputText'
import { googleClientId, fbAppId } from 'core/config'

interface StoreProps {
  loginNative: (formValues: LoginForm) => void
  loginFacebook: (data: any) => void
  loginGoogle: (data: any) => void
  verify: () => void
  isLogin: boolean
  state: string
  isInitial: boolean
  errorTxt: string
}

const onClickSignup = () => {
  const state = get(history, ['location', 'state'])
  history.replace({ pathname: '/signup', state })
}

const LoginPage = ({ errorTxt, loginNative, loginFacebook,
  loginGoogle, isLogin, state, isInitial }: StoreProps) => {
  const [formValues, setFormValues] = useState({ email: '', password: '' })
  const [error, setError] = useState({ email: '', password: '' })
  const [disabled, setDisabled] = useState(false)
  const handleChange = (e: any) => {
    const { name, value } = e.target
    const newFormValues = { ...formValues, [name]: value }
    setFormValues(newFormValues)
    validator(name, newFormValues)
  }

  const validator = (name: string, newFormValues: LoginForm) => {
    let validate
    if (name === 'email') validate = emailValidation({ email: newFormValues.email })
    else if (name === 'password') validate = passwordValidation({ password: newFormValues.password })
    const message = get(validate, 'error.details[0].message', [])
    setError({ ...error, [name]: message })
  }

  const onSubmit = (e: any) => {
    e.preventDefault()
    loginNative(formValues)
    setDisabled(true)
  }

  useEffect(() => {
    if (!isInitial) setDisabled(false)
    else if (state === 'loading') setDisabled(true)
    else setDisabled(false)
    const historyState = get(history, ['location', 'state'], {})
    const { redirect, ...safeState } = historyState
    if (isLogin) history.push({ pathname: redirect || '/home', state: safeState })
  }, [disabled, isInitial, isLogin, state])

  const responseFacebook = (response: any) => {
    loginFacebook(response)
  }

  const responseGoogle = (response: any) => {
    loginGoogle(response)
  }

  return (

    < Container >
      {errorTxt ? <ErrorMessage message={errorTxt} /> : <ErrorSection />}
      <FormContainer onSubmit={onSubmit} id="form" >
        <InputText required title="Email" type=" text" name="email"
	onChange={handleChange} value={formValues.email} />
        {error ? <ErrorText>{error.email}</ErrorText> : null}
        <InputText required type="password" name="password"
	onChange={handleChange} title="Password" value={formValues.password} />
        {error ? <ErrorText>{error.password}</ErrorText> : null}

        <LoginButton disabled={disabled} type="submit" form="form" >LOGIN</LoginButton>
        <ORSection>
          <Line /><ORText>OR</ORText><Line />
        </ORSection>
        <SocialSection>
          <GoogleLogin
	autoLoad={false}
	clientId={googleClientId}
	render={renderProps => (
              <Img onClick={renderProps.onClick} src={gmail} />
            )}
	onSuccess={responseGoogle}
	onFailure={responseGoogle}
	cookiePolicy={'single_host_origin'}
          />
          <FacebookLogin
	appId={fbAppId}
	render={(renderProps: any) => (
              <Img onClick={renderProps.onClick} src={facebook} />
            )}
	buttonStyle={{}}
	fields="id,first_name,last_name,email,picture,gender"
	cssClass="facebookLogin"
	callback={responseFacebook}
          />
        </SocialSection>
        <SignUpText>{'Haven\'t any account? '}  <Signup onClick={onClickSignup}>Signup</Signup></SignUpText>
      </FormContainer>
    </Container >

  )
}

interface Stores {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  inject(({ userStore }: Stores) => ({
    loginNative: userStore.loginNative,
    loginFacebook: userStore.loginFacebook,
    loginGoogle: userStore.loginGoogle,
    isLogin: userStore.isLogin,
    state: userStore.state,
    isInitial: userStore.isInitial,
    errorTxt: userStore.error
  })),
  observer
)(LoginPage)
