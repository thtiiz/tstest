import styled from 'styled-components'
import { ResponsiveCard } from 'utils'

export const TitleSection = styled.div`
	width: 100%;
	height: 55px;
	background: #ffffff;
	display: flex;
	justify-content: space-between;
	padding-right: 40px;
	align-items: center;

	border-bottom: 5px solid #f7522a;
`
export const ViewMore = styled.span`
	padding: 0px 6px 3px;
	border: 1px solid #fa246c;
	border-radius: 4px;
	font-size: 0.85rem;
	vertical-align: middle;
	color: #fa246c;
	cursor: pointer;
`

export const Title = styled.h1`
	font-size: 20px;
	color: #20212a;
	margin-left: 18px;
`

export const Container = styled.div`
	background: white;
`
export const CarouselSection = styled.div`
	padding: 15px 12px 15px 12px;
	background-color: #ffffff;
	display: flex;
`

export const Card = styled.div`
	${ResponsiveCard};
`
