import styled from 'styled-components'

interface Props {
	isShowMenu: boolean;
}

export const Daybox = styled.div`
	width: 335px;
	height: 43px;
	color: #ffffff;
	background-color: #f7522a;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	font-size: 18px;
	display: flex;
	justify-content: space-between;
	padding-left: 20px;
	line-height: 43px;
	border-radius: ${({ isShowMenu }: Props) => (isShowMenu ? '10px 10px 0 0' : '10px')};
`

export const Dropdown = styled.div`
	cursor: pointer;
	padding: 0 20px 0 20px;
`

export const Box = styled.div`
	height: 100px;
	width: 335px;
	background-color: #f7522a;
	color: #ffffff;
	display: flex;
	padding: 10px 0 10px 0;
	border-bottom-left-radius: 10px;
	border-bottom-right-radius: 10px;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`
export const Rightbox = styled.div`
	width: 50%;
	height: 80px;
	font-size: 14px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
`
export const Leftbox = styled(Rightbox)`
	width: 50%;
	height: 80px;
	display: flex;
	align-items: center;
	justify-content: center;
`
export const Righttopbox = styled.div`
	word-break: break-word;
`

export const Img = styled.div`
	height: 100%;
	width: 100%;
	background-repeat: no-repeat;
	background-position: center center;
	background-size: auto 90%;
`
export const ImgBox = styled.div`
	width: 150px;
	height: 100%;
	background-color: #ffffff;
	border-radius: 4px;
`
export const PlusMinusInCard = styled.div`
	display: flex;
	width: 100px;
	height: 30px;
	justify-content: space-between;
	background-color: #4d4d4d;
	color: #ffffff;
	border-radius: 4px;
	align-items: center;
`
export const ClickBox = styled.img`
	cursor: pointer;
	padding: 10px;
`
