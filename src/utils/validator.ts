import Joi from 'joi'

export const emailValidation = (data: any) => {
	const schema = {
		email: Joi.string()
			.min(6)
			.max(64)
			.required()
			.email(),
	}
	return Joi.validate(data, schema)
}

export const passwordValidation = (data: any) => {
	const schema = {
		password: Joi.string()
			.min(8)
			.max(24)
			.required(),
	}
	return Joi.validate(data, schema)
}
