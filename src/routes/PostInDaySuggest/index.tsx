import React from 'react'
import RenderExercises from 'components/RenderExercises'
import MyProgramsHeader, { HEADER_STATES } from 'components/ProgramsHeader'
import { Container, Line } from './styled'
import { observer, inject } from 'mobx-react'
import { compose } from 'recompose'
import { DayType } from 'modules/stores/types'
import { get } from 'lodash'
import { useParams } from 'react-router'
import SuggestProgramStore from 'modules/stores/SuggestProgramStore'
import history from 'core/history'

interface StoreProps {
  dayIndex: (programId: string, index: number) => DayType
}

const PostInDaySuggest = ({ dayIndex }: StoreProps) => {
  const numDay = get(useParams(), 'numDay', '')
  const id = get(history, ['location', 'state', 'programId'], '')
  const day: DayType = dayIndex(id, numDay - 1)
  return (
    <Container>
      <MyProgramsHeader isEdit title={numDay} numHeader={HEADER_STATES.POSTINDAY} />
      <Line />
      <RenderExercises dayExercises={day.exercises} />
    </Container>
  )
}

interface Store {
  suggestProgramStore: SuggestProgramStore
}

export default compose<StoreProps, {}>(
  inject(({ suggestProgramStore }: Store) => ({
    dayIndex: suggestProgramStore.dayIndex
  })),
  observer,
)(PostInDaySuggest)
