import React from 'react'
import { Background, Appname, Social, Copyright, Map, Apptext } from './styled'
import Mapmarker from 'components/images/Mapmarker.svg'
import Phonecall from 'components/images/Phonecall.svg'
import Instagram from 'components/images/Instagram.svg'
import Facebook from 'components/images/Facebook.svg'
import Twitter from 'components/images/Twitter.svg'
import 'react-circular-progressbar/dist/styles.css'
const Footer = () => {
	return (
		<Background>
			<Appname>
				Exersite<Social>Social</Social>
			</Appname>
			<Map style={{ marginTop: '40px' }}>
				<img src={Mapmarker} alt={`Mapmaker-${Map}`} />
				<div style={{ marginLeft: '10px' }}>50 Thanon Ngam Wong Wan, Lat Yao,</div>
				<img src={Instagram} alt={`Instagram-${Map}`} style={{ marginLeft: '45px' }} />
				<Apptext>instagram</Apptext>
			</Map>
			<Map style={{ marginRight: '155px' }}> Chatuchak, Bangkok 10900</Map>
			<Map style={{ marginTop: '33px', marginRight: '6px' }}>
				<img src={Phonecall} alt={`Phonecall-${Map}`} />
				<div style={{ marginLeft: '8px' }}>097-252-0454</div>
				<img src={Facebook} alt={`Facebook-${Map}`} style={{ marginLeft: '134px' }} />
				<Apptext>facebook</Apptext>
			</Map>
			<Map style={{ marginTop: '46px' }}>
				<img src={Twitter} alt={`Twitter-${Map}`} style={{ marginLeft: '190px' }} />
				<Apptext>twitter</Apptext>
			</Map>
			<Copyright>Copyright © 2019 All Rights Reserved | This template is made by</Copyright>
			<div>PPAFF</div>
		</Background>
	)
}

export default Footer
