import { createBrowserHistory } from 'history'
// import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'

const browserHistory = createBrowserHistory()
// const routingStore = new RouterStore()

// export default syncHistoryWithStore(browserHistory, routingStore)
export default browserHistory
