import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export const Box = styled.div`
	height: 109.067vw;
	width: 100%;
	align-items: center;
	justify-content: center;
	background-color: #20212a;
	display: flex;
	@media (min-width: 768px) {
		height: 837.63px;
	}
	@media (min-width: 1367px) {
		height: 708.92px;
		width: fit-content;
		padding: 10px 0.732vw 30px 0.732vw;
	}
`
export const ContentBox = styled.div`
	height: fit-content;
	@media (min-width: 768px) {
		height: 759.66px;
	}
	@media (min-width: 1367px) {
		height: 653.58px;
	}
`
export const WhiteBox = styled.div`
	max-width: 405.5px;
	max-height: 131.06px;
	height: 17.067vw;
	width: 52.8vw;
	border-radius: 1.067vw;
	display: flex;
	justify-content: space-between;
	padding: 3.467vw 2.667vw 3.467vw 2.667vw;
	background-color: #ffffff;
	color: #000000;
	text-align: left;
	@media (min-width: 768px) {
		padding: 26.6662px 20.4826px 26.6662px 20.4826px;
		border-radius: 8px;
	}
	@media (min-width: 1367px) {
		max-width: 316.8px;
		max-height: 102.39px;
		padding: 20.802px 16.002px 20.802px 16.002px;
		font-size: 12px;
	}
`
export const Pic = styled.img`
	max-width: 222px;
	max-height: 503px;
	width: 28.8vw;
	height: 65.333vw;
	background-size: 100% 100%;
	margin: 8vw 0 8vw 0;
	border-radius: 1.3vw;
	@media (min-width: 768px) {
		margin: 61.44px 0 61.44px 0;
		border-radius: 10px;
	}
	@media (min-width: 1367px) {
		margin: 52px 0 52px 0;
		max-width: 187.19px;
		max-height: 424.66px;
	}
`
export const OrangeBox = styled.img`
	width: 10.133vw;
	height: 10.133vw;
	background-color: #f7522a;
	border-radius: 1.067vw;
	padding: 1.333vw;
	@media (min-width: 768px) {
		max-height: 77.81px;
		max-width: 77.81px;
		padding: 10.2374px;
		border-radius: 8px;
	}
	@media (min-width: 1367px) {
		max-height: 60.8px;
		max-width: 60.8px;
		padding: 7.998px;
		margin-right: 16.002px;
	}
`

export const LearnBox = styled.div`
	max-width: 222px;
	max-height: 503px;
	width: 28.8vw;
	height: 65.333vw;
	color: #ffffff;
	font-size: 6.4vw;
	border-radius: 1.067vw;
	border: 2px solid #ffffff;
	word-wrap: break-word;
	align-items: center;
	justify-content: center;
	display: flex;
	padding: 4vw;
	@media (min-width: 768px) {
		padding: 30.72px;
		font-size: 49.152px;
		border-radius: 8px;
	}
`
export const ArrowBox = styled(FontAwesomeIcon)`
	height: 4.267vw;
	width: 4.267vw;
`
export const DesktopBox = styled.div`
	display: flex;
	width: 100%;
	align-items: center;
	justify-content: center;
	background-color: #20212a;
`
export const Button = styled.div`
	height: 36px;
	width: 200px;
	background-color: #f7522a;
	color: #ffffff;
	text-align: center;
	font-size: 18px;
	border-radius: 4px;
	margin: 0 auto;
	display: block;
	line-height: 36px;
	cursor: pointer;
	&:hover{
		height: 48px;
		width: 266.67px;
		line-height: 48px;
		font-size: 24px;
		background-color: #e0261b;
	}
	@media (min-width: 1851px) {
		margin-top: 1vw;
	}
`
export const Header = styled.div`
	color: #f7522a;
	font-size: 14px;
`
export const ContextMobileBox = styled.div`
	height: 10.133vw;
	width: 34.667vw;
	max-height: 77.81px;
	max-width: 266.23px;
	font-size: 12px;
	align-items: center;
	display: flex;
`
export const Detail = styled.div`
	font-size: 12px;
	visibility: hidden;
	${WhiteBox}:active {
		background-color: #ffffff;
		height: 131.06px;
		width: 405.5px;
		visibility: visible;
	}
`
