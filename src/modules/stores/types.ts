import { MyProgramType } from './types'

interface Response {
  page: number
  nextPage: number
  length: number
}

export interface FetchExercises extends Response {
  data: ExerciseType[]
}

export interface FetchPrograms extends Response {
  data: ProgramType[]
}

export interface FetchExercise extends Response {
  data: ExerciseType
}

export interface FetchUser extends Response {
  data: UserType
}

export interface LoginForm {
  email: string
  password: string
}
export interface RegisterForm {
  firstName: string
  lastName: string
  email: string
  password: string
  gender: string
}

export interface EditProfileForm {
  firstName?: string,
  lastName?: string,
  oldPassword?: string,
  newPassword?: string,
  image?: string
}
export interface UserType {
  firstName: string
  lastName: string
  email: string
  gender: string
  image: string
  myPrograms: MyProgramType[]
}
export interface MyProgramType {
  id: string
  program: ProgramType
  completedExercises: number
  currentDay?: number
  totalDays: number
  totalDayComplete: number
  percent: number
  status: number
}

export interface ProgramType {
  id: string
  name: string
  ownerName: string
  ownerPic: string
  description: string
  difficulty: number
  days: DayType[]
  image: string
  rating: number
}

export interface DayExerciseType {
  id: string
  exercise: string
  setsPerExercise: number
}

export interface DayType {
  id: string
  exercises: DayExerciseType[]
  isComplete: boolean
}

export interface ExerciseType {
  id: string
  description: string
  difficulty: number
  comments: string[] | string
  name: string
  category: number
  muscles: number[]
  musclesSecondary: number[]
  equipment: number[]
  images: string[]
  imageGif: string
  set: number
}

export enum CATEGORY {
  Arms = 8,
  Legs,
  Abs,
  Chest,
  Back,
  Shoulders,
  Calves,
}

export enum EQUIPMENT {
  'Barbell' = 1,
  'SZ-Bar',
  'Dumbbell',
  'Gym mat',
  'Swiss Ball',
  'Pull-up bar',
  'bodyweight',
  'Bench',
  'Incline bench',
  'Kettlebell'
}
