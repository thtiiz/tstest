import React, { useState, useEffect, useCallback } from 'react'
import {
  Detail, Line, DoneButton,
  StartButton, ProgramAction, Container, ActionSection
} from './styled'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import { useParams } from 'react-router'
import { Linked } from 'components/Linked'
import { faPlay, faRedo } from '@fortawesome/free-solid-svg-icons'
import add from 'components/images/add.svg'
import withAuth from 'utils/withAuth'
import { get, pick } from 'lodash'
import { STATES } from 'core/api/FetchData'
import { Spinner } from 'components/Spinner'
import Daylist from 'modules/Daylist'
import history from 'core/history'
import { AddProgramContext } from 'utils'
import UserStore from 'modules/stores/UserStore'
import MyProgramStore from 'modules/stores/MyProgramStore'
import { MyProgramType } from 'modules/stores/types'
import RenderPercentCircle from 'components/RenderPercentCircle'
import MyProgramsHeader, { HEADER_STATES } from 'components/ProgramsHeader'
import DayText from 'components/DayText'
import EditMyProgram from 'modules/EditMyProgram'
import NewButton from 'components/NewButton'

interface StoreProps {
  myProgramId: (id: string) => MyProgramStore
  addMyProgram: (myProgram: MyProgramType) => void
}

const MyProgram = ({ myProgramId, addMyProgram }: StoreProps) => {
  const id = get(useParams(), 'id')
  const myProgramStore = myProgramId(id)
  const { startProgram, restartProgram, newDayToProgram,
    addExerciseToDays, state, initialEditDayState, days } = myProgramStore
  const myProgram = get(myProgramStore, 'myProgram')
  const { program, status } = pick(myProgram, ['program', 'percent', 'status'])
  const { name, description, image } = pick(program, ['name', 'description', 'image'])
  const [isEdit, setIsEdit] = useState(false)
  const toggleIsEdit = () => { setIsEdit(!isEdit) }

  const onStartProgram = async () => {
    const myProgramStarted = await startProgram()
    if (myProgramStarted) {
      addMyProgram(myProgramStarted)
      history.replace({
        pathname: `/myprogram/${myProgramStarted.id}`,
      })
    }
  }

  const onNewDay = () => { newDayToProgram(id) }
  const { isAddProgram, exerciseId } = get(history, ['location', 'state'], {})
  const onAddProgramToDay = useCallback(async () => {
    await addExerciseToDays(exerciseId)
  }, [addExerciseToDays, exerciseId])
  const RenderDone = () => {
    return isAddProgram ? (
      <Linked onClick={onAddProgramToDay} to={`/exercise/${exerciseId}`}>
        <DoneButton >DONE</DoneButton>
      </Linked>
    ) : null
  }

  const RenderActionSection = () => {
    let programActionIcon
    let onAction
    if (status === 1) {
      onAction = onStartProgram
      programActionIcon = faPlay
    } else {
      onAction = restartProgram
      programActionIcon = faRedo
    }
    const button = isAddProgram ? <NewButton onClick={onNewDay} src={add} alt="add" />
      : <StartButton onClick={onAction} >
        <ProgramAction size="lg" icon={programActionIcon} color="#ffffff" />
      </StartButton>
    const isLoading = state === STATES.LOADING
    return <ActionSection>
      <Line style={{ width: '81.33%' }} />
      {isLoading ? <Spinner />
        : button}
      <Line style={{ width: 'calc(18.67% - 50px)' }} />
    </ActionSection>
  }

  useEffect(() => {
    const { totalDays } = myProgram
    initialEditDayState(totalDays)
  }, [initialEditDayState, myProgram])

  return (
    <AddProgramContext.Provider value={isAddProgram}>
      <Container>
        {isEdit ? <EditMyProgram myProgramStore={myProgramStore} toggleIsEdit={toggleIsEdit} />
          : <MyProgramsHeader image={image} numHeader={HEADER_STATES.MYPROGRAM}
	title={name} fetchState={state}
	isAddProgram={isAddProgram} isEdit={isEdit}
	toggleIsEdit={toggleIsEdit} />}
        {!isEdit && <Detail>{description}</Detail>}
        {RenderActionSection()}
        <RenderPercentCircle isAddProgram={isAddProgram} myProgram={myProgram} />
        <DayText isEdit={isEdit} />
        <Daylist isEdit={isEdit} myProgramStore={myProgramStore} days={days} />
        {RenderDone()}
      </Container>
    </AddProgramContext.Provider>
  )
}

interface Store {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  inject(({ userStore }: Store) => ({
    myProgramId: userStore.myProgramId,
    addMyProgram: userStore.addMyProgram
  })),
  observer,
)(MyProgram)
