import qs from 'querystring'
import React, { createContext } from 'react'
import { isArray } from 'lodash'
import { css } from 'styled-components'

export const ResponsiveCard = css`
	@media only screen and (max-width: 500px) {
		width: 80.8vw;
	}
	@media screen and (min-width: 501px) {
		width: 60.6vw;
	}
	@media screen and (min-width: 701px) {
		width: 40.4vw;
	}
	@media screen and (min-width: 1024px) {
		width: 26.933vw;
	}
	@media screen and (min-width: 1440px) {
		width: 24vw;
	}
	@media screen and (min-width: 1920px) {
		width: 21vw;
	}
	@media screen and (min-width: 2560px) {
		width: 20vw;
	}
`

export const serializeParams = (params: any) => {
  return qs.stringify(params)
}

export const fullHeight = css`
	height: calc(100vh - 56px);
`

export const paramToString = (
  params: string[] | string,
  syncFilterTagItems: string[],
  mapper: Map<number, string>,
) => {
  if (isArray(params)) {
    params.forEach(param => {
      const mappedParam = mapper.get(Number(param))
      if (mappedParam) syncFilterTagItems.push(mappedParam)
    })
  } else {
    const mappedParam = mapper.get(Number(params))
    if (mappedParam) syncFilterTagItems.push(mappedParam)
  }
}

export const joinArray = (arr: any) => {
  return arr.length ? <span>{arr.join(', ')}</span> : 'none'
}

export const ENTER_CODE = 13

export const AddProgramContext = createContext(false)
