import React from 'react'
import Skeleton from 'react-loading-skeleton'
import { ImageWrapper, TextWrapper, PicBox } from './styled'

const SkeletonCard = () => {
	return (
		<PicBox>
			<Skeleton height={135} wrapper={ImageWrapper} />
			<Skeleton width={160} height={10} count={4} wrapper={TextWrapper} />
		</PicBox>
	)
}

export default SkeletonCard
