import React, { useState } from 'react'
import {
  OverallContainer,
  BGHeader,
  ProfileContainer,
  ProfilePicture,
  StopLine,
  ApplyBtn,
  HeaderContent,
  EditBtn,
  PasswordContainer,
  ErrorMessage,
} from './styled'
import HeaderPIC from 'components/images/BG-HEADER.jpg'
import EditContent from 'modules/EditContent'
import UserStore from 'modules/stores/UserStore'
import { compose } from 'recompose'
import withAuth from 'utils/withAuth'
import { inject, observer } from 'mobx-react'
import { UserType, EditProfileForm } from 'modules/stores/types'
import { omit, omitBy } from 'lodash'
import Upload from 'modules/Upload'

interface StoreProps {
  user: UserType
  error: any
  editProfile: (formValues: EditProfileForm) => void
}

export const EditProfile = (prop: StoreProps) => {
  const { user, error, editProfile } = prop
  const [formValue, setformValue] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    oldPassword: '',
    newPassword: '',
    repeatPassword: '',
    image: user.image,
  })

  const handleChange = (e: any) => {
    const { name, value } = e.target
    const newFormValues = { ...formValue, [name]: value }
    setformValue(newFormValues)
  }

  const handleUpload = (image: string) => {
    const newFormValues = { ...formValue, image }
    setformValue(newFormValues)
  }

  const onSubmit = () => {
    editProfile(omitBy(omit(formValue, 'repeatPassword'), profile => profile === ''))
  }

  const [isEditPassword, setisEditPassword] = useState(false)
  const handleToggle = () => setisEditPassword(!isEditPassword)

  return (
    <OverallContainer>
      <BGHeader src={HeaderPIC} />
      <ProfileContainer>
        <ProfilePicture src={formValue.image} onChange={handleChange} />
        <Upload isSquare handleUpload={handleUpload} />
        <EditContent
	title="FirstName"
	value={formValue.firstName}
	onChange={handleChange}
	isPassword={false}
	name="firstName"
        />
        <EditContent
	title="LastName"
	value={formValue.lastName}
	onChange={handleChange}
	isPassword={false}
	name="lastName"
        />

        <StopLine />

        <PasswordContainer>
          {<EditBtn onClick={handleToggle}>Edit</EditBtn>}

          {!isEditPassword ? <HeaderContent>Password</HeaderContent> : null}

          {isEditPassword ? (
            <div>
              <EditContent
	title="Old Password"
	value={formValue.oldPassword}
	onChange={handleChange}
	isPassword={true}
	name="oldPassword"
              />
              <EditContent
	title="New Password"
	value={formValue.newPassword}
	onChange={handleChange}
	isPassword={true}
	name="newPassword"
              />
              <EditContent
	title="Repeat Password"
	value={formValue.repeatPassword}
	onChange={handleChange}
	isPassword={true}
	name="repeatPassword"
              />
            </div>
          ) : null}
          {formValue.newPassword === formValue.repeatPassword
            ? null : <ErrorMessage>Wrong Repeat Password</ErrorMessage>}
          <ErrorMessage>{error}</ErrorMessage>
          <ApplyBtn onClick={onSubmit}>Update Profile</ApplyBtn>
        </PasswordContainer>
      </ProfileContainer>
    </OverallContainer>
  )
}

interface Stores {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  inject(({ userStore }: Stores) => ({
    user: userStore.user,
    error: userStore.error,
    editProfile: userStore.editProfile,
  })),
  observer
)(EditProfile)
