import { computedFn } from 'mobx-utils'
import history from 'core/history'
import { MyProgramType, DayType } from 'modules/stores/types'
import { observable, action, computed } from 'mobx'
import FetchData from 'core/api/FetchData'
import { get } from 'lodash'

class MyProgramStore extends FetchData {
	@observable.deep
	myProgram: MyProgramType

	@observable
	deletedDays: string[] = []

	@observable
	isCheckedDays: boolean[] = []

	@observable
	setsPerDayArr: number[] = []

	@observable
	deletedElements: string[] = []

	@observable
	isCheckedElements: boolean[] = []

	constructor(myProgram: MyProgramType) {
		super()
		this.myProgram = observable(myProgram)
	}

	dayId = computedFn((id: string) => {
		const index = this.myProgram.program.days.findIndex(day => day.id === id)
		return this.myProgram.program.days[index]
	})

	@computed
	get days() {
		return this.myProgram.program.days
	}

	@computed
	get status() {
		return this.myProgram.status
	}

	@computed
	get program() {
		return this.myProgram.program
	}

	@action
	editMyProgram = async (newName: string, newDescription: string, newImage: string) => {
		const fetch = await this.fetchData({
			path: '/programs/edit',
			method: 'post',
			body: {
				myProgramId: this.id,
				newName,
				newDescription,
				newImage,
				daysId: this.deletedElements,
			},
		})
		const data: MyProgramType = get(fetch, 'data')
		if (data) {
			this.myProgram = data
		}
	}

	@action
	initialEditState = (length: number) => {
		this.isCheckedElements = new Array(length).fill(false)
		this.deletedElements = []
	}

	@action
	initialEditDayState = (length: number) => {
		this.setsPerDayArr = new Array(length).fill(0)
		this.initialEditState(length)
	}

	@action
	clickElementOnDelete = (index: number, id: string) => {
		this.isCheckedElements[index] = !this.isCheckedElements[index]
		if (this.deletedElements.indexOf(id) !== -1) {
			this.deletedElements = this.deletedElements.filter((deletedElement: string) => deletedElement !== id)
		} else {
			this.deletedElements = this.deletedElements.concat(id)
		}
	}

	@action
	deleteExercises = async (dayId: string) => {
		const fetch = await this.fetchData({
			path: '/programs/deleteExercises',
			method: 'post',
			body: {
				myProgramId: this.id,
				dayId,
				dayExercisesId: this.deletedElements,
			},
		})
		const data: MyProgramType = get(fetch, 'data')
		if (data) {
			this.myProgram = data
		}
	}

	@action
	completeDay = async (dayId: string) => {
		const fetch = await this.fetchData({
			path: '/programs/completeDay',
			method: 'post',
			body: {
				myProgramId: this.id,
				dayId,
			},
		})
		const data: MyProgramType = get(fetch, 'data', {})
		if (data) {
			this.myProgram = data
		}
	}

	@computed
	get id() {
		const id = get(this.myProgram, 'id', '')
		return id
	}

	@action
	addExerciseToDays = async (exerciseId: string) => {
		const myProgramDays = this.days
		const days = this.setsPerDayArr.reduce((prev: any, setsPerDay, i: number) => {
			return prev.concat({ setsPerExercise: setsPerDay, id: myProgramDays[i].id })
		}, [])
		history.go(-2)
		const res = await this.fetchData({
			path: '/programs/addExerciseToDays',
			method: 'post',
			body: {
				myProgramId: this.id,
				exerciseId,
				days,
			},
		})
		const data: MyProgramType = get(res, 'data')
		if (data) {
			this.myProgram = data
		}
	}

	@action
	startProgram = async () => {
		const res = await this.fetchData({
			path: '/programs/startProgram',
			method: 'post',
			body: { myProgramId: this.id },
		})
		const data: MyProgramType = get(res, 'data')
		return data
	}

	@action
	restartProgram = async () => {
		const res = await this.fetchData({
			path: '/programs/restartProgram',
			method: 'post',
			body: { myProgramId: this.id },
		})
		const data: MyProgramType = get(res, 'data')
		if (data) {
			this.myProgram = data
		}
	}

	@action
	newDayToProgram = async (id: string) => {
		const fetch = await this.fetchData({
			path: '/programs/newDayToProgram',
			method: 'post',
			body: { myProgramId: id },
		})
		const data: DayType = get(fetch, 'data')
		if (data) {
			this.myProgram.totalDays++
			this.myProgram.program.days = this.myProgram.program.days.concat(data)
			this.setsPerDayArr = this.setsPerDayArr.concat(0)
		}
	}

	@action
	increaseSetPerExercise = (index: number) => {
		this.setsPerDayArr[index] += 1
	}

	@action
	decreaseSetPerExercise = (index: number) => {
		if (this.setsPerDayArr[index] > 0) this.setsPerDayArr[index] -= 1
	}
}

export default MyProgramStore
