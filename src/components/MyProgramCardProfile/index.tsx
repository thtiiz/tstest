import React, { useState } from 'react'
import {
  ProgramName,
  TitleName,
  ProgramCardContainer,
  PercentCoverContainer,
  Cover,
  Percent,
  ArrowIcon,
  ProgramDetailBox,
  ProgramCardOverallContainer,
} from './styled'
import Arrow from 'components/images/chevrons-down.svg'
import history from 'core/history'
import { mapDifficulty } from 'utils/mapper'
import { MyProgramType } from 'modules/stores/types'
import { pick, get } from 'lodash'

interface Props {
  myProgram: MyProgramType;
}

const MyProgramCard = (props: Props) => {
  const { myProgram } = props
  const { id, status, program, totalDays } = pick(myProgram, ['id', 'status', 'program', 'totalDays'])
  const percent = get(myProgram, 'percent', 0)
  const { difficulty, description, name } = pick(program, ['difficulty', 'description', 'name'])
  const [isShowDetail, setIsShowDetail] = useState(false)
  const toggleShowDetail = () => setIsShowDetail(!isShowDetail)
  const onClickProgram = () => {
    const state = history.location.state
    history.push({
      pathname: `/myprogram/${id}`,
      state
    })
  }

  return (
    <ProgramCardOverallContainer>
      <ArrowIcon onClick={toggleShowDetail} src={Arrow} isShowDetail={isShowDetail} />
      <ProgramCardContainer onClick={onClickProgram} >
        <TitleName>
          <ProgramName>{name}</ProgramName>
        </TitleName>
        <PercentCoverContainer >
          <Cover PercentCover={percent} status={status} />
          <Percent>{percent} %</Percent>
        </PercentCoverContainer>
      </ProgramCardContainer>
      {isShowDetail ? <ProgramDetailBox>
        <div>
          Day : {totalDays}
        </div>
        <div>
          Difficulty : {mapDifficulty.get(difficulty)}
        </div>
        <div>
          Description : {description}
        </div>
      </ProgramDetailBox> : null}
    </ProgramCardOverallContainer>
  )
}

export default MyProgramCard
