import React from 'react'
import {
  Box, ProgramName, Detail, OwnerWrap,
  LeftSection, RightSection, ProgramSection, ImgOwner, OwnerName
} from './styled'
import { ProgramType } from 'modules/stores/types'
import { pick } from 'lodash'
import history from 'core/history'
import BackAboutUs from 'components/images/BackAboutUs.jpg'

interface Props {
  program: ProgramType
  className?: string
}
const ProgramCard = ({ program, className }: Props) => {
  const { ownerName, name, description, image, ownerPic } =
    pick(program, ['ownerName', 'name', 'description', 'image', 'ownerPic', 'rating'])
  const onClick = () => {
    history.push({
      pathname: `/programsuggest/${program.id}`,
      state: { isSuggestProgram: true }
    })
  }
  return (
    <Box className={className} onClick={onClick}>
      <LeftSection>
        <ProgramSection>
          <ProgramName>{name}</ProgramName>
          <Detail>{description}</Detail>
        </ProgramSection>
        <OwnerWrap>
          <ImgOwner src={ownerPic} />
          <OwnerName>@ {ownerName}</OwnerName>
        </OwnerWrap>
      </LeftSection>
      <RightSection src={image || BackAboutUs} />
    </Box>
  )
}

export default ProgramCard
