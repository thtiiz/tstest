import React, { useState } from 'react'
import {
  OverallContainer,
  BGHeader,
  ProfileContainer,
  ProfilePicture,
  Name,
  UserName,
  SelectIcon,
  ProgramImg,
  MyProgramText,
  MyProgramContainer,
  SettingBtn,
} from './styled'
import HeaderPIC from 'components/images/BG-HEADER.jpg'
import MyProgramIcon from '../../components/images/MyProgramIcon.svg'
import MyProgramContent from 'modules/MyProgramContent'
import SettingBtnIcon from 'components/images/Settings.svg'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import UserStore from 'modules/stores/UserStore'
import { UserType, MyProgramType } from 'modules/stores/types'
import withAuth from 'utils/withAuth'
import history from 'core/history'
import { ViewPic } from 'components/Viewpic'
import { pick } from 'lodash'

interface StoreProps {
  user: UserType
  myPrograms: MyProgramType[]
}

const onClickAdd = () => {
  history.push({
    pathname: `/Editprofile`,
  })
}

const onClickMyPrograms = () => {
  history.push({
    pathname: `/myprograms`,
  })
}

export const Profile = (props: StoreProps) => {
  const { user, myPrograms } = props
  const [isView, setIsView] = useState(false)
  const toggleView = () => setIsView(!isView)
  const { email, image, firstName, lastName } = pick(user, ['email', 'image', 'firstName', 'lastName'])
  var Usename = email.slice(0, email.indexOf('@'))
  return (
    <OverallContainer>
      {isView ? <ViewPic img={image} onClick={toggleView}></ViewPic> : null}
      <SettingBtn src={SettingBtnIcon} onClick={onClickAdd} />
      <BGHeader src={HeaderPIC} />
      <ProfileContainer>
        <ProfilePicture src={image} onClick={toggleView}></ProfilePicture>
        <Name>
          {firstName} {lastName}
        </Name>
        <UserName>@ {Usename}</UserName>
        <SelectIcon onClick={onClickMyPrograms} >
          <ProgramImg src={MyProgramIcon}></ProgramImg>
          <MyProgramText>My Programs</MyProgramText>
        </SelectIcon>
        <MyProgramContainer>
          <MyProgramContent myPrograms={myPrograms}></MyProgramContent>
        </MyProgramContainer>
      </ProfileContainer>
    </OverallContainer>
  )
}
interface Stores {
  userStore: UserStore
}

export default compose<StoreProps, {}>(
  withAuth,
  inject(({ userStore }: Stores) => ({
    user: userStore.user,
    myPrograms: userStore.getMyPrograms
  })),
  observer
)(Profile)
