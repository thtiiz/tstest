import React, { useEffect } from 'react'
import { UploadPic } from './styled'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCamera } from '@fortawesome/free-solid-svg-icons'

interface Props {
  color?: string
  className?: string
  folder?: string
  isSquare?: boolean
  handleUpload: (image: string) => void
}

const Upload = ({ folder, handleUpload, color = 'white', className, isSquare }: Props) => {
  const widget = () => {
    var widget = (window as any).cloudinary.createUploadWidget(
      {
        cloudName: 'del7hdvpw',
        uploadPreset: 'xfjglucz',
        googleApiKey: '857615762462737',
        cropping: 'server',
        cropping_coordinates_mode: 'custom',
        folder: folder || 'profile-img',
        croppingAspectRatio: isSquare ? 1 : null
      },
      (_error: any, result: any) => {
        if (result.event === 'success') {
          handleUpload(result.info.url)
        }
      },
    )
    widget.open()
  }
  useEffect(() => {
    var loadScript = function () {
      var tag = document.createElement('script')
      tag.async = false
      tag.src = 'https://widget.cloudinary.com/v2.0/global/all.js'
      var body = document.getElementsByTagName('body')[0]
      body.appendChild(tag)
    }
    loadScript()
  }, [])
  return (
    <UploadPic onClick={widget} className={className}>
      <FontAwesomeIcon size="2x" icon={faCamera} color={color} />
    </UploadPic>
  )
}

export default Upload
