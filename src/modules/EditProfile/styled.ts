import { Btn } from 'components/Button/styled'
import styled from 'styled-components'

interface Props {
	src: string;
}

export const OverallContainer = styled.div`
	background-color: #c4c4c4;
	padding-bottom: 92px;
`

export const BGHeader = styled.div`
	height: 265px;
	width: 100%;
	background-image: url(${({ src }: Props) => src});
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
`

export const ProfileContainer = styled.div`
	min-height: 650px;
	position: relative;
	margin-left: 10px;
	margin-right: 10px;
	padding-bottom: 40px;
	background-color: #ffffff;
	display: flex;
	flex-direction: column;
	align-items: center;
	box-shadow: 0px 2px 24px rgba(0, 0, 0, 0.14), 0px 8px 10px rgba(0, 0, 0, 0.2);
	border-radius: 6px;
`
export const ProfilePicture = styled.div`
	background-image: url(${({ src }: Props) => src});
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	transform: translate(0, -50%);
	border-radius: 50%;
	width: 160px;
	height: 160px;
	box-shadow: 0px 16px 30px rgba(0, 0, 0, 0.3), 0px 4px 10px rgba(0, 0, 0, 0.3);
`

export const StopLine = styled.div`
	border: 1px solid #000000;
	min-width: 329px;
	height: 0px;
	margin: 0px 13px 27px 13px;
`

export const ApplyBtn = styled(Btn)`
	width: 203px;
	height: 64px;
	background: #f7522a;
	box-shadow: 0px 4px 20px #dadada;
	border-radius: 4px;
	margin-top: 40px;
	font-weight: 500;
	font-size: 18px;
`

export const EditBtn = styled(Btn)`
	width: 47px;
	height: 27px;
	background: #f7522a;
	border-radius: 4px;
	position: absolute;
	top: 0px;
	right: 0px;
`
export const HeaderContent = styled.div`
	font-weight: bold;
	font-size: 18px;
	text-align: left;
	color: #20212a;
	margin-bottom: 12px;
`
export const PasswordContainer = styled.div`
	width: 300px;
	min-height: 69px;
	position: relative;
`

export const ErrorMessage = styled.div`
	font-size: 12px;
	color: red;
`
