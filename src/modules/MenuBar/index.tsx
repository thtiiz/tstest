import React from 'react'
import {
	Container, SignInText, Backdrop,
	AvatarSection, ImageProfile, SectionMenu, ImgLogo, MenuName
} from 'modules/MenuBar/styled'
import home from 'components/images/menuBar-home.svg'
import mapGym from 'components/images/menuBar-mapGym.svg'
import myWorkoutPlan from 'components/images/menuBar-myWorkoutPlan.svg'
import workoutPlan from 'components/images/menuBar-workoutPlan.svg'
import aboutUs from 'components/images/menuBar-user.svg'
import logout from 'components/images/menuBar-logout.svg'
import { Linked } from 'components/Linked/'
import { inject, observer } from 'mobx-react'
import { compose } from 'recompose'
import UserStore from 'modules/stores/UserStore'
import avatarImage from 'components/images/ava.svg'
import { UserType } from 'modules/stores/types'
import { delayUnmounting } from 'components/DelayUnmounting'

const MenuItems = [
	{ path: '/home', icon: home, text: 'Home' },
	{ path: '/search', icon: mapGym, text: 'Exercise' },
	{ path: '/myprograms', icon: myWorkoutPlan, text: 'My Programs' },
	{
		path: {
			pathname: '/myprograms',
			state: { isSuggestProgram: true }
		},
		icon: workoutPlan,
		text: 'Suggest Programs'
	},
	{ path: '/aboutus', icon: aboutUs, text: 'About Us' }
]

const logOutMenu =
	{ path: '/home', icon: logout, text: 'Logout' }
interface StoreProps {
	isLogin: boolean
	user: UserType
	logout: any
}
interface Props {
	isOpen: boolean
	closeMenuBar: () => void
}
interface MenuItemTypes {
	path: string | object
	icon: string
	text: string
}
interface HocTypes {
	delayTime: number
	isOpen: boolean
}

export const MenuBar = (props: Props & StoreProps) => {
	const { user, logout, isLogin, isOpen, closeMenuBar } = props

	const renderIconMenus = (menuItems: MenuItemTypes[]) => (
		menuItems.map((menuItem, i) =>
			<Linked onClick={closeMenuBar} to={menuItem.path}
				className="navbar-item" key={`menuItem-${menuItem.text}`}>
				<SectionMenu>
					<ImgLogo src={menuItem.icon} alt="icon" />
					<MenuName>{menuItem.text}</MenuName>
				</SectionMenu >
			</Linked>
		)
	)

	const onLogout = () => {
		logout()
		closeMenuBar()
	}
	return (

		<div>
			<Backdrop onClick={closeMenuBar} isOpen={isOpen}></Backdrop>
			<Container isOpen={isOpen}>
				<AvatarSection>
					{isLogin ? <Linked to='/profile' onClick={closeMenuBar}><ImageProfile
						src={user.image} alt="imageProfile" /></Linked>
						: <Linked onClick={closeMenuBar} to='/login'>
							<ImageProfile src={avatarImage} alt="avatarImage" />
						</Linked>}
					{isLogin ? <SignInText>{user.firstName}</SignInText>
						: <Linked onClick={closeMenuBar} to='/login' ><SignInText>Login</SignInText></Linked>}
				</AvatarSection>

				{renderIconMenus(MenuItems)}
				{isLogin ? <Linked onClick={onLogout} to={logOutMenu.path} >
					<SectionMenu>
						<ImgLogo src={logOutMenu.icon} alt="icon" />
						<MenuName>{logOutMenu.text}</MenuName>
					</SectionMenu ></Linked> : null}

			</Container>
		</div >
	)
}

interface Stores {
	userStore: UserStore
}

export default compose<HocTypes & StoreProps & Props, Props & HocTypes>(
	delayUnmounting,
	inject(({ userStore }: Stores) => ({
		isLogin: userStore.isLogin,
		user: userStore.user,
		logout: userStore.logout,
	})),
	observer
)(MenuBar)
