
import React, { Component } from 'react'
import { CarouselCard } from 'modules/CarouselCard'
import { TitleSection, Title, Container, Card, ViewMore } from './styled'
import { ProgramType } from 'modules/stores/types'
import SkeletonCard from 'components/SkeletonCard'
import ProgramCard from 'components/ProgramCard'
import history from 'core/history'
import SuggestProgramStore from 'modules/stores/SuggestProgramStore'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
interface Props {
    programs: ProgramType[]
    title: string
}
interface StoreProps {
    fetchSuggestProgram: () => void
}

const renderSkeletonCard = () => (new Array(6).fill(0).map((value, i) =>
    <Card key={`program-skeleton-${i}`}><SkeletonCard /></Card>
))

class CarouselProgram extends Component<StoreProps & Props, {}> {
    componentDidMount() {
        const { fetchSuggestProgram } = this.props
        fetchSuggestProgram()
    }

    carouselCard = () => {
        const { programs } = this.props
        return (<CarouselCard autoPlay>
            {programs.length !== 0 ? this.renderCard() : renderSkeletonCard()}
        </CarouselCard>)
    }

    handleViewMore = () => {
        history.push({
            pathname: `/myprograms`,
            state: { isSuggestProgram: true }
        })
    }

    renderCard = () => {
        const { programs } = this.props
        return programs.map(program =>
            <Card key={`programCard-${program.id}`}>
                <ProgramCard program={program} />
            </Card>
        )
    }

    render() {
        return (
            <div>
                <TitleSection >
                    <Title>{this.props.title}</Title>
                    <ViewMore onClick={this.handleViewMore}>view more</ViewMore>
                </TitleSection >
                <Container>
                    {this.carouselCard()}
                </Container>
            </div>
        )
    }
}
interface Stores {
    suggestProgramStore: SuggestProgramStore
}

export default compose<StoreProps & Props, Props>(
    inject(({ suggestProgramStore }: Stores) => ({
        fetchSuggestProgram: suggestProgramStore.fetchSuggestProgram,
    })),
    observer,
)(CarouselProgram)
