import styled from 'styled-components'

export const ContentContainer = styled.div`
	padding: 15px 10px 15px 10px;
	display: flex;
	flex-direction: column;
	align-items: center;
	> div:not(:last-child) {
		margin-bottom: 10px;
	}
`
