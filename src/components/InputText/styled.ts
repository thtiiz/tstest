import styled, { css } from 'styled-components'

export const Text = styled.input``
export const UnderLine = styled.span`
		display: block;
        position: absolute;
        bottom:-1px;
        left:0;
        width: 0;
        height: 2px;
        background: #f7522a;
        transition: all 200ms ease-out;
`

const style = css`
	input{
    width: 300px;
    height: 30px;
    line-height: 30px;
    font-size:14px;
    border:0;
    background: none;
    border-bottom:1px solid #ccc;
    outline:none;
    border-radius: 0;
    &:focus,&:valid{
			&~label{
          color: black;
          transform: translateY(-20px);
          font-size:0.825em;
          cursor:default;
      }	
    }
					
    &:focus{
      &~span{
        width: 100%;
      }
    }
	}
	label{
    position: absolute;
    top:0;
    left:0;
    height: 30px;
    color:#ccc;
    cursor:text;
    pointer-events:none;
    transition: all 200ms ease-out;
  }
  span{
    display: block;
    position: absolute;
    bottom:-1px;
    left:0;
    width: 0;
    height: 2px;
    color:#f7522A;
    transition: all 200ms ease-out;
  }
`

export const InputWrap = styled.div`
		position: relative;
		${style}	
`
