import styled from 'styled-components'
import Upload from 'modules/Upload'

interface Props {
	src: string;
}

export const Container = styled.div`
	position: relative;
`

export const Cover = styled.div`
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	height: 200px;
	background-image: url(${({ src }: Props) => src});
`

export const StyleUpload = styled(Upload)`
	right: 4px;
	top: 30px;
`
