import styled from 'styled-components'

export const Background = styled.div`
	height: 320px;
	width: 100%;
	background-color: black;
	color: white;
`
export const Appname = styled.div`
	justify-content: center;
	padding-top: 22px;
	font-size: 24px;
	display: flex;
`
export const Social = styled.div`
	margin-left: 120px;
	font-size: 24px;
`
export const Copyright = styled.div`
	margin-top: 28px;
	font-size: 11px;
`

export const Map = styled.div`
	justify-content: center;
	text-align: left;
	font-size: 9px;
	display: flex;
`
export const Apptext = styled.div`
	padding-left: 22px;
	font-size: 13px;
`
