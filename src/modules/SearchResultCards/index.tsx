import React from 'react'
import ExcercisePostCard from 'components/ExcercisePostCard'
import { ExerciseType } from 'modules/stores/types'
import { Container, ContainerCard } from './styled'

interface Props {
  searchResults: ExerciseType[];
}

const SearchResultCards = ({ searchResults }: Props) => (
  <Container>
    {searchResults.map(searchResult => {
      return (
        <ContainerCard key={`search-${searchResult.name}`}>
          <ExcercisePostCard hasAddProgramIcon exercise={searchResult} />
        </ContainerCard>
      )
    })}
  </Container>
)

export default SearchResultCards
