import React from 'react'
import { Btn } from './styled'
import { Spinner } from 'components/Spinner'

interface ButtonTypes {
  color?: string
  children?: any
  secondary?: boolean
  className?: string
  type?: string
  form?: string
  onClick?: any
  disabled?: any

}

const Button = (props: any) => {
  const { color, secondary, className, children, onClick, disabled } = props
  return (
    disabled ? <div style={{ marginBottom: '25px' }}><Spinner /></div>
      : <Btn color={color} secondary={secondary} className={className} onClick={onClick} disabled={disabled}>
        {children}
      </Btn>
  )
}

export default Button
