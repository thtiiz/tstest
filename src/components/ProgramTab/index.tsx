import React from 'react'
import { ChoiceButton, ChooseButton } from './styled'

interface Props {
  isClick: boolean
  onClick: (tabId: number) => void
  tabId: number
  name: any
}

const ProgramTab = ({ isClick, name, onClick, tabId }: Props) => {
  const setTabState = () => {
    onClick(tabId)
  }
  return (
    <div style={{ width: '100%', marginBottom: '20px' }} onClick={setTabState}>
      {isClick ? <ChooseButton>{name}</ChooseButton> : <ChoiceButton>{name}</ChoiceButton>}
    </div>
  )
}

export default ProgramTab
