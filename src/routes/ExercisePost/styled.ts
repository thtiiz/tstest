import { fullHeight } from './../../utils/index'
import { Btn } from 'components/Button/styled'
import styled from 'styled-components'

export const ExerciseDetailCard = styled.div`
	width: 100%;
	${fullHeight};
	background-color: white;
	justify-content: center;
`

export const ImgBox = styled.div`
	width: 100%;
	height: 222px;
	background-repeat: no-repeat;
	background-position: center center;
	background-size: auto 90%;
`

export const DetailBoxblack = styled.div`
	width: 100%;
	min-height: 428px;
	background-color: #323232;
	padding: 23px;
	display: flex;
	justify-content: center;
`
export const DetailBoxwhite = styled.div`
	width: 100%;
	min-height: 150px;
	background-color: white;
	padding: 23px;
	display: flex;
	justify-content: center;
`

export const HeaderName = styled.h1`
	font-style: normal;
	font-weight: bold;
	font-size: 24px;
	line-height: 31px;
	color: #f7422a;
	border-radius: 20px;
	margin-top: 19px;
`

export const AddBtn = styled(Btn)`
	cursor: pointer;
	width: 261px;
	height: 59px;
	font-size: 18px;
	margin-bottom: 50px;
	font-weight: bold;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	&:hover{
		background-color: #E0261B;
	}
`
export const CoverText = styled.div`
	min-width: 300px;
	width: 900px;
	text-align: left;
`
