import styled from 'styled-components'

export const Container = styled.div`
    margin-top:20px;
    width:100%;
    height:50px;
    background-color:black;
    display:flex;
    align-items:center;
    justify-content:center;
`

export const Text = styled.span`
    font-size:16px;
    color:white;
    
`
