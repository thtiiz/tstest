import styled from 'styled-components'

export const SearchBox = styled.div`
	position: relative;
	display: flex;
	width: 281px;
	height: 41px;
	background: white;
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	align-items: center;
	border-radius: 4px;
`

export const FilterStyled = styled.img`
	position: absolute;
	top: 13px;
	right: 15px;
	cursor: pointer;
`
export const SearchIconStyled = styled.img`
	align-items: center;
	margin-left: 12px;
	cursor: pointer;
`
