import styled, { css } from 'styled-components'

interface Props {
	status?: number;
	isShowDetail?: boolean;
}

interface StatusType extends Props {
	PercentCover: number;
}

const CoverBarColor = (props: Props) => {
	const { status } = props
	switch (status) {
		case 1:
			return '#f7522a'
		case 2:
			return '#bfbfbf'
		case 3:
			return '#00ad89'
	}
}

export const ProgramName = styled.div`
	font-size: 18px;
	line-height: 14px;
	display: flex;
	align-items: flex-end;
	color: #000000;
`
export const TitleName = styled.div`
	position: relative;
	align-items: center;
	margin-bottom: 20px;
`
export const ProgramCardContainer = styled.div`
	padding: 10px;
	cursor: pointer;
`

export const PercentCoverContainer = styled.div`
	border-radius: 3px;
	background: #bfbfbf;
	width: 255px;
	height: 12px;
	margin-bottom: 20px;
	position: relative;
`

export const Cover = styled.div`
	width: ${({ PercentCover }: StatusType) => `${PercentCover}%`};
	height: 100%;
	background: ${CoverBarColor};
	border-radius: 3px 0px 0px 3px;
`

export const Percent = styled.div`
	position: absolute;
	left: 0px;
	top: 0px;
	font-size: 11px;
	line-height: 42px;
	text-align: center;
	color: #000000;
`
const rotate = css`
	transform: rotate(180deg);
`
export const ArrowIcon = styled.img`
	${({ isShowDetail }: Props) => (isShowDetail ? rotate : null)};
	cursor: pointer;
	z-index: 1;
	position: absolute;
	right: 10px;
	top: 12px;
`

export const ProgramDetailBox = styled.div`
	height: 80px;
	background: #fcfcfc;
	box-sizing: border-box;
	text-align: left;
	font-size: 18px;
	line-height: 13px;
	padding: 10px;
	> div:not(:last-child) {
		margin-bottom: 10px;
	}
`

export const ProgramCardOverallContainer = styled.div`
	box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
	border-radius: 6px;
	margin-bottom: 50px;
	position: relative;
`
