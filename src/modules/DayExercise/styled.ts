import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface Props {
	isEdit: boolean;
}

export const CheckIcon = styled(FontAwesomeIcon)`
	margin-right: 10px;
	margin-top: 90px;
	cursor: pointer;
`

export const CardWrapper = styled.div`
	pointer-events: ${({ isEdit }: Props) => (isEdit ? 'None' : 'auto')};
	width: 303px;
`

export const Container = styled.div`
	display: flex;
`
