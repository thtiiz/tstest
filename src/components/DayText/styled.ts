import styled from 'styled-components'

export const SecondText = styled.div`
	font-size: 20px;
	font-weight: bold;
	text-align: left;
	margin: 0px 0px 15px 20px;
`

export const Highlight = styled.span`
	color: #f7522a;
`
