import styled from 'styled-components'

export const Box = styled.div`
	display: flex;
	width: 100px;
	height: 30px;
	justify-content: space-between;
	font-size: 14px;
	background-color: #4d4d4d;
	color: #ffffff;
	border-radius: 4px;
	align-items: center;
`
export const ClickBox = styled.img`
	cursor: pointer;
	padding: 10px;
`
