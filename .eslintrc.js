// 1 warning, 2 error
module.exports = {
	// "parser": "babel-eslint",
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2018,
		ecmaFeatures: {
			jsx: true,
			experimentalObjectRestSpread: true,
		},
		sourceType: 'module',
	},

	env: {
		browser: true,
		node: true,
		commonjs: true,
		es6: true,
		'jest/globals': true,
	},

	// Standard JavaScript Style Guide
	plugins: ['jsx-a11y', 'jest', 'standard', 'promise', '@typescript-eslint'],
	extends: [
		'react-app',
		'standard',
		// "plugin:prettier/recommended",
		'plugin:react/recommended',
		'plugin:@typescript-eslint/recommended',
		'eslint:recommended',
		'prettier/@typescript-eslint',
	],
	rules: {
		'comma-dangle': [0],
		'no-var-requires': [0],
		'@typescript-eslint/no-explicit-any': [0],
		'@typescript-eslint/explicit-member-accessibility': [0],
		'@typescript-eslint/explicit-function-return-type': [0],
		semi: [2, 'never'],
		'no-unused-vars': [1, { args: 'none' }], //ex (state, props) -> props never used
		indent: [0, 'tab', { SwitchCase: 1 }], // after git commit -> failed
		'space-before-function-paren': [0],
		'no-underscore-dangle': [0], // _ before action name in actions.js
		'no-tabs': [0],
		'no-param-reassign': [0], // eg. function(a) { a = 12 }
		'global-require': [0], // eg. Unexpected require() in src/modules/th.js
		'no-confusing-arrow': [0],
		'no-shadow': [0],
		'max-len': [1, 110, 2], // comment len
		'consistent-return': [0], // must return at the end of arrow function
		'prefer-destructuring': [0],
		'no-use-before-define': [0], // define function after function that use it
		'no-return-assign': [0], // Arrow function should not return assignment eg. cant do this -> ref={el => (this.picture = el)}
		'no-console': [0], // allow just console.log
		'object-shorthand': [0], // Expected method shorthand
		'func-names': [0], // functions must have a name
		'no-restricted-properties': [1], // Math.pow -> exponentiation operator (**) instead
		'no-plusplus': [0], // eg. i++ or i--
		'no-mixed-operators': [0], // eg. ( a + b / c)
		'class-methods-use-this': [0], // Expected 'this' to be used by class method 'selector'
		'default-case': [0], // switch must have default case
		'no-lone-blocks': [0], // Block is redundant eg. { } in switch case
		'no-unused-expressions': [0],
		'no-restricted-globals': [0],
		'no-restricted-syntax': [0],
		'function-paren-newline': [0], // Unexpected newline after '(' or before ')' of function -> maybe conflict with prettier
		'arrow-parens': [2, 'as-needed'],
		'one-var': [0], // Split 'const' declarations into multiple statements
		'prefer-promise-reject-errors': [0], // Expected the Promise rejection reason to be an Error eg. in ApiManager.js
		'no-sequences': [0],
		'no-nested-ternary': [0], // Nesting ternary expressions can make code more difficult to understand
		'array-callback-return': [0], // enforces usage of return statement in callbacks of array’s methods.
		'no-empty-function': [0],
		'no-continue': [0], // no continue in if
		'guard-for-in': [0], // need Object.prototype.hasOwnProperty.call() in for in of obj
		'no-new': [0],
		'no-buffer-constructor': [0], // Use the producer methods Buffer.from, Buffer.alloc, and Buffer.allocUnsafe instead
		'no-proto': [0], // no __proto__ -> Use getPrototypeOf method instead
		'no-new-func': [0], // no new Function()
		'operator-assignment': [0], // Assignment can be replaced with operator assignment & failed after git commit
		'object-curly-newline': [0], // can fix with --fix but faild after git push
		'quote-props': [0], // can fix with --fix but conflict with flow "on-string literal property keys not supported"
		'no-alert': [0], // allow alert(), confirm(), prompt()
		'no-mixed-spaces-and-tabs': [0],
		'wrap-iife': [0],
		'semi-style': [0],
		'no-extra-semi': [0],
		'generator-star-spacing': [0],
		'implicit-arrow-linebreak': [0],
		'linebreak-style': [0],
		'no-else-return': [0, { allowElseIf: true }],

		//react
		'react/display-name': [0],
		'react/prop-types': [0],
		'react/prefer-stateless-function': [1],
		'react/jsx-no-target-blank': [1], // no target='_blank' in <a> element
		'react/jsx-no-bind': [0], // no bind() in JSX prop -> it will create new function
		'react/no-redundant-should-component-update': [0], // if have shouldComponentUpdate defined in component that extends React.PureComponent but it works with React.Component
		'react/no-did-mount-set-state': [1], // Do not use setState in componentDidMount
		'react/no-multi-comp': [0], // Declare only one React component per file
		'react/no-unescaped-entities': [1], // text in html element eg. -> <span className={s.spanText}>OWNER'S MESSAGE</span>
		'react/no-array-index-key': [0], // cannot use key={index of array}
		'react/jsx-key': [2], // requires key prop for react element in a collection
		'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx', 'ts', 'tsx'] }],
		'react/jsx-indent': [0, 'tab'], // after git commit it failed
		'react/jsx-indent-props': [2, 'tab'],
		'react/jsx-closing-tag-location': [0], // can fix by --fix but after git commit it failed
		'react/no-unused-state': [1],
		'react/no-unused-prop-types': [1],
		'react/sort-comp': [
			2,
			{
				order: [
					'static-methods',
					'state',
					'/.*(formState|FormState).*/',
					'init',
					'lifecycle',
					'/^build.+$/',
					'/^on.+$/',
					'/^(get|set)(?!(InitialState$|DefaultProps$|ChildContext$)).+$/',
					'everything-else',
					'/^render.+$/',
					'render',
				],
			},
		],
		'@typescript-eslint/camelcase': [0],


		//import
		'import/no-extraneous-dependencies': [0], // Forbid the import of external modules that are not declared in the package.json's dependencies, devDependencies, optionalDependencies or peerDependencies.
		'import/first': [0], // imports that come after non-import statements.
		'import/no-unresolved': [0],
		'import/extensions': [0],
		'import/prefer-default-export': [0],
		'import/no-mutable-exports': [0], // Forbids the use of mutable exports with var or let
		'import/newline-after-import': [0],
		'import/no-dynamic-require': [0],

		//jsx-a11y
		'jsx-a11y/anchor-is-valid': [0], // <a tag if has onClick -> <button
		'jsx-a11y/click-events-have-key-events': [0], // if has onClick must have at least onKeyUp, onKeyDown, onKeyPress
		'jsx-a11y/no-static-element-interactions': [0], // HTML elements with event handlers require a role={}
		'jsx-a11y/alt-text': [1], // img elements must have an alt prop
		'jsx-a11y/anchor-has-content': [0], // fail if <a><TextWrapper aria-hidden /></a>
		'jsx-a11y/iframe-has-title': [1], // <iframe> elements must have a unique title property
		'jsx-a11y/no-noninteractive-element-interactions': [0],
		'jsx-a11y/no-autofocus': [0],

		//test
		'jest/no-disabled-tests': 'warn',
		'jest/no-focused-tests': 'error',
		'jest/no-identical-title': 'error',
		'jest/prefer-to-have-length': 'warn',
		'jest/valid-expect': 'error',
	},
	overrides: [
		{
			files: ['*.js'],
			rules: {
				// "@typescript-eslint/no-var-requires": "off",
				// "@typescript-eslint/no-use-before-define": "off"
			},
		},
	],
	settings: {
		'import/resolver': {
			node: {
				moduleDirectory: ['node_modules', 'src/'],
			},
		},
		react: {
			version: require('./package.json').dependencies.react,
		},
	},
}
