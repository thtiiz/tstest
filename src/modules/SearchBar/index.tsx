import React, { Component } from 'react'
import { SearchBox, FilterStyled, SearchIconStyled } from './styled'
import filter from 'components/images/SearchBar-filter.svg'
import searchIcon from 'components/images/SearchBar-searchicon.svg'
import Filter from 'modules/Filter/lazy'
import history from 'core/history'
import { serializeParams } from 'utils'
import qs from 'querystring'
import FilterStore from 'modules/stores/FilterStore'
import { compose } from 'recompose'
import { observer, inject } from 'mobx-react'
import { ExerciseName } from 'modules/stores/FilterStore/types'
import SearchInput from 'modules/SearchInput'
import { get } from 'lodash'
import SearchStore from 'modules/stores/SearchStore'
import { STATES } from 'core/api/FetchData'
interface State {
  isOpenFilter: boolean;
}

interface StoreProps {
  state: string,
  searchValue: string
  handleSearchChange: (newVal: string) => void
  fetchExerciseNames: () => string[]
  exerciseNames: ExerciseName[]
  historyOptions: string[]
  pushSearchHistory: (value: string) => void
  syncHistory: () => void
}

class SearchBar extends Component<{} & StoreProps, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      isOpenFilter: false,
    }
  }

  componentDidMount() {
    const { syncHistory } = this.props
    syncHistory()
  }

  fetchOptions = () => {
    const { fetchExerciseNames, exerciseNames } = this.props
    if (exerciseNames.length === 0) fetchExerciseNames()
  }

  handleSearch = (newVal?: any) => {
    const { pushSearchHistory } = this.props
    const params = qs.parse(history.location.search.slice(1))
    const value = get(newVal, 'value')
    if (value) pushSearchHistory(value)
    history.push({
      pathname: '/search',
      search: serializeParams(Object.assign(params, { q: value }))
    })
  }

  handleToggle = () => {
    const { isOpenFilter } = this.state
    this.setState({ isOpenFilter: !isOpenFilter })
  }

  render() {
    const { isOpenFilter } = this.state
    const { searchValue, exerciseNames, historyOptions, handleSearchChange, state } = this.props
    const options = searchValue ? exerciseNames : historyOptions
    return (
      <div>
        <SearchBox>
          <SearchIconStyled src={searchIcon} onClick={this.handleSearch} />
          <SearchInput value={searchValue} handleSearch={this.handleSearch}
	isLoading={state !== STATES.LOADED} options={options}
	onFocus={this.fetchOptions} onInputChange={handleSearchChange} />
          <FilterStyled src={filter} onClick={this.handleToggle} />
        </SearchBox>
        {isOpenFilter && <Filter handleToggle={this.handleToggle} isOpenFilter={isOpenFilter} />}
      </div>
    )
  }
}

interface Store {
  filterStore: FilterStore
  searchStore: SearchStore
}

export default compose<StoreProps, {}>(
  inject(({ filterStore, searchStore }: Store) => ({
    state: filterStore.state,
    searchValue: filterStore.searchValue,
    handleSearchChange: filterStore.handleSearchChange,
    fetchExerciseNames: filterStore.fetchExerciseNames,
    exerciseNames: filterStore.exerciseNames,
    syncHistory: filterStore.syncHistory,
    historyOptions: searchStore.historyOptions,
    pushSearchHistory: searchStore.pushSearchHistory
  })),
  observer
)(SearchBar)
