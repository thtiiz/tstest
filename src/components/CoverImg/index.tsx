import React from 'react'
import { Cover, StyleUpload, Container } from './styled'
import BackAboutUs from 'components/images/BackAboutUs.jpg'
interface Props {
  image: string
  canEdit?: boolean
  handleUpload?: (imageUrl: string) => void
}

const CoverImg = ({ canEdit = false, image, handleUpload }: Props) => {
  return (
    <Container>
      {canEdit && <StyleUpload folder="cover-img" color="#4c4c4c"
	handleUpload={handleUpload || (() => null)} />}
      <Cover src={image || BackAboutUs} />
    </Container>
  )
}

export default CoverImg
