import styled from 'styled-components'

export const Header = styled.div`
	font-size: 18px;
	text-align: center;
	color: #000000;
	margin-top: 13px;
`
export const More = styled.div`
	font-size: 12px;
	text-align: center;
	color: #f7522a;
	margin: 10px;
	margin-bottom: 18px;
`
export const SelectBtn = styled.div`
	display: flex;
`

const Button = styled.div`
	width: 160px;
	height: 51px;
	display: flex;
	align-items: center;
	justify-content: center;
	cursor: pointer;
`

export const Yes = styled(Button)`
	color: #ffffff;
	background-color: #f7522a;
`
export const No = styled(Button)`
	color: rgba(0, 0, 0, 0.6);
	border-top: 1px solid rgba(0, 0, 0, 0.25);
`
