import React, { useContext } from 'react'
import { Day, Container, CheckIcon } from './styled'
import { faCircle } from '@fortawesome/free-regular-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { ExerciseType, DayType } from 'modules/stores/types'
import DayWithDropdown from 'components/DayWithDropdown'
import { compose } from 'recompose'
import { inject, observer } from 'mobx-react'
import ExerciseStore from 'modules/stores/ExerciseStore'
import { get } from 'lodash'
import { AddProgramContext } from 'utils'
import history from 'core/history'
import MyProgramStore from 'modules/stores/MyProgramStore'
import { useParams } from 'react-router'
import withAuth from 'utils/withAuth'

interface StoreProps {
  exerciseId: (id: string) => ExerciseType
}

interface Props {
  day: DayType
  index: number
  isEdit: boolean
  myProgramStore: MyProgramStore
}

const DayButton = ({ isEdit, day, myProgramStore, index, exerciseId }: Props & StoreProps) => {
  const { isCheckedElements, clickElementOnDelete, setsPerDayArr,
    increaseSetPerExercise, decreaseSetPerExercise } = myProgramStore

  const checkIcon = isCheckedElements[index] ? faCheckCircle : faCircle
  const id = get(history, ['location', 'state', 'exerciseId'], '')
  const myProgramId = get(useParams(), 'id')
  const exercise = exerciseId(id)

  const clickDayOnEdit = () => {
    clickElementOnDelete(index, day.id)
  }
  const onClickDay = () => {
    const state = history.location.state
    history.push({
      pathname: `/myprogram/day/${index + 1}`,
      state: { ...state, myProgramId, dayId: day.id }
    })
  }
  const increaseSet = () => {
    increaseSetPerExercise(index)
  }

  const decreaseSet = () => {
    decreaseSetPerExercise(index)
  }

  const img = get(exercise, ['images', 0], '')
  const name = get(exercise, 'name', '')
  const isAddProgram = useContext(AddProgramContext)
  return (
    isAddProgram ? <DayWithDropdown decreaseSet={decreaseSet}
	increaseSet={increaseSet} img={img} name={name}
	index={index} setPerExercise={setsPerDayArr[index]} />
      : <Container onClick={isEdit ? clickDayOnEdit : onClickDay}>
        {isEdit && <CheckIcon icon={checkIcon} size="2x" color="#f7522a" />}
        <Day isComplete={day.isComplete}>Day {index + 1}</Day>
      </Container >
  )
}

interface Stores {
  exerciseStore: ExerciseStore
}

export default compose<StoreProps & Props, Props>(
  withAuth,
  inject(({ exerciseStore }: Stores) => ({
    exerciseId: exerciseStore.exerciseId,
  })),
  observer
)(DayButton)
