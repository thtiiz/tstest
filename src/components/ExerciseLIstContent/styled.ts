import styled from 'styled-components'
interface Props {
	theme: string;
}
const headercolor = ({ theme }: Props) => {
	return theme === 'black' ? '#C4C4C4' : '#000000'
}

const detailcolor = ({ theme }: Props) => {
	return theme === 'black' ? '#FFFFFF' : '#000000'
}

export const HeaderSection = styled.span`
	font-weight: bold;
	font-size: 18px;
	line-height: 23px;
	color: ${headercolor};
	border-radius: 20px;
`
export const Detail = styled.span`
	font-size: 18px;
	line-height: 23px;
	color: ${detailcolor};
	border-radius: 20px;
`

export const ExerciseEachBox = styled.div`
	margin: 33px 0px 33px 0px;
`
