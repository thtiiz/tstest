import { fullHeight } from 'utils'
import styled from 'styled-components'

export const Container = styled.div`
	background-color: white;
	${fullHeight};
`
export const Line = styled.div`
	height: 2px;
	background-color: #f7522a;
	margin-top: 23.5px;
	margin-bottom: 23.5px;
`
