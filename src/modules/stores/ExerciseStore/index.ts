import { FetchExercise, FetchExercises, ExerciseType } from 'modules/stores/types'
import FetchData from 'core/api/FetchData'
import { observable, action } from 'mobx'

import { get } from 'lodash'
import { computedFn } from 'mobx-utils'
class ExerciseStore extends FetchData {
	@observable
	exercises: ExerciseType[]

	@observable
	exerciseClicked: ExerciseType | {}

	@observable
	chestExercise: ExerciseType[]

	@observable
	beginnerExercise: ExerciseType[]

	constructor() {
		super()
		this.exercises = []
		this.exerciseClicked = {}
		this.fetchExercises()
		this.chestExercise = []
		this.beginnerExercise = []
		this.fetchBeginnerExercise()
		this.fetchChestCategory()
	}

	exerciseId = computedFn((id: string) => {
		return this.exercises
			.concat(this.chestExercise, this.beginnerExercise)
			.find(exercise => exercise.id === id)
	})

	@action
	fetchExercises = async () => {
		const fetch: FetchExercises = await this.fetchData({
			path: '/exercises',
			method: 'get',
			params: { page: 1 },
		})
		this.exercises = get(fetch, 'data', [])
	}

	@action
	fetchBeginnerExercise = async () => {
		const fetch: FetchExercises = await this.fetchData({
			path: '/exercises',
			method: 'get',
			params: { difficulty: 1 },
		})
		this.beginnerExercise = get(fetch, 'data', [])
	}

	@action
	fetchChestCategory = async () => {
		const fetch: FetchExercises = await this.fetchData({
			path: '/exercises',
			method: 'get',
			params: { category: 11 },
		})
		this.chestExercise = get(fetch, 'data', [])
	}

	@action
	clickExercise = (exercise: ExerciseType) => {
		this.exerciseClicked = exercise
	}

	@action
	fetchExerciseById = async (id: string) => {
		const fetch: FetchExercise = await this.fetchData({
			path: `/exercises/${id}`,
			method: 'get',
		})
		this.exerciseClicked = get(fetch, ['data'], {})
	}
}
export default ExerciseStore
