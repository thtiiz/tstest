import styled from 'styled-components'

export const ChooseButton = styled.div`
	cursor: pointer;
	text-align: center;
	background-color: #ffffff;
	color: #20212a;
	font-size: 20px;
	padding: 6px 20px 6px 20px;
	border-bottom: solid 3px #f7522a;
	box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.25);
`
export const ChoiceButton = styled.div`
	background-color: #ffffff;
	font-size: 20px;
	padding: 6px 20px 6px 20px;
	color: #b6b6b6;
	text-align: center;
	cursor: pointer;
	border-bottom: solid 2px #b6b6b6;
`
