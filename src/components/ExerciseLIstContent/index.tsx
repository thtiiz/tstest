import React from 'react'
import { ExerciseEachBox, HeaderSection, Detail } from './styled'

interface Props {
	title: string;
	content: any;
	theme: string;
}
const ExerciseListcontent = ({ title, theme, content }: Props) => {
	return (
		<ExerciseEachBox>
			<HeaderSection theme={theme}>{title}</HeaderSection>
			<Detail theme={theme}> : </Detail>
			<Detail theme={theme}>{content}</Detail>
		</ExerciseEachBox>
	)
}

export default ExerciseListcontent
