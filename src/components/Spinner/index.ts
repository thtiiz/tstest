import styled from 'styled-components'

export const Spinner = styled.div`
	border: 10px solid #f3f3f3;
	border-radius: 50%;
	border-top: 10px solid #f7522a;
	width: 49px;
	height: 49px;
	animation: spin 0.5s linear infinite;
	@keyframes spin {
		0% {
			transform: rotate(0deg);
		}
		100% {
			transform: rotate(360deg);
		}
	}
`
