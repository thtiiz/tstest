import styled, { keyframes } from 'styled-components'
import girl1 from 'components/images/girl1.svg'
import girl2 from 'components/images/girl2.svg'
import girl3 from 'components/images/girl3.svg'
import man1 from 'components/images/man1.svg'
import man2 from 'components/images/man2.svg'

export const InfiFade = keyframes`
	from {color: #ffffff;}
  to {color: #000000;}
`

export const Anime = keyframes`
		0%  {background-image: url(${girl1});}
		20%   {background-image: url(${man1});}
		40%  {background-image: url(${girl2});}
		60%   {background-image: url(${man2});}
		80% {background-image: url(${girl3});}
		100%  {background-image: url(${girl1});}
`

export const ContentBlackBox = styled.div`
	align-items: center;
	justify-content: space-between;
	display: flex;
	padding: 10.133vw;
	@media (min-width: 769px) {
		padding: 10.133vw;
	}
	@media (min-width: 1025px) {
		width: 1024px;
		height: 600.69px;
		padding: 103.762px;
		position: absolute;
		left: 50%;
		transform: translate(-50%);
	}
`
export const ImgBox = styled.div`
	background-repeat: no-repeat;
	background-position: center center;
	background-size: 100% 100%;
`
export const MainText = styled.div`
	display: flex;
	padding: 7.2vw 8vw 7.2vw 8vw;
	font-family: Righteous;
	font-style: normal;
	font-weight: normal;
	font-size: 4.8vw;
	text-align: center;
	@media (min-width: 769px) {
		font-size: 36.864px;
		height: 103.28px;
		padding: 55.296px 61.44px 55.296px 61.44px;
	}
	@media (min-width: 2251px) {
		padding: 7.2vw 8vw 7.2vw 8vw;
	}
`
export const CursorImgBox = styled(ImgBox)`
	@media (min-width: 769px) {
		width: 47.09px;
	}
`
export const MainImgBox = styled(ImgBox)`
	width: 100vw;
	padding-top: 87.5vw;
	@media (min-width: 769px) {
		width: 768px;
		padding-top: 672px;
		display: block;
		margin-left: auto;
		margin-right: auto;
	}
`

export const BlackBox = styled.div`
	height: 113.333vw;
	width: 100%;
	background-color: #20212a;
	@media (min-width: 769px) {
		height: 870.397px;
		display: block;
		margin-left: auto;
		margin-right: auto;
	}
	@media (min-width: 1751px) {
		height: 49.709vw;
		display: block;
		margin-left: auto;
		margin-right: auto;
	}
	@media (min-width: 2151px) {
		height: 60vw;
		display: block;
		margin-left: auto;
		margin-right: auto;
	}
`
export const WhiteBox = styled.div`
	width: 100%;
	height: 150.933vw;
	background-color: #ffffff;
	transform-origin: top left;
	transform: skew(0, 340deg);
	position: absolute;
	top: 214.933vw;
	@media (min-width: 769px) {
		width: 100%;
		height: 1159.165px;
		top: 1650.685px;
	}
	@media (min-width: 1025px) {
		top: 161.2vw;
		width: 100%;
	}
	@media (min-width: 1183px) {
		top: 1905.384px;
		width: 100%;
	}
	@media (min-width: 1751px) {
		top: 108.817vw;
		width: 100%;
		height: 70vw;
	}
`

export const OrangeBox = styled(WhiteBox)`
	background-color: #f7522a;
	transform: skew(0, 20deg);
	top: 189.867vw;
	@media (min-width: 769px) {
		top: 1458.179px;
	}
	@media (min-width: 2000px) {
		top: 70vw;
	}
`
export const PinkBox = styled(WhiteBox)`
	background-color: #fdeeeb;
	top: 344.533vw;
	@media (min-width: 769px) {
		top: 2646.013px;
		width: 100%;
	}
	@media (min-width: 1751px) {
		top: 151.114vw;
		width: 100%;
		height: 56vw;
	}
`
export const Pic = styled.div`
	background-size: 100% 100%;
	border-radius: 2.667vw;
`
export const Button = styled.div`
	height: 40px;
	width: 120px;
	background-color: #f7522a;
	color: #ffffff;
	text-align: center;
	font-size: 18px;
	line-height: 40px;
	margin: auto;
	border-radius: 4px;
	margin-top: 40px;
	cursor: pointer;
	&:hover {
		height: 53.33px;
		line-height: 53.33px;
		width: 160px;
		font-size: 24px;
		background-color: #e0261b;
	}
`
export const ContentText = styled.div`
	font-size: 6.4vw;
	color: #4d4d4d;
	@media (min-width: 769px) {
		font-size: 49.152px;
	}
`
export const WhiteContentText = styled(ContentText)`
	animation: ${InfiFade} 3s ease 0s infinite;
`
export const ContentWhiteBox = styled(ContentBlackBox)`
	position: absolute;
	width: 100%;
	top: 231.733vw;
	padding: 4vw;
	left: 50%;
	margin-right: -50%;
	transform: translate(-50%);
	@media (min-width: 769px) {
		width: 768px;
		top: 1779.71px;
		padding: 30.72px;
	}
	@media (min-width: 1851px) {
		top: 100vw;
	}
`
export const PicInWhiteBox = styled(Pic)`
	height: 59.467vw;
	width: 50.667vw;
	position: relative;
	@media (min-width: 769px) {
		height: 456.706px;
		width: 389.123px;
	}
`
export const PicBehind = styled.img`
	width: 23.2vw;
	height: 51.2vw;
	margin-left: 4.533vw;
	@media (min-width: 769px) {
		width: 178.17px;
		height: 393.2px;
		margin-left: 34.8134px;
	}
`
export const Picture = styled(PicBehind)`
	width: 18.133vw;
	margin-left: 0vw;
	background-size: 100% 100%;
	border-radius: 2.933vw;
	position: absolute;
	animation: ${Anime} 20s ease 0s infinite;
	@media (min-width: 769px) {
		width: 139.25px;
		margin-left: 0px;
		border-radius: 22.5254px;
	}
`
export const ContentPinkBox = styled(ContentWhiteBox)`
	top: 343.467vw;
	position: absolute;
	@media (min-width: 769px) {
		top: 2637.8265px;
	}
	@media (min-width: 1851px) {
		top: 140vw;
	}
`
export const SlideBlackBox = styled.div`
	width: 100%;
	height: 113.333vw;
	background-color: #20212a;
	position: absolute;
	top: 410.933vw;
	height: fit-content;
	@media (min-width: 769px) {
		top: 3155.965px;
	}
	@media (min-width: 1851px) {
		top: 170vw;
		padding-top: 4vw;
	}
`
export const BacktoTopBox = styled.div`
	height: 10.933vw;
	width: 100%;
	color: #ffffff;
	top: 520vw;
	position: absolute;
	background-color: #404040;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 4.8vw;
	@media (min-width: 769px) {
		height: 83.965px;
		top: 3993.6px;
		font-size: 36.864px;
	}
	@media (min-width: 1367px) {
		top: 3950px;
	}
	@media (min-width: 1851px) {
		top: 216vw;
	}
	@media (min-width: 2001px) {
		top: 213vw;
	}
	@media (min-width: 2181px) {
		top: 210vw;
	}
	@media (min-width: 2366px) {
		top: 207vw;
	}
`
export const UpArrow = styled.div`
	width: 5.6vw;
	height: 5.6vw;
	margin: 1.333vw 1.333vw 0 0;
	background-size: 100% 100%;
	@media (min-width: 769px) {
		width: 43.008px;
		height: 43.008px;
		margin: 10.237px 10.237px 0 0;
	}
`
export const WhiteTextBox = styled.div`
	width: 52.533vw;
	height: 17.067vw;
	justify-content: space-between;
	padding: 2.667vw;
	background-color: #ffffff;
	border-radius: 1.067vw;
	display: flex;
`
export const FooterBox = styled.div`
	width: 100%;
	color: #ffffff;
	top: 530.933vw;
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	@media (min-width: 769px) {
		top: 4077.565px;
	}
	@media (min-width: 1367px) {
		top: 4033.965px;
	}
	@media (min-width: 1851px) {
		top: 220.2vw;
	}
	@media (min-width: 2001px) {
		top: 217vw;
	}
	@media (min-width: 2101px) {
		top: 216.8vw;
	}
	@media (min-width: 2181px) {
		top: 213.5vw;
	}
	@media (min-width: 2366px) {
		top: 210.5vw;
	}
	@media (min-width: 2401px) {
		top: 210.4vw;
	}
	@media (min-width: 2481px) {
		top: 210.3vw;
	}
	@media (min-width: 2549px) {
		top: 210.2vw;
	}
`
