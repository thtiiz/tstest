import React from 'react'
import { BoxText, Text, CloseInTag } from './styled'
import close from 'components/images/close.svg'

interface Props {
    postName: string
    onClose: any;
}

const FilterTag = (props: Props) => {
    const { postName, onClose } = props
    return (
        <BoxText>
            <Text>
                {postName}
            </Text>
            <CloseInTag src={close} alt="cls" onClick = {onClose} />
        </BoxText>
    )
}

export default FilterTag
